# Online Clinic ManagementManagement

This project uses **.NET 6.0 | NodeJS 20.5.1 ** to call the API

## How to install

- Download this repo & extract or clone using Git
- Install NodeJS version 20.5.1
- Install project dependency by running 
```bash
npm install
```
- Run
```bash
node app.mjs
```
- Or run this for Development Environment
```bash
npx nodemon app.mjs
```
- Or run this for Production Environments
```bash
npm start
```
- Open `http://localhost/`

## How to run https with SSL

- Prepare 2 files in the ssl folder: key.pem and cert.pem
- Add the following lines of code:
```bash
import fs from 'fs';
import path from 'path';
import https from 'https';
// Another line of code
const privateKey = fs.readFileSync(path.join('ssl', 'key.pem'), 'utf8');
const certificate = fs.readFileSync(path.join('ssl', 'cert.pem'), 'utf8');
const passphrase = '12345678'; // your certificate password
const credentials = { key: privateKey, cert: certificate, passphrase: passphrase };
const httpsServer = https.createServer(credentials, app);
```
- Use
```bash
httpsServer.listen(443, function () { console.log(`Server is running`); });
```
- Instead:
```bash
app.listen(80, function () { console.log(`Server is running`); });
```
- Open `https://localhost/`
