import log from '../setup/log.mjs';

class ClientController {
    async saveClientToken(req, res) {
        let token = req.body.token;
        req.session.clientToken = token;
        return res.send('tolen saved')
    };

    async removeClientToken(req, res) {
        if (req.session.clientToken) {
            delete req.session.clientToken;
            return res.send('Token removed');
        } else {
            return res.status(404).send('Token not found');
        }
    }

    async renderHome(req, res) {
        try {
            return res.render('page/client/home');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderLogin(req, res) {
        try {
            return res.render('page/client/login');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderRegister(req, res) {
        try {
            return res.render('page/client/register');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderCart(req, res) {
        try {
            return res.render('page/client/cart');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderOrderList(req, res) {
        try {
            return res.render('page/client/order');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderOrderDetail(req, res) {
        try {
            return res.render('page/client/order-detail');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderMedicine(req, res) {
        try {
            return res.render('page/client/medicine');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderMedicineDetail(req, res) {
        try {
            return res.render('page/client/medicine-detail');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    

    async renderScientificApparatus(req, res) {
        try {
            return res.render('page/client/scientific-apparatus');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderScientificApparatusDetail(req, res) {
        try {
            return res.render('page/client/scientific-apparatus-detail');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };
    
    
    async renderSubject(req, res) {
        try {
            return res.render('page/client/subject');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderSubjectDetail(req, res) {
        try {
            return res.render('page/client/subject-detail');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderEducationalEventDetail(req, res) {
        try {
            return res.render('page/client/educational-event-detail');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }    
    };

    async renderMyAccount(req, res) {
        try {
            return res.render('page/client/my-account');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderContact(req, res) {
        try {
            return res.render('page/client/contact');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };
}

export default new ClientController();