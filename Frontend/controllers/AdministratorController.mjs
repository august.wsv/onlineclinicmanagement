import log from '../setup/log.mjs';

class AdministratorController {
    async saveAdminToken(req, res) {
        let token = req.body.token;
        req.session.adminToken = token;
        return res.send('tolen saved')
    };

    async removeAdminToken(req, res) {
        if (req.session.adminToken) {
            delete req.session.adminToken;
            return res.send('Token removed');
        } else {
            return res.status(404).send('Token not found');
        }
    }

    async renderAdministratorLogin(req, res) {
        try {
            return res.render('page/administrator/login');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderAdministratorAdmin(req, res) {
        try {
            return res.render('page/administrator/admin');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    
    async renderAdministratorStaffMember(req, res) {
        try {
            return res.render('page/administrator/staff-member');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderAdministratorClient(req, res) {
        try {
            return res.render('page/administrator/client');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderAdministratorClientMessage(req, res) {
        try {
            return res.render('page/administrator/client-message');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderAdministratorClinic(req, res) {
        try {
            return res.render('page/administrator/clinic');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderAdministratorAboutUs(req, res) {
        try {
            return res.render('page/administrator/about-us');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };
    
    async renderAdministratorMedicineType(req, res) {
        try {
            return res.render('page/administrator/medicine-type');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderAdministratorSubject(req, res) {
        try {
            return res.render('page/administrator/subject');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };
    
    async renderAdministratorMedicine(req, res) {
        try {
            return res.render('page/administrator/medicine');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };
    
    async renderAdministratorScientificApparatus(req, res) {
        try {
            return res.render('page/administrator/scientific-apparatus');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderAdministratorPurchase(req, res) {
        try {
            return res.render('page/administrator/purchase');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    
    async renderAdministratorBusinessReport(req, res) {
        try {
            return res.render('page/administrator/business-report');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };
}

export default new AdministratorController();