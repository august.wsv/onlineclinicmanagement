import log from '../setup/log.mjs';

class StaffMemberController {
    async saveStaffMemberToken(req, res) {
        let token = req.body.token;
        req.session.staffMemberToken = token;
        return res.send('tolen saved')
    };

    async removeStaffMemberToken(req, res) {
        if (req.session.staffMemberToken) {
            delete req.session.staffMemberToken;
            return res.send('Token removed');
        } else {
            return res.status(404).send('Token not found');
        }
    }    

    async renderStaffMemberLogin(req, res) {
        try {
            return res.render('page/staff-member/login');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderStaffMemberSubjectAsHome(req, res) {
        try {
            return res.render('page/staff-member/home');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderStaffMemberSubjectDetail(req, res) {
        try {
            return res.render('page/staff-member/subject-detail');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderStaffMemberEducationalEventDetail(req, res) {
        try {
            return res.render('page/staff-member/educational-event-detail');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }    
    };

    async renderStaffMemberMyAccount(req, res) {
        try {
            return res.render('page/staff-member/my-account');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };
}

export default new StaffMemberController();