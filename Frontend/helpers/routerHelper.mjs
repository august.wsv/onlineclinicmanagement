//v1
const namedRoutes = {};

function addRoute(routeName, path) {
    if (!(routeName in namedRoutes)) {
        namedRoutes[routeName] = path;
    } else {
        throw new Error(`Duplicate route name registration with '${routeName}' detected..`);
    }
}
//

//v2
// import fs from 'fs';
// import path from 'path';
// const folderPath = 'cache';
// const filePath = path.join(folderPath, 'route.json');
// if (!fs.existsSync(folderPath)) {
//     fs.mkdirSync(folderPath, { recursive: true });
// }
// if (!fs.existsSync(filePath)) {
//     fs.writeFileSync(filePath, JSON.stringify({}, null, 2), 'utf8');
// }
// const namedRoutesJSON = fs.readFileSync(filePath, 'utf8');
// const namedRoutes = JSON.parse(namedRoutesJSON);

// function addRoute(routeName, path) {
//     if (!(routeName in namedRoutes)) {
//         namedRoutes[routeName] = path;
//         const updatedNamedRoutesJSON = JSON.stringify(namedRoutes, null, 2);
//         fs.writeFileSync(filePath, updatedNamedRoutesJSON, 'utf8');
//     } else {
//         throw new Error(`Duplicate route name registration with '${routeName}' detected..`);
//     }
// }
//

function getPath(routeName, params = {}) {
    return generatePath(routeName, params);
}

function generatePath(routeName, params = {}, withDomain = false, prefix) {
    const routePath = namedRoutes[routeName];
    if (!routePath) {
        throw new Error(`Route with name '${routeName}' does not exist.`);
    }
    let generatedPath = routePath;
    for (const paramName in params) {
        generatedPath = generatedPath.replace(`:${paramName}`, params[paramName]);
    }
    if (prefix) {
        generatedPath = `/${prefix + generatedPath}`;
    }
    if (withDomain) {
        const domain = process.env.NODE_URL;
        return domain + generatedPath;
    }
    return generatedPath;
}

function register(path, routeName) {
    addRoute(routeName, path);
    return getPath(routeName);
}

function route(routeName, params = {}, prefix = '') {
    return generatePath(routeName, params, true, prefix);
}

export { register, route };