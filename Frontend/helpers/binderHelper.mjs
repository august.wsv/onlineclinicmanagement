function bindMethodsToThis(instance) {
    for (const methodName of Object.getOwnPropertyNames(Object.getPrototypeOf(instance))) {
        const method = instance[methodName];
        if (typeof method === 'function' && methodName !== 'constructor') {
            instance[methodName] = method.bind(instance);
        }
    }
}

export { bindMethodsToThis };