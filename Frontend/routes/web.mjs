import ClientController from '../controllers/ClientController.mjs';
import AdministratorController from '../controllers/AdministratorController.mjs';
import StaffMemberController from '../controllers/StaffMemberController.mjs';
import { register } from '../helpers/routerHelper.mjs';
import AuthMiddleware from '../middlewares/authMiddleware.mjs';

export default (express) => {
    const router = express.Router();

    router.post('/save-client-token', ClientController.saveClientToken);
    router.post('/remove-client-token', ClientController.removeClientToken);
    router.get(
        register('/', 'homePage'),
        ClientController.renderHome
    );
    router.get(
        register('/login', 'loginPage'),
        AuthMiddleware.clientLogined,
        ClientController.renderLogin
    );
    router.get(
        register('/register', 'registerPage'),
        AuthMiddleware.clientLogined,
        ClientController.renderRegister);
    router.get(
        register('/medicine', 'medicinePage'),
        ClientController.renderMedicine
    );
    router.get(
        register('/medicine-detail/:id', 'medicineDetailPage'),
        ClientController.renderMedicineDetail
    );
    router.get(
        register('/scientific-apparatus', 'scientificApparatusPage'),
        ClientController.renderScientificApparatus
    );
    router.get(
        register('/scientific-apparatus-detail/:id', 'scientificApparatusDetailPage'),
        ClientController.renderScientificApparatusDetail
    );
    router.get(
        register('/subject', 'subjectPage'),
        ClientController.renderSubject
    );
    router.get(
        register('/subject-detail/:id', 'subjectDetailPage'),
        ClientController.renderSubjectDetail
    );
    router.get(
        register('/educational-event-detail/:id', 'educationalEventDetailPage'),
        ClientController.renderEducationalEventDetail
    );
    router.get(
        register('/cart', 'cartPage'),
        AuthMiddleware.requireClientLogin,
        ClientController.renderCart
    );
    router.get(
        register('/order', 'orderListPage'),
        AuthMiddleware.requireClientLogin,
        ClientController.renderOrderList
    );
    router.get(
        register('/order-detail/:id', 'orderDetailPage'),
        AuthMiddleware.requireClientLogin,
        ClientController.renderOrderDetail
    );
    router.get(
        register('/my-account', 'myAccountPage'),
        AuthMiddleware.requireClientLogin,
        ClientController.renderMyAccount
    );
    router.get(
        register('/contact', 'contactPage'),
        AuthMiddleware.requireClientLogin,
        ClientController.renderContact
    );




    router.post('/save-admin-token', AdministratorController.saveAdminToken);
    router.post('/remove-admin-token', AdministratorController.removeAdminToken);
    router.get(
        register('/administrator/login', 'administratorLoginPage'),
        AuthMiddleware.adminLogined,
        AdministratorController.renderAdministratorLogin
    );
    router.get(
        register('/administrator/admin', 'administratorAdminPage'),
        AuthMiddleware.requireAdminLogin,
        AdministratorController.renderAdministratorAdmin
    );
    router.get(
        register('/administrator/staff-member', 'administratorStaffMemberPage'),
        AuthMiddleware.requireAdminLogin,
        AdministratorController.renderAdministratorStaffMember
    );
    router.get(
        register('/administrator/client', 'administratorClientPage'),
        AuthMiddleware.requireAdminLogin,
        AdministratorController.renderAdministratorClient
    );
    router.get(
        register('/administrator/client-message', 'administratorClientMessagePage'),
        AuthMiddleware.requireAdminLogin,
        AdministratorController.renderAdministratorClientMessage
    );
    router.get(
        register('/administrator/clinic', 'administratorClinicPage'),
        AuthMiddleware.requireAdminLogin,
        AdministratorController.renderAdministratorClinic
    );
    router.get(
        register('/administrator/about-us', 'administratorAboutUsPage'),
        AuthMiddleware.requireAdminLogin,
        AdministratorController.renderAdministratorAboutUs
    );
    router.get(
        register('/administrator/medicine-type', 'administratorMedicineTypePage'),
        AuthMiddleware.requireAdminLogin,
        AdministratorController.renderAdministratorMedicineType
    );
    router.get(
        register('/administrator/subject', 'administratorSubjectPage'),
        AuthMiddleware.requireAdminLogin,
        AdministratorController.renderAdministratorSubject
    );
    router.get(
        register('/administrator/medicine', 'administratorMedicine'),
        AuthMiddleware.requireAdminLogin,
        AdministratorController.renderAdministratorMedicine
    );
    router.get(
        register('/administrator/scientific-apparatus', 'administratorScientificApparatus'),
        AuthMiddleware.requireAdminLogin,
        AdministratorController.renderAdministratorScientificApparatus
    );
    router.get(
        register('/administrator/purchase', 'administratorPurchasePage'),
        AuthMiddleware.requireAdminLogin,
        AdministratorController.renderAdministratorPurchase
    );
    router.get(
        register('/administrator/business-report', 'administratorBusinessReportPage'),
        AuthMiddleware.requireAdminLogin,
        AdministratorController.renderAdministratorBusinessReport
    );



    router.post('/save-staff-member-token', StaffMemberController.saveStaffMemberToken);
    router.post('/remove-staff-member-token', StaffMemberController.removeStaffMemberToken);
    router.get(
        register('/staff-member/login', 'staffMemberLoginPage'),
        AuthMiddleware.staffMemberLogined,
        StaffMemberController.renderStaffMemberLogin
    );
    router.get(
        register('/staff-member/', 'subjectAsHomePage'),
        AuthMiddleware.requireStaffMemberLogin,
        StaffMemberController.renderStaffMemberSubjectAsHome
    );
    router.get(
        register('/staff-member/subject-detail/:id', 'subjectDetail2Page'),
        AuthMiddleware.requireStaffMemberLogin,
        StaffMemberController.renderStaffMemberSubjectDetail
    );

    router.get(
        register('/staff-member/educational-event-detail/:id', 'educationalEventDetail2Page'),
        AuthMiddleware.requireStaffMemberLogin,
        StaffMemberController.renderStaffMemberEducationalEventDetail
    );

    router.get(
        register('/staff-member/my-account', 'myAccount2Page'),
        AuthMiddleware.requireStaffMemberLogin,
        StaffMemberController.renderStaffMemberMyAccount
    );

    return router;
};