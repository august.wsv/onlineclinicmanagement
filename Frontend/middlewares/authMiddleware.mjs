import jwt from 'jsonwebtoken';
import { route } from '../helpers/routerHelper.mjs';
class AuthMiddleware {
    requireClientLogin(req, res, next) {
        let isTokenValid = false;
        const token = req.session.clientToken;
        if (token) {
            const secretKey = process.env.JWT_SECRET;
            try {
                const decodedToken = jwt.verify(token, secretKey);
                const personType = decodedToken.PersonType;
                if(personType == 'Client'){
                    isTokenValid = true;
                }  
            } catch (error) { }
        }
        if (!isTokenValid) {
            return res.render('page/client/login');
        }
        return next();
    };

    requireAdminLogin(req, res, next) {
        let isTokenValid = false;
        const token = req.session.adminToken;
        if (token) {
            const secretKey = process.env.JWT_SECRET;
            try {
                const decodedToken = jwt.verify(token, secretKey);
                const personType = decodedToken.PersonType;
                if(personType == 'Admin'){
                    isTokenValid = true;
                }                
            } catch (error) { }
        }
        if (!isTokenValid) {
            return res.render('page/administrator/login');
        }
        return next();
    };

    requireStaffMemberLogin(req, res, next) {
        let isTokenValid = false;
        const token = req.session.staffMemberToken;
        if (token) {
            const secretKey = process.env.JWT_SECRET;
            try {
                const decodedToken = jwt.verify(token, secretKey);
                const personType = decodedToken.PersonType;
                if(personType == 'StaffMember'){
                    isTokenValid = true;
                }                
            } catch (error) { }
        }
        if (!isTokenValid) {
            return res.render('page/staff-member/login');
        }
        return next();
    };

    clientLogined(req, res, next) {
        const token = req.session.clientToken;
        if (token) {
            return res.redirect(route('homePage'));
        }
        return next();        
    }

    adminLogined(req, res, next) {
        const token = req.session.adminToken;
        if (token) {
            return res.redirect(route('administratorBusinessReportPage'));
        }
        return next();        
    }

    staffMemberLogined(req, res, next) {
        const token = req.session.staffMemberToken;
        if (token) {
            return res.redirect(route('subjectAsHomePage'));
        }
        return next();        
    }
}

export default new AuthMiddleware();