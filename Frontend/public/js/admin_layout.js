$(document).on('click', '#logout-btn', function() {
    var settings = {
        "url": `${Application_Server_Address}/remove-admin-token`,
        "method": "POST"
    };
    $.ajax(settings).done();
    localStorage.removeItem('admin_access_token');
    localStorage.removeItem('admin_about');
    window.location.reload();
})

if(localStorage.getItem('admin_access_token')){
    var data = JSON.parse(localStorage.getItem('admin_about'))
    $('#show_name').html(data.name);
    $('#show_role_name').html(data.rolename);
    $('#show_username').html(data.username);
    
}