var settings = {
    "url": `${Application_Server_Address}/remove-staff-member-token`,
    "method": "POST"
};
$.ajax(settings).done();
localStorage.removeItem('staff_member_access_token');
var allInputTypePassword = $("input[type=password]");
var showPasswordButton = $("#showPassword");
var label2 = $('.label2');
var divtoDisplayFlexCustom = $('.divtoDisplayFlexCustom');

showPasswordButton.click(function () {
    allInputTypePassword.each(function () {
        var input = $(this);
        input.attr("type", (input.attr("type") === "password") ? "text" : "password");
    });
});

$('#submitbtn').click(function () {
    var form = new FormData();
    form.append("Email", $('input[name="email"]').val());
    form.append("Password", $('input[name="password"]').val());

    var settings = {
        "url": `${API_Server_Address}/Api/StaffMember/Login`,
        "method": "POST",
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    divtoDisplayFlexCustom.each(function (index) {
        ValidationSucceeded(index);
    });

    $.ajax(settings).done(function (response) {
        alertify.alert(response.message, function () { 
            var settings = {
                "url": `${Application_Server_Address}/save-staff-member-token`,
                "method": "POST",
                "headers": {
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "token": response.meta.token
                }),
            };
            $.ajax(settings).done();

            localStorage.setItem('staff_member_access_token', response.meta.token);
            if (window.location.href.includes("login")) {
                window.location.href = `${Application_Server_Address}/staff-member`
            } else {
                window.location.reload();
            }

        }).setHeader('Notice!');
    }).fail(function (response) {
        alertify.alert(response.responseJSON.message).setHeader('Notice!');
        if (response.responseJSON.errors) {
            
            $.each(response.responseJSON.errors, function (fieldName, errorMessage) {
                var label = $('input[name="' + fieldName.toLowerCase() + '"]').siblings('.label2');
                var index = divtoDisplayFlexCustom.index(label.parent());
                ValidationFailed(index, errorMessage);
            });
        }
    });
});

function ValidationFailed(e, string) {
    label2.eq(e).css('display', 'block').text(string);
    divtoDisplayFlexCustom.eq(e).css('border-color', '#ff000083');
}

function ValidationSucceeded(e) {
    label2.eq(e).css('display', 'none');
    divtoDisplayFlexCustom.eq(e).css('border-color', '#00b8d479');
}
