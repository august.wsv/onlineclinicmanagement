var currentURL = window.location.href;
var match = currentURL.match(/\/subject-detail\/(\d+)/);
var number = match[1];
var data;

var editorCreate;
ClassicEditor
    .create(document.querySelector('#editorCreate'), {
        ckfinder: {}
    })
    .then(newEditor => {
        editorCreate = newEditor;
    })
    .catch(error => {
        editorCreate.error(error);
    });

var editorUpdate;
ClassicEditor
    .create(document.querySelector('#editorUpdate'), {
        ckfinder: {}
    })
    .then(newEditor => {
        editorUpdate = newEditor;
    })
    .catch(error => {
        editorUpdate.error(error);
    });

function init(){
    var settings = {
        "url": `${API_Server_Address}/Api/Subject/GetOne/${number}`,
        "method": "GET",
    };
    
    $.ajax(settings).done(function (response) {
        $('#subTitle').html(response.data.name);
        $('#imagePath').attr('src', response.data.imagePath);
        $('#name').html(response.data.name);
        $('#briefDescription').html(response.data.briefDescription);
        appendOrdersToTab(response, [1], "#lecture-tab");
        appendOrdersToTab(response, [2], "#practical-tab");
        appendOrdersToTab(response, [3], "#seminar-tab");
        data = response.data.educationalEvents;
    });
}
function appendOrdersToTab(response, statuses, tabSelector) {
    var filteredData = response.data.educationalEvents.filter(function (item) {
        return statuses.includes(item.educationalEventType);
    });
    var tab = $(tabSelector);
    tab.html('');
    if (filteredData.length > 0) {
        filteredData.forEach(function (object) {
            tab.append(educationalEventHtml2(object));
        });
    } else {
        tab.append(`
            <tr>
                <th colspan="3">Lesson not found</th>
            </tr>
        `);
    }
}

init();

let rememberLocation = 0;
let navigationItem = $('.navigationItem');
let tabContent = $('.tabContent');

function clickNavigationItemEvent(index) {
    navigationItem.eq(index).click(function () {
        if (index !== rememberLocation) {
            navigationItem.eq(index).css('background-color', '#171d1b');
            tabContent.eq(index).show();

            navigationItem.eq(rememberLocation).css('background-color', '#505050');
            tabContent.eq(rememberLocation).hide();
            rememberLocation = index;
        }
    });
}

navigationItem.each(function (index) {
    clickNavigationItemEvent(index);
});

$('#create').click(function () {
    $('#createEEModal').show();
});

$('#closeCreateModal').click(function () {
    $('#createEEModal').hide();
    $('#createEEModal input, #createEEModal select').val('');
});

$('#saveCreateEE').click(function () {
    var form = new FormData();
    form.append("name", $('#createForm input[name="name"]').val());
    form.append("videoPath", $('#createForm input[name="videoPath"]').prop('files')[0]);
    form.append("description", editorCreate.getData());
    form.append("briefDescription", $('#createForm input[name="briefDescription"]').val());
    form.append("educationalEventType", $('#createForm select[name="educationalEventType"]').val());
    form.append("subjectId", number);

    var settings = {
        "url": "https://localhost/Api/EducationalEvent/Create",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('staff_member_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        $('#createEEModal').hide();
        $('#createEEModal input, #createEEModal select').val('');
        alertify.alert(response.message).setHeader('Thank!!!');
        init();
    }).fail(function (response) {
        const errors = response.responseJSON.errors;
        let errorString = "";
        for (const key in errors) {
            if (errors.hasOwnProperty(key)) {
                errorString += `<b>${key}:</b> ${errors[key]} <br>`;
            }
        }
        alertify.alert(`${response.responseJSON.message}${errors ? `: <br>${errorString}` : ''}`).setHeader(errors ? 'Notice!' : '');
    });
})


$(document).on('click', '.updateEE', function () {
    id = $(this).val();
    var filteredData = data.find(function (item) {
        return item.id == id;
    });
    $('#updateEEModal').show();
    $('#updateForm input[name="name"]').val(filteredData.name);
    editorUpdate.setData(filteredData.description)
    $('#updateForm input[name="briefDescription"]').val(filteredData.briefDescription);
    $('#updateForm select[name="educationalEventType"]').val(filteredData.educationalEventType);
});

$('#closeUpdateModal').click(function () {
    $('#updateEEModal').hide();
    $('#updateEEModal input, #updateEEModal select').val('');
});

$('#saveUpdateEE').click(function () {
    var form = new FormData();
    form.append("name", $('#updateForm input[name="name"]').val());
    form.append("videoPath", $('#updateForm input[name="videoPath"]').prop('files')[0]);
    form.append("description", editorUpdate.getData());
    form.append("briefDescription", $('#updateForm input[name="briefDescription"]').val());
    form.append("educationalEventType", $('#updateForm select[name="educationalEventType"]').val());
    form.append("subjectId", number);


    var settings = {
        "url": `${API_Server_Address}/Api/EducationalEvent/Update/${id}`,
        "method": "PUT",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('staff_member_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        $('#updateEEModal').hide();
        $('#updateEEModal input, #updateEEModal select').val('');
        alertify.alert(response.message).setHeader('Thank!!!');
        init();
    }).fail(function (response) {
        const errors = response.responseJSON.errors;
        let errorString = "";
        for (const key in errors) {
            if (errors.hasOwnProperty(key)) {
                errorString += `<b>${key}:</b> ${errors[key]} <br>`;
            }
        }
        alertify.alert(`${response.responseJSON.message}${errors ? `: <br>${errorString}` : ''}`).setHeader(errors ? 'Notice!' : '');
    });
});


$(document).on('click', '.deleteEE', function () {
    var id = $(this).val();
    var name = $(this).closest('tr').find('.name').text();
    alertify.confirm(`Do you want delete remove this education event: ${name}`,
        function () {
            var settings = {
                "url": `${API_Server_Address}/Api/EducationalEvent/Delete/${id}`,
                "method": "DELETE",
                "headers": {
                    "Authorization": `Bearer ${localStorage.getItem('staff_member_access_token')}`
                },
                "dataType": "json"
            };

            $.ajax(settings).done(function (response) {
                alertify.alert(response.message).setHeader('Notice!!!');
                init();
            }).fail(function (response) {
                alertify.alert(response.responseJSON.message).setHeader('Notice!!!');
            });
        },
        function () {
            //Cancel
        }).setHeader('Notice!');
});