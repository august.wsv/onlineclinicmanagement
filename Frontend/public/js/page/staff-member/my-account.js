var id;
function Init() {
    var settings = {
        "url": `${API_Server_Address}/Api/StaffMember/AboutStaffMember`,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('staff_member_access_token')}`
        },
        "dataType": "json"
    };
    $.ajax(settings).done(function (response) {
        $('#personalBiography').html(response.data.personalBiography);
        id = response.data.id;
        $('textarea[name="personalBiography"]').val(response.data.personalBiography);
    });
}


Init();

$(document).on('click', '#editButton', function () {
    toggleEdit()
})

$(document).on('click', '#cancelButton', function () {
    cancelEdit()
})

function toggleEdit() {
    $('.editable').show();
    $('.non-editable').hide();
    $('#editButton').hide();
    $('#cancelButton').show();
    $('#saveButton').show();
}

function cancelEdit() {
    $('.editable').hide();
    $('.non-editable').show();
    $('#cancelButton').hide();
    $('#saveButton').hide();
    $('#editButton').show();
}


$(document).on('click', '#saveButton', function () {
    var form = new FormData();
    form.append("personalBiography", $('textarea[name="personalBiography"]').val());

    var settings = {
        "url": `${API_Server_Address}/Api/StaffMember/Update/${id}`,
        "method": "PUT",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('staff_member_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        Init();
        cancelEdit()
    }).fail(function (response) {
        alertify.alert(response.responseJSON.message).setHeader('Notice!');
        if (response.responseJSON.errors) {
            
            $.each(response.responseJSON.errors, function (fieldName, errorMessage) {
                var label = $('input[name="' + fieldName.toLowerCase() + '"]').siblings('.label2');
                var index = divtoDisplayFlexCustom.index(label.parent());
                ValidationFailed(index, errorMessage);
            });
        }
    });
})



