var data;
function init() {
    var settings = {
        "url": `${API_Server_Address}/Api/AboutUs/GetAll`,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

init();

function ajaxLoad(settings) {
    $.ajax(settings).done(function (response) {
        var insertData = $('#insert_data');
        insertData.html('')
        response.data.forEach(function (object) {
            insertData.append(aboutUsHtml2(object));
        });
        data = response.data;
    });
}

function loadData(url) {
    $('#insert_data').html('');
    var settings = {
        "url": url,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

var id;
$(document).on('click', '.updateAboutUs', function () {
    id = $(this).val();
    var filteredData = data.find(function (item) {
        return item.id == id;
    });
    $('#updateAboutUsModal').modal('show');    
    $('#updateForm input[name="description"]').val(filteredData.description);
});

$('#saveUpdateAboutUs').click(function () {
    var form = new FormData();
    form.append("description", $('#updateForm input[name="description"]').val());

    var settings = {
        "url": `${API_Server_Address}/Api/AboutUs/Update/${id}`,
        "method": "PUT",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        $('#updateAboutUsModal').modal('hide');
        $('#updateAboutUsModal input, #updateAboutUsModal select').val('');
        alertify.alert(response.message).setHeader('Thank!!!');
        init();
    }).fail(function (response) {
        const errors = response.responseJSON.errors;
        let errorString = "";
        for (const key in errors) {
            if (errors.hasOwnProperty(key)) {
                errorString += `<b>${key}:</b> ${errors[key]} <br>`;
            }
        }
        alertify.alert(`${response.responseJSON.message}${errors ? `: <br>${errorString}` : ''}`).setHeader(errors ? 'Notice!' : '');
    });
});

$('#closeUpdateModal').click(function () {
    $('#updateAboutUsModal').modal('hide');
    $('#updateAboutUsModal input, #updateAboutUsModal select').val('');
});