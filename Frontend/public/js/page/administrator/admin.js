var defaultPageSize = '?pageSize=10'
init(defaultPageSize);
var meta;
var data;
function init(param) {
    var settings = {
        "url": `${API_Server_Address}/Api/Admin/GetList${param}`,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

function ajaxLoad(settings) {
    $.ajax(settings).done(function (response) {
        var insertData = $('#insert_data');
        insertData.html('')
        response.data.forEach(function (object) {
            insertData.append(adminHtml(object));
        });
        meta = response.meta;
        data = response.data;

        $('#page-now').html(`${meta.page}/${meta.pageCount}`);

        if (meta.prevPageLink == null) {
            $('#prevPageLink').prop("disabled", true);
        } else {
            $('#prevPageLink').prop("disabled", false);
        }

        if (meta.nextPageLink == null) {
            $('#nextPageLink').prop("disabled", true);
        } else {
            $('#nextPageLink').prop("disabled", false);
        }
    });
}

$('#search').click(function () {
    let param = defaultPageSize;
    $('#insert_data').html('');

    const filterFields = [
        'name',
        'username'
    ];

    for (const field of filterFields) {
        const value = $(`input[name="${field}"], select[name="${field}"]`).val();
        if (value !== '') {
            param += `&${field}=${value}`;
        }
    }
    if (param !== defaultPageSize) {
        param = `${param}`;
    }
    init(param);
});

$('#reset').click(function () {
    $('#form-search input, #form-search select').val('');
    $('#insert_data').html('');
    init(defaultPageSize);
});

function loadData(url) {
    $('#insert_data').html('');
    var settings = {
        "url": url,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

$('#firstPageLink').click(function () {
    loadData(meta.firstPageLink);
});

$('#prevPageLink').click(function () {
    loadData(meta.prevPageLink);
});

$('#nextPageLink').click(function () {
    loadData(meta.nextPageLink);
});

$('#lastPageLink').click(function () {
    loadData(meta.lastPageLink);
});



$('#create').click(function () {
    $('#createAdminModal').modal('show');
});

$('#saveAdmin').click(function () {
    var form = new FormData();
    form.append("name", $('#name').val());
    form.append("username", $('#username').val());
    form.append("password", $('#password').val());

    var settings = {
        "url": `${API_Server_Address}/Api/Admin/Register`,
        "method": "POST",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        $('#createAdminModal').modal('hide');
        $('#createAdminModal input, #createAdminModal select').val('');
        alertify.alert(response.message).setHeader('Thank!!!');
        init(defaultPageSize);
    }).fail(function (response) {
        const errors = response.responseJSON.errors;
        let errorString = "";
        for (const key in errors) {
            if (errors.hasOwnProperty(key)) {
                errorString += `<b>${key}:</b> ${errors[key]} <br>`;
            }
        }
        alertify.alert(`${response.responseJSON.message}: <br>${errorString}`).setHeader('Notice!');
    });
});

$('#closeModal').click(function () {
    $('#createAdminModal').modal('hide');
    $('#createAdminModal input, #createAdminModal select').val('');
});

var id;
$(document).on('click', '.updateAccount', function () {
    id = $(this).val();
    var filteredData = data.find(function (item) {
        return item.id == id;
    });
    $('#updateAdminModal').modal('show');
    $('#updateForm input[name="name"]').val(filteredData.name);
    $('#updateForm input[name="username"]').val(filteredData.username);
});

$('#saveUpdateStaffMember').click(function () {
    var form = new FormData();
    form.append("name", $('#updateForm input[name="name"]').val());
    form.append("username", $('#updateForm input[name="username"]').val());
    if ($('#updateForm input[name="password"]').val() !== '') {
        form.append("password", $('#updateForm input[name="password"]').val());
    }


    var settings = {
        "url": `${API_Server_Address}/Api/Admin/Update/${id}`,
        "method": "PUT",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        $('#updateAdminModal').modal('hide');
        $('#updateAdminModal input, #updateAdminModal select').val('');
        alertify.alert(response.message).setHeader('Thank!!!');
        init(defaultPageSize);
    }).fail(function (response) {
        const errors = response.responseJSON.errors;
        let errorString = "";
        for (const key in errors) {
            if (errors.hasOwnProperty(key)) {
                errorString += `<b>${key}:</b> ${errors[key]} <br>`;
            }
        }
        alertify.alert(`${response.responseJSON.message}${errors ? `: <br>${errorString}` : ''}`).setHeader(errors ? 'Notice!' : '');
    });
});

$('#closeUpdateModal').click(function () {
    $('#updateAdminModal').modal('hide');
    $('#updateAdminModal input, #updateAdminModal select').val('');
});


$(document).on('click', '.deleteAccount', function () {
    var id = $(this).val();
    var name = $(this).closest('tr').find('.name').text();
    alertify.confirm(`Do you want delete remove this account: ${name}`,
        function () {
            var settings = {
                "url": `${API_Server_Address}/Api/Admin/Delete/${id}`,
                "method": "DELETE",
                "headers": {
                    "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
                },
                "dataType": "json"
            };

            $.ajax(settings).done(function (response) {
                alertify.alert(response.message).setHeader('Notice!!!');
                init(defaultPageSize);
            }).fail(function (response) {
                alertify.alert(response.responseJSON.message).setHeader('Notice!!!');
            });
        },
        function () {
            //Cancel
        }).setHeader('Notice!');
});