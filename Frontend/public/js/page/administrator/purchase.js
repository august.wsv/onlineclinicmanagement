var defaultPageSize = '?pageSize=10'
init(defaultPageSize);
var meta;
var data;
function init(param) {
    var settings = {
        "url": `${API_Server_Address}/Api/Purchase/GetList${param}`,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

function ajaxLoad(settings) {
    $.ajax(settings).done(function (response) {
        var insertData = $('#insert_data');
        insertData.html('')
        response.data.forEach(function (object) {
            insertData.append(purchaseHtml2(object));
        });
        meta = response.meta;
        data = response.data;

        $('#page-now').html(`${meta.page}/${meta.pageCount}`);

        if (meta.prevPageLink == null) {
            $('#prevPageLink').prop("disabled", true);
        } else {
            $('#prevPageLink').prop("disabled", false);
        }

        if (meta.nextPageLink == null) {
            $('#nextPageLink').prop("disabled", true);
        } else {
            $('#nextPageLink').prop("disabled", false);
        }
    });
}

$('#search').click(function () {
    let param = defaultPageSize;
    $('#insert_data').html('');
    const filterFields = [
        'clientName',
        'clientEmail',
        'status',
        'address',
        'minCreatedAt',
        'maxCreatedAt'
    ];

    for (const field of filterFields) {
        const value = $(`#form-search input[name="${field}"], #form-search select[name="${field}"]`).val();
        if (value !== '') {
            param += `&${field}=${value}`;
        }
    }
    if (param !== defaultPageSize) {
        param = `${param}`;
    }
    init(param);
});

$('#reset').click(function () {
    $('#form-search input, #form-search select').val('');
    $('#insert_data').html('');
    init(defaultPageSize);
});

function loadData(url) {
    $('#insert_data').html('');
    var settings = {
        "url": url,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

$('#firstPageLink').click(function () {
    loadData(meta.firstPageLink);
});

$('#prevPageLink').click(function () {
    loadData(meta.prevPageLink);
});

$('#nextPageLink').click(function () {
    loadData(meta.nextPageLink);
});

$('#lastPageLink').click(function () {
    loadData(meta.lastPageLink);
});

var id;
$(document).on('click', '.updatePurchase', function () {
    id = $(this).val();
    var filteredData = data.find(function (item) {
        return item.id == id;
    });
    if(filteredData.status == 0){
        $('select[name="status"] option[value="1"]').show();
        $('select[name="status"] option[value="2"]').hide();
        $('select[name="status"] option[value="3"]').show();
        $('select[name="status"] option[value="5"]').hide();
    } else if (filteredData.status == 1){
        $('select[name="status"] option[value="1"]').hide();
        $('select[name="status"] option[value="2"]').show();
        $('select[name="status"] option[value="4"]').hide();
        $('select[name="status"] option[value="5"]').show();
    } else if(filteredData.status == 2 || filteredData.status == 4){
        $('select[name="status"] option[value="1"]').hide();
        $('select[name="status"] option[value="2"]').hide();
        $('select[name="status"] option[value="4"]').hide();
    } else if(filteredData.status == 5 || filteredData.status == 6){
        $('select[name="status"] option[value="1"]').hide();
        $('select[name="status"] option[value="2"]').hide();
        $('select[name="status"] option[value="3"]').hide();
        $('select[name="status"] option[value="4"]').hide();
        $('select[name="status"] option[value="5"]').hide();
    }
    $('#updatePurchaseModal').modal('show');
});
$('#saveUpdatePurchase').click(function () {
    var form = new FormData();
    form.append("status", $('#updateForm select[name="status"]').val());

    var settings = {
        "url": `${API_Server_Address}/Api/Purchase/Update/${id}`,
        "method": "PUT",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        $('#updatePurchaseModal').modal('hide');
        $('#updatePurchaseModal select').val('Choice status');
        alertify.alert(response.message).setHeader('Thank!!!');
        init(defaultPageSize);
    }).fail(function (response) {
        const errors = response.responseJSON.errors;
        let errorString = "";
        for (const key in errors) {
            if (errors.hasOwnProperty(key)) {
                errorString += `<b>${key}:</b> ${errors[key]} <br>`;
            }
        }
        alertify.alert(`${response.responseJSON.message}${errors ? `: <br>${errorString}` : ''}`).setHeader(errors ? 'Notice!' : '');
    });
});

$('#closeUpdateModal').click(function () {
    $('#updatePurchaseModal').modal('hide');
    $('#updatePurchaseModal select').val('Choice status');
});

$(document).on('click', '.getOnePurchase', function () {
    var id = $(this).val();
    var settings = {
        "url": `${API_Server_Address}/Api/Purchase/GetOne/${id}`,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        $('#name').html(response.data.aboutClient.name)
        $('#phone').html(response.data.aboutClient.phone)
        $('#email').html(response.data.aboutClient.email)
        $('#address').html(response.data.address)
        $('#createdAt').html(moment(response.data.createdAt).format('YYYY/MM/DD HH:mm:ss'))
        $('#total').html(response.data.totalString);
        $('#show_data').html(null)
        $.each(response.data.purchaseDetails, function(index, purchaseDetail) {
            $('#show_data').append(purchaseDetailHtml(purchaseDetail));
        });
        $('#viewPurchaseModal').modal('show');
    });
});

$('#closeViewModal').click(function () {
    $('#viewPurchaseModal').modal('hide');
});