var defaultPageSize = '?pageSize=10'
init(defaultPageSize);
var meta;
var data;
function init(param) {
    var settings = {
        "url": `${API_Server_Address}/Api/Medicine/GetList${param}`,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

var editorCreate;
ClassicEditor
    .create(document.querySelector('#editorCreate'), {
        ckfinder: {}
    })
    .then(newEditor => {
        editorCreate = newEditor;
    })
    .catch(error => {
        editorCreate.error(error);
    });

var editorUpdate;
ClassicEditor
    .create(document.querySelector('#editorUpdate'), {
        ckfinder: {}
    })
    .then(newEditor => {
        editorUpdate = newEditor;
    })
    .catch(error => {
        editorUpdate.error(error);
    });


function ajaxLoad(settings) {
    $.ajax(settings).done(function (response) {
        var insertData = $('#insert_data');
        insertData.html('')
        response.data.forEach(function (object) {
            insertData.append(medicineHtml2(object));
        });
        meta = response.meta;
        data = response.data;

        $('#page-now').html(`${meta.page}/${meta.pageCount}`);

        if (meta.prevPageLink == null) {
            $('#prevPageLink').prop("disabled", true);
        } else {
            $('#prevPageLink').prop("disabled", false);
        }

        if (meta.nextPageLink == null) {
            $('#nextPageLink').prop("disabled", true);
        } else {
            $('#nextPageLink').prop("disabled", false);
        }
    });
}

var settings = {
    "url": `${API_Server_Address}/Api/MedicineType/GetAll`,
    "method": "GET",
    "headers": {
        "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
    },
    "dataType": "json"
};

$.ajax(settings).done(function (response) {
    var select = $('select[name="medicineTypeId"]');
    response.data.forEach(function (object) {
        select.append(medicineTypeHtml(object));
    });
});

$('#search').click(function () {
    let param = defaultPageSize;
    $('#insert_data').html('');

    const filterFields = [
        'name',
        'code',
        'medicineTypeId',
        'minPrice',
        'maxPrice',
        'minExpDate',
        'maxExpDate'
    ];

    for (const field of filterFields) {
        const value = $(`#form-search input[name="${field}"], #form-search select[name="${field}"]`).val();
        if (value !== '') {
            param += `&${field}=${value}`;
        }
    }
    if (param !== defaultPageSize) {
        param = `${param}`;
    }
    init(param);
});

$('#reset').click(function () {
    $('#form-search input, #form-search select').val('');
    $('#insert_data').html('');
    init(defaultPageSize);
});

function loadData(url) {
    $('#insert_data').html('');
    var settings = {
        "url": url,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

$('#firstPageLink').click(function () {
    loadData(meta.firstPageLink);
});

$('#prevPageLink').click(function () {
    loadData(meta.prevPageLink);
});

$('#nextPageLink').click(function () {
    loadData(meta.nextPageLink);
});

$('#lastPageLink').click(function () {
    loadData(meta.lastPageLink);
});



$('#create').click(function () {
    $('#createMedicineModal').modal('show');
});
$('#saveCreateMedicine').click(function () {
    var form = new FormData();
    form.append("name", $('#createForm input[name="name"]').val());
    form.append("code", $('#createForm input[name="code"]').val());
    form.append("price", $('#createForm input[name="price"]').val());
    form.append("quantity", $('#createForm input[name="quantity"]').val());
    form.append("expDate", $('#createForm input[name="expDate"]').val());
    form.append("medicineTypeId", $('#createForm select[name="medicineTypeId"]').val());
    form.append("imagePath", $('#createForm input[name="imagePath"]').prop('files')[0]);
    form.append("description", editorCreate.getData());


    var settings = {
        "url": `${API_Server_Address}/Api/Medicine/Create`,
        "method": "POST",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        $('#createMedicineModal').modal('hide');
        $('#createMedicineModal input, #createMedicineModal select').val('');
        alertify.alert(response.message).setHeader('Thank!!!');
        init(defaultPageSize);
    }).fail(function (response) {
        const errors = response.responseJSON.errors;
        let errorString = "";
        for (const key in errors) {
            if (errors.hasOwnProperty(key)) {
                errorString += `<b>${key}:</b> ${errors[key]} <br>`;
            }
        }
        alertify.alert(`${response.responseJSON.message}: <br>${errorString}`).setHeader('Notice!');
    });
});

$('#closeCreateModal').click(function () {
    $('#createMedicineModal').modal('hide');
    $('#createMedicineModal input, #createMedicineModal select').val('');
});


$(document).on('click', '.deleteMedicine', function () {
    var id = $(this).val();
    var name = $(this).closest('tr').find('.name').text();
    alertify.confirm(`Do you want delete remove this account: ${name}`,
        function () {
            var settings = {
                "url": `${API_Server_Address}/Api/Medicine/Delete/${id}`,
                "method": "DELETE",
                "headers": {
                    "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
                },
                "dataType": "json"
            };

            $.ajax(settings).done(function (response) {
                alertify.alert(response.message).setHeader('Notice!!!');
                init(defaultPageSize);
            }).fail(function (response) {
                alertify.alert(response.responseJSON.message).setHeader('Notice!!!');
            });
        },
        function () {
            //Cancel
        }).setHeader('Notice!');
});

var id;
$(document).on('click', '.updateMedicine', function () {
    id = $(this).val();
    var filteredData = data.find(function (item) {
        return item.id == id;
    });
    $('#updateMedicineModal').modal('show');
    $('#updateForm input[name="name"]').val(filteredData.name);
    $('#updateForm input[name="code"]').val(filteredData.code);
    $('#updateForm input[name="price"]').val(filteredData.price);
    $('#updateForm input[name="quantity"]').val(filteredData.quantity);
    $('#updateForm input[name="expDate"]').val(filteredData.expDate);
    $('#updateForm select[name="medicineTypeId"]').val(filteredData.medicineTypeId);
    $('#updateForm input[name="name"]').val(filteredData.name);
    editorUpdate.setData(filteredData.description)
});

$('#saveUpdateMedicine').click(function () {
    var form = new FormData();
    form.append("name", $('#updateForm input[name="name"]').val());
    form.append("code", $('#updateForm input[name="code"]').val());
    form.append("price", $('#updateForm input[name="price"]').val());
    form.append("quantity", $('#updateForm input[name="quantity"]').val());
    form.append("expDate", $('#updateForm input[name="expDate"]').val());
    form.append("medicineTypeId", $('#updateForm select[name="medicineTypeId"]').val());
    form.append("imagePath", $('#updateForm input[name="imagePath"]').prop('files')[0]);
    form.append("description", editorUpdate.getData());


    var settings = {
        "url": `${API_Server_Address}/Api/Medicine/Update/${id}`,
        "method": "PUT",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        $('#updateMedicineModal').modal('hide');
        $('#updateMedicineModal input, #updateMedicineModal select, #updateMedicineModal textarea').val('');
        alertify.alert(response.message).setHeader('Thank!!!');
        init(defaultPageSize);
    }).fail(function (response) {
        const errors = response.responseJSON.errors;
        let errorString = "";
        for (const key in errors) {
            if (errors.hasOwnProperty(key)) {
                errorString += `<b>${key}:</b> ${errors[key]} <br>`;
            }
        }
        alertify.alert(`${response.responseJSON.message}${errors ? `: <br>${errorString}` : ''}`).setHeader(errors ? 'Notice!' : '');
    });
});

$('#closeUpdateModal').click(function () {
    $('#updateMedicineModal').modal('hide');
    $('#updateMedicineModal input, #updateMedicineModal select, #updateMedicineModal textarea').val('');
});