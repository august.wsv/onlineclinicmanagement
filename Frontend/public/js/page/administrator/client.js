var defaultPageSize = '?pageSize=10'
init(defaultPageSize);
var meta;
var data;
function init(param) {
    var settings = {
        "url": `${API_Server_Address}/Api/Client/GetList${param}`,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

function ajaxLoad(settings) {
    $.ajax(settings).done(function (response) {
        var insertData = $('#insert_data');
        insertData.html('')
        response.data.forEach(function (object) {
            insertData.append(clientHtml(object));
        });
        meta = response.meta;
        data = response.data;

        $('#page-now').html(`${meta.page}/${meta.pageCount}`);

        if (meta.prevPageLink == null) {
            $('#prevPageLink').prop("disabled", true);
        } else {
            $('#prevPageLink').prop("disabled", false);
        }

        if (meta.nextPageLink == null) {
            $('#nextPageLink').prop("disabled", true);
        } else {
            $('#nextPageLink').prop("disabled", false);
        }
    });
}

$('#search').click(function () {
    let param = defaultPageSize;
    $('#insert_data').html('');

    const filterFields = [
        'name',
        'phone',
        'email',
        'address',
        'clinicName'
    ];

    for (const field of filterFields) {
        const value = $(`#form-search input[name="${field}"], #form-search select[name="${field}"]`).val();
        if (value !== '') {
            param += `&${field}=${value}`;
        }
    }
    if (param !== defaultPageSize) {
        param = `${param}`;
    }
    init(param);
});

$('#reset').click(function () {
    $('#form-search input, #form-search select').val('');
    $('#insert_data').html('');
    init(defaultPageSize);
});

function loadData(url) {
    $('#insert_data').html('');
    var settings = {
        "url": url,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

$('#firstPageLink').click(function () {
    loadData(meta.firstPageLink);
});

$('#prevPageLink').click(function () {
    loadData(meta.prevPageLink);
});

$('#nextPageLink').click(function () {
    loadData(meta.nextPageLink);
});

$('#lastPageLink').click(function () {
    loadData(meta.lastPageLink);
});

$(document).on('click', '.deleteAccount', function () {
    var id = $(this).val();
    var name = $(this).closest('tr').find('.name').text();
    alertify.confirm(`Do you want delete remove this account: ${name}`,
        function () {
            var settings = {
                "url": `${API_Server_Address}/Api/Client/Delete/${id}`,
                "method": "DELETE",
                "headers": {
                    "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
                },
                "dataType": "json"
            };

            $.ajax(settings).done(function (response) {
                alertify.alert(response.message).setHeader('Notice!!!');
                init(defaultPageSize);
            }).fail(function (response) {
                alertify.alert(response.responseJSON.message).setHeader('Notice!!!');
            });
        },
        function () {
            //Cancel
        }).setHeader('Notice!');
});