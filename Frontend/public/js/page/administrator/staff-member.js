var defaultPageSize = '?pageSize=10'
init(defaultPageSize);
var meta;
var data;
function init(param) {
    var settings = {
        "url": `${API_Server_Address}/Api/StaffMember/GetList${param}`,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

function ajaxLoad(settings) {
    $.ajax(settings).done(function (response) {
        var insertData = $('#insert_data');
        insertData.html('')
        response.data.forEach(function (object) {
            insertData.append(staffMemberHtml(object));
        });
        meta = response.meta;
        data = response.data;

        $('#page-now').html(`${meta.page}/${meta.pageCount}`);

        if (meta.prevPageLink == null) {
            $('#prevPageLink').prop("disabled", true);
        } else {
            $('#prevPageLink').prop("disabled", false);
        }

        if (meta.nextPageLink == null) {
            $('#nextPageLink').prop("disabled", true);
        } else {
            $('#nextPageLink').prop("disabled", false);
        }
    });
}

$('#search').click(function () {
    let param = defaultPageSize;
    $('#insert_data').html('');

    const filterFields = [
        'name',
        'phone',
        'email'
    ];

    for (const field of filterFields) {
        const value = $(`#form-search input[name="${field}"], #form-search select[name="${field}"]`).val();
        if (value !== '') {
            param += `&${field}=${value}`;
        }
    }
    if (param !== defaultPageSize) {
        param = `${param}`;
    }
    init(param);
});

$('#reset').click(function () {
    $('#form-search input, #form-search select').val('');
    $('#insert_data').html('');
    init(defaultPageSize);
});

function loadData(url) {
    $('#insert_data').html('');
    var settings = {
        "url": url,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

$('#firstPageLink').click(function () {
    loadData(meta.firstPageLink);
});

$('#prevPageLink').click(function () {
    loadData(meta.prevPageLink);
});

$('#nextPageLink').click(function () {
    loadData(meta.nextPageLink);
});

$('#lastPageLink').click(function () {
    loadData(meta.lastPageLink);
});



$('#create').click(function () {
    $('#createStaffMemberModal').modal('show');
});

$('#saveCreateStaffMember').click(function () {
    var form = new FormData();
    form.append("name", $('#createForm input[name="name"]').val());
    form.append("phone", $('#createForm input[name="phone"]').val());
    form.append("email", $('#createForm input[name="email"]').val());
    form.append("password", $('#createForm input[name="password"]').val());

    var settings = {
        "url": `${API_Server_Address}/Api/StaffMember/Register`,
        "method": "POST",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        $('#createStaffMemberModal').modal('hide');
        $('#createStaffMemberModal input, #createStaffMemberModal select').val('');
        alertify.alert(response.message).setHeader('Thank!!!');
        init(defaultPageSize);
    }).fail(function (response) {
        const errors = response.responseJSON.errors;
        let errorString = "";
        for (const key in errors) {
            if (errors.hasOwnProperty(key)) {
                errorString += `<b>${key}:</b> ${errors[key]} <br>`;
            }
        }
        alertify.alert(`${response.responseJSON.message}: <br>${errorString}`).setHeader('Notice!');
    });
});

$('#closeCreateModal').click(function () {
    $('#createStaffMemberModal').modal('hide');
    $('#createStaffMemberModal input, #createStaffMemberModal select').val('');
});


$(document).on('click', '.deleteAccount', function () {
    var id = $(this).val();
    var name = $(this).closest('tr').find('.name').text();
    alertify.confirm(`Do you want delete remove this account: ${name}`,
        function () {
            var settings = {
                "url": `${API_Server_Address}/Api/StaffMember/Delete/${id}`,
                "method": "DELETE",
                "headers": {
                    "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
                },
                "dataType": "json"
            };

            $.ajax(settings).done(function (response) {
                alertify.alert(response.message).setHeader('Notice!!!');
                init(defaultPageSize);
            }).fail(function (response) {
                alertify.alert(response.responseJSON.message).setHeader('Notice!!!');
            });
        },
        function () {
            //Cancel
        }).setHeader('Notice!');
});

var id;
$(document).on('click', '.updateAccount', function () {
    id = $(this).val();
    var filteredData = data.find(function (item) {
        return item.id == id;
    });
    $('#updateStaffMemberModal').modal('show');
    $('#updateForm input[name="name"]').val(filteredData.name);
    $('#updateForm input[name="phone"]').val(filteredData.phone);
    $('#updateForm input[name="email"]').val(filteredData.email);
});

$('#saveUpdateStaffMember').click(function () {
    var form = new FormData();
    form.append("name", $('#updateForm input[name="name"]').val());
    form.append("phone", $('#updateForm input[name="phone"]').val());
    form.append("email", $('#updateForm input[name="email"]').val());
    if ($('#updateForm input[name="password"]').val() !== '') {
        form.append("password", $('#updateForm input[name="password"]').val());
    }


    var settings = {
        "url": `${API_Server_Address}/Api/StaffMember/Update/${id}`,
        "method": "PUT",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        $('#updateStaffMemberModal').modal('hide');
        $('#updateStaffMemberModal input, #updateStaffMemberModal select').val('');
        alertify.alert(response.message).setHeader('Thank!!!');
        init(defaultPageSize);
    }).fail(function (response) {
        const errors = response.responseJSON.errors;
        let errorString = "";
        for (const key in errors) {
            if (errors.hasOwnProperty(key)) {
                errorString += `<b>${key}:</b> ${errors[key]} <br>`;
            }
        }
        alertify.alert(`${response.responseJSON.message}${errors ? `: <br>${errorString}` : ''}`).setHeader(errors ? 'Notice!' : '');
    });
});

$('#closeUpdateModal').click(function () {
    $('#updateStaffMemberModal').modal('hide');
    $('#updateStaffMemberModal input, #updateStaffMemberModal select').val('');
});