function optionsFn(element, data, categories, name) {
    var height = parseInt(KTUtil.css(element, 'height'));
    var labelColor = KTUtil.getCssVariableValue('--kt-gray-500');
    var borderColor = KTUtil.getCssVariableValue('--kt-gray-200');
    var baseColor = KTUtil.getCssVariableValue('--kt-info');
    var options = {
        series: [{
            name: name,
            data: data
        }],
        chart: {
            fontFamily: 'inherit',
            type: 'area',
            height: height,
            toolbar: {
                show: false
            }
        },
        plotOptions: {

        },
        legend: {
            show: false
        },
        dataLabels: {
            enabled: false
        },
        fill: {
            type: 'solid',
            opacity: 1
        },
        stroke: {
            curve: 'smooth',
            show: true,
            width: 3,
            colors: ['#00ffa6']
        },
        xaxis: {
            categories: categories,
            axisBorder: {
                show: false,
            },
            axisTicks: {
                show: false
            },
            labels: {
                style: {
                    colors: labelColor,
                    fontSize: '12px'
                }
            },
            crosshairs: {
                position: 'front',
                stroke: {
                    color: baseColor,
                    width: 1,
                    dashArray: 3
                }
            },
            tooltip: {
                enabled: true,
                formatter: undefined,
                offsetY: 0,
                style: {
                    fontSize: '12px'
                }
            }
        },
        yaxis: {
            labels: {
                style: {
                    colors: labelColor,
                    fontSize: '12px'
                }
            }
        },
        states: {
            normal: {
                filter: {
                    type: 'none',
                    value: 0
                }
            },
            hover: {
                filter: {
                    type: 'none',
                    value: 0
                }
            },
            active: {
                allowMultipleDataPointsSelection: false,
                filter: {
                    type: 'none',
                    value: 0
                }
            }
        },
        tooltip: {
            style: {
                fontSize: '12px'
            },
            y: {
                formatter: function (val) {
                    return '$' + val + ' thousands'
                }
            }
        },
        colors: ['#f5fffc'],
        grid: {
            borderColor: borderColor,
            strokeDashArray: 4,
            yaxis: {
                lines: {
                    show: true
                }
            }
        },
        markers: {
            strokeColor: baseColor,
            strokeWidth: 3
        }
    };
    return options;
}





var settings = {
    "url": "https://localhost/Api/BusinessReport/GetThisWeekReport",
    "method": "GET",
    "timeout": 0,
};

$.ajax(settings).done(function (response) {
    const totalSalesArray = response.data.map(item => item.totalSales);
    var weeklyElement = document.getElementById('kt_apexcharts_weekly');
    var weeklyChart = new ApexCharts(
        weeklyElement,
        optionsFn(
            weeklyElement,
            totalSalesArray,
            ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            'Something'
        )
    );
    weeklyChart.render();
});





var settings = {
    "url": "https://localhost/Api/BusinessReport/GetThisMonthReport",
    "method": "GET",
    "timeout": 0,
};
$.ajax(settings).done(function (response) {
    const totalSalesArray = response.data.map(item => item.totalSales);
    var monthlyElement = document.getElementById('kt_apexcharts_monthly');
    var monthlyChart = new ApexCharts(
        monthlyElement,
        optionsFn(
            monthlyElement,
            totalSalesArray,
            ['1st', '2nd', '3rd', '4th', '5th', '6th', '7th', '8th', '9th', '10th', '11th', '12th', '13th', '14th', '15th', '16th', '17th', '18th', '19th', '20th', '21st', '22nd', '23rd', '24th', '25th', '26th', '27th', '28th', '29th', '30th'],
            'Something'
        )
    );
    monthlyChart.render();
});
