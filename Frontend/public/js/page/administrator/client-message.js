var defaultPageSize = '?pageSize=10'
init(defaultPageSize);
var meta;
var data;
function init(param) {
    var settings = {
        "url": `${API_Server_Address}/Api/ClientMessage/GetList${param}`,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

function ajaxLoad(settings) {
    $.ajax(settings).done(function (response) {
        responseJson = response;
        var insertData = $('#insert_data');
        insertData.html('')
        response.data.forEach(function (object) {
            insertData.append(ClientMessageHtml(object));
        });
        meta = response.meta;
        data = response.data;

        $('#page-now').html(`${meta.page}/${meta.pageCount}`);

        if (meta.prevPageLink == null) {
            $('#prevPageLink').prop("disabled", true);
        } else {
            $('#prevPageLink').prop("disabled", false);
        }

        if (meta.nextPageLink == null) {
            $('#nextPageLink').prop("disabled", true);
        } else {
            $('#nextPageLink').prop("disabled", false);
        }
    });
}

$('#search').click(function () {
    let param = defaultPageSize;
    $('#insert_data').html('');

    const filterFields = [
        'title',
        'clientName',
        'clientPhone',
        'clientEmail',
        'minCreatedAt',
        'maxCreatedAt'
    ];

    for (const field of filterFields) {
        const value = $(`#form-search input[name="${field}"], #form-search select[name="${field}"]`).val();
        if (value !== '') {
            param += `&${field}=${value}`;
        }
    }
    if (param !== defaultPageSize) {
        param = `${param}`;
    }
    init(param);
});

$('#reset').click(function () {
    $('#form-search input, #form-search select').val('');
    $('#insert_data').html('');
    init(defaultPageSize);
});

function loadData(url) {
    $('#insert_data').html('');
    var settings = {
        "url": url,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('admin_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

$('#firstPageLink').click(function () {
    loadData(meta.firstPageLink);
});

$('#prevPageLink').click(function () {
    loadData(meta.prevPageLink);
});

$('#nextPageLink').click(function () {
    loadData(meta.nextPageLink);
});

$('#lastPageLink').click(function () {
    loadData(meta.lastPageLink);
});


$(document).on('click', '.getOneMessage', function () {
    var id = $(this).val();
    var settings = {
        "url": `${API_Server_Address}/Api/ClientMessage/GetOne/${id}`,
        "method": "GET",
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        $('#title').html(response.data.title);
        $('#name').html(response.data.aboutClient.name)
        $('#phone').html(response.data.aboutClient.phone)
        $('#email').html(response.data.aboutClient.email)
        $('#createdAt').html(moment(response.data.createdAt).format('YYYY/MM/DD HH:mm:ss'))
        $('#content').html(response.data.content);
        $('#viewMessageModal').modal('show');
    });
});

$('#closeModal').click(function () {
    $('#viewMessageModal').modal('hide');
});