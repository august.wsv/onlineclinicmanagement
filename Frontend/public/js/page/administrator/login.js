$('#loginButton').click(function () {
    var form = new FormData();
    form.append("Username", $('input[name="username"]').val());
    form.append("Password", $('input[name="password"]').val());

    var settings = {
    "url": `${API_Server_Address}/Api/Admin/Login`,
    "method": "POST",
    "processData": false,
    "mimeType": "multipart/form-data",
    "contentType": false,
    "data": form,
    "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        var settings = {
            "url": `${Application_Server_Address}/save-admin-token`,
            "method": "POST",
            "headers": {
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({
                "token": response.meta.token
            }),
        };
        $.ajax(settings).done();

        localStorage.setItem('admin_access_token', response.meta.token);
        localStorage.setItem(
            'admin_about', 
            JSON.stringify({
                name: response.data.name,
                username: response.data.username,
                rolename: response.data.rolename
            })
        );
        if (window.location.href.includes("login")) {
            window.location.href = `${Application_Server_Address}/administrator/business-report`
        } else {
            window.location.reload();
        }
    }).fail(function (response) {
        const errors = response.responseJSON.errors;
        let errorString = "";
        for (const key in errors) {
            if (errors.hasOwnProperty(key)) {
                errorString += `<b>${key}:</b> ${errors[key]} <br>`;
            }
        }
        alertify.alert(`${response.responseJSON.message}: <br>${errorString}`).setHeader('Notice!');
    });
});