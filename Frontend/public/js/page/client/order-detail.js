var currentURL = window.location.href;
var match = currentURL.match(/\/order-detail\/(\d+)/);
var number = match[1];

var settings = {
    "url": `${API_Server_Address}/Api/Purchase/GetOne/${number}`,
    "method": "GET",
    "headers": {
        "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
    },
    "dataType": "json"
};

$.ajax(settings).done(function (response) {
    $('#show_createdAt').html(moment(response.data.createdAt).format('YYYY/MM/DD HH:mm:ss'));
    $('#show_address').html(response.data.address);
    switch (response.data.status) {
        case 0:
            $('#show_status_convert').html('Processing...');
            $('#cancel_order_div').html(`<button id="submitBtn">Cancel</button>`);
            break;
        case 1:
            $('#show_status_convert').html('Verified and currently in transit...');
            $('#cancel_order_div').html(`<button id="submitBtnII">Cancel</button>`);
            break;
        case 2:
            $('#show_status_convert').html('Received...');
            break;
        case 3:
            $('#show_status_convert').html('Canceled...');
            break;
        case 4:
            $('#show_status_convert').html('Canceled by admin...')
            break;
        case 5:
            $('#show_status_convert').html('Canceled by admin (User do not received item)...')
            break;
        case 6:
            $('#show_status_convert').html('Canceled by user (Error item)...')
            break;
    };
    var myOrderDetail = $('#my-order-detail');
    response.data.purchaseDetails.forEach(function (object) {

        myOrderDetail.append(orderDetailHtml(object));
    });
    myOrderDetail.append(`
        <tr>
            <td colspan="5" class="bold">Total payment:</td>
            <td class="bold">${reverseCommaToDot(response.data.totalString)}</td>
        </tr>
    `)
});


$(document).on('click', '#submitBtn', function () {
    var form = new FormData();
    form.append("Status", "3");

    var settings = {
        "url": `${API_Server_Address}/Api/Purchase/Update/${number}`,
        "method": "PUT",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $('#submitBtn').remove();
    $('#show_status_convert').html('Canceled...');
    $.ajax(settings).done(function (response) { });
});


$(document).on('click', '#submitBtnII', function () {
    var form = new FormData();
    form.append("Status", "6");

    var settings = {
        "url": `${API_Server_Address}/Api/Purchase/Update/${number}`,
        "method": "PUT",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $('#submitBtnII').remove();
    $('#show_status_convert').html('Canceled...');
    $.ajax(settings).done(function (response) { });
});