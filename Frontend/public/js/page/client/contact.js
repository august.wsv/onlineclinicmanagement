$('#submit_message_btn').click(function () {
    var form = new FormData();
    form.append("Title", $('input[name="title"]').val());
    form.append("Content", $('textarea[name="content"]').val());

    var settings = {
        "url": `${API_Server_Address}/Api/ClientMessage/Create`,
        "method": "POST",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        alertify.alert(response.message).setHeader('Notice!');
        $('input[name="title"]').val(null);
        $('textarea[name="content"]').val(null);
    });
})