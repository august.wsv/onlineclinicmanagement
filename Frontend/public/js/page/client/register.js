var settings = {
    "url": `${API_Server_Address}/Api/Clinic/GetAll`,
    "method": "GET",
    "dataType": "json"
};

$.ajax(settings).done(function (response) {
    var select = $('select[name="clinicId"]');
    response.data.forEach(function (object) {
        select.append(clinicHtml(object));
    });
});

var settings = {
    "url": `${Application_Server_Address}/remove-client-token`,
    "method": "POST"
};
$.ajax(settings).done();
localStorage.removeItem('client_access_token');

var allInputTypePassword = $("input[type=password]");
var showPasswordButton = $("#showPassword");
var label2 = $('.label2');
var divtoDisplayFlexCustom = $('.divtoDisplayFlexCustom');
var token;

showPasswordButton.click(function () {
    allInputTypePassword.each(function () {
        var input = $(this);
        input.attr("type", (input.attr("type") === "password") ? "text" : "password");
    });
});
$('#submitbtn').click(function () {
    var form = new FormData();
    form.append("name", $('input[name="name"]').val());
    form.append("phone", $('input[name="phone"]').val());
    form.append("email", $('input[name="email"]').val());
    form.append("password", $('input[name="password"]').val());
    form.append("clinicId", $('select[name="clinicId"]').val());

    var settings = {
        "url": `${API_Server_Address}/Api/Client/EmailVerification`,
        "method": "POST",
        "processData": false,

        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    divtoDisplayFlexCustom.each(function (index) {
        ValidationSucceeded(index);
    });

    $.ajax(settings).done(function (response) {
        alertify.alert(response.message).setHeader('Notice!');
        token = response.meta.token
        $("#LoginForm").html(`
            <div class="divtoDisplayFlexCustom">
                <input type="text" name="code" placeholder="Nhập code xác minh">
                <label class="label1">Code</label>
                <label class="label2">display error text</label>
            </div>
            <div>
                <button id="submitbtn2" type="submit">Hoàn tất đăng ký</button>
            </div>
        `);
    }).fail(function (response) {
        alertify.alert(response.responseJSON.message).setHeader('Notice!');
        if (response.responseJSON.errors) {
            $.each(response.responseJSON.errors, function (fieldName, errorMessage) {
                var label = $('input[name="' + fieldName.toLowerCase() + '"]').siblings('.label2');
                var index = divtoDisplayFlexCustom.index(label.parent());
                ValidationFailed(index, errorMessage);
            });
        }
    });
});



$(document).on('click', '#submitbtn2', function () {
    label2 = $('.label2');
    divtoDisplayFlexCustom = $('.divtoDisplayFlexCustom');
    var form = new FormData();
    form.append("code", $('input[name="code"]').val());

    var settings = {
        "url": `${API_Server_Address}/Api/Client/Register`,
        "method": "POST",
        "headers": {
            "Authorization": `Bearer ${token}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    divtoDisplayFlexCustom.each(function (index) {
        ValidationSucceeded(index);
    });

    $.ajax(settings).done(function (response) {
        alertify.alert(response.message).setHeader('Notice!');
        alertify.alert(response.message, function () {
            window.location.href = `${Application_Server_Address}/login`
        }).setHeader('Notice!');

    }).fail(function (response) {
        alertify.alert(response.responseJSON.message).setHeader('Notice!');
        if (response.responseJSON.errors) {
            $.each(response.responseJSON.errors, function (fieldName, errorMessage) {
                var label = $('input[name="' + fieldName.toLowerCase() + '"]').siblings('.label2');
                var index = divtoDisplayFlexCustom.index(label.parent());
                ValidationFailed(index, errorMessage);
            });
        }
    });
});


function ValidationFailed(e, string) {
    label2.eq(e).css('display', 'block').text(string);
    divtoDisplayFlexCustom.eq(e).css('border-color', '#ff000083');
}

function ValidationSucceeded(e) {
    label2.eq(e).css('display', 'none');
    divtoDisplayFlexCustom.eq(e).css('border-color', '#00b8d479');
}