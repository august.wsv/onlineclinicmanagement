var currentURL = window.location.href;
var match = currentURL.match(/\/medicine-detail\/(\d+)/);
var number = match[1];


var settings = {
    "url": `${API_Server_Address}/Api/Medicine/GetOne/${number}`,
    "method": "GET",
    "headers": {
        "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
    },
    "dataType": "json"
};

$.ajax(settings).done(function (response) {
    $('#subTitle').html(response.data.name);
    $('#imagePath').attr('src', response.data.imagePath);
    $('#name').html(response.data.name);
    $('#priceString').html(response.data.priceString);
    $('#medicineTypeName').html(response.data.aboutMedicineType.name);
    $('#expDate').html(moment(response.data.expDate).format('YYYY/MM/DD HH:mm:ss'));
    $('#code').html(response.data.code);
    $('#quantity').html(response.data.quantity);
    $('#description').html(response.data.description);
});

var settings = {
    "url": `${API_Server_Address}/Api/Feedback/CheckExist/1/${number}`,
    "method": "GET",
    "headers": {
        "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
    },
    "dataType": "json"
};

$.ajax(settings).done(function (response) {
    if(!response.data){
        $('.feedback').append(`
            <div id="feedback-form">
                <label for="detail">Feedback:</label>
                <textarea id="detail" rows="4" cols="50"></textarea>
                <label for="rating">Rating:</label>
                <p>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                </p>
                <input type="hidden" id="rating">
                <br>
                <button id="send-feedback">SEND</button>
            </div>
        `);
    }
});




function Feedback() {
    var settings = {
        "url": `${API_Server_Address}/Api/Feedback/GetListByItem/1/${number}`,
        "method": "GET",
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        var ul = $('#feedback-list');
        ul.html('');
        response.data.forEach(function (object) {
            ul.append(feedbackDetail(object));
        });
    }).fail(function(){
        window.location.href = `${Application_Server_Address}/login`
    });
}

Feedback();

$(document).on('click', '#feedback-form .fa-star', function() {
    var index = $(this).index();

    $('#feedback-form .fa-star').each(function(i) {
        if (i <= index) {
            $(this).addClass('checked');
        } else {
            $(this).removeClass('checked');
        }
    });

    $('#rating').val(index + 1);
});





$(document).on('click', '#send-feedback', function() {
    if ($('#detail').val() !== '' & $('#rating').val() !== '') {
        var form = new FormData();
        form.append("Detail", $('#detail').val());
        form.append("ItemId", number);
        form.append("Rating", $('#rating').val());
        form.append("ItemType", "1");

        var settings = {
            "url": `${API_Server_Address}/Api/Feedback/Create`,
            "method": "POST",
            "headers": {
                "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
            },
            "processData": false,
            "mimeType": "multipart/form-data",
            "contentType": false,
            "data": form,
            "dataType": "json"
        };

        $.ajax(settings).done(function (response) {
            Feedback();
            alertify.alert(response.message).setHeader('Thank!!!');
            $('#feedback-form').remove();
        }).fail(function (response) {
            alertify.alert(response.responseJSON.message).setHeader('Notice!');
        });

    } else {
        alertify.alert('Content is null!').setHeader('Error!!!');
    }
})

$('.div1 button').click(function () {

    var form = new FormData();
    form.append("ItemId", number);
    form.append("ItemType", "1");

    var settings = {
        "url": `${API_Server_Address}/Api/Cart/Create`,
        "method": "POST",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        alertify.alert(response.message).setHeader('Thank!!!');
    }).fail(function(){
        window.location.href = `${Application_Server_Address}/login`
    });
});