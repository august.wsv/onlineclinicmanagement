var settings = {
    "url": `${API_Server_Address}/Api/Cart/GetListByClient`,
    "method": "GET",
    "headers": {
        "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
    },
    "dataType": "json"
};
$.ajax(settings).done(function (response) {
    var myCart = $('#my-cart');
    if (response.data.cartDetail != '') {
        myCart.append(`
            <div id="clear_all_cart_div">
                <div id="clear_all_cart">
                    <svg viewBox="0 0 96 96" focusable="false">
                        <path
                            d="M79.17 11.17L48 42.34 16.83 11.17l-5.66 5.66L42.34 48 11.17 79.17l5.66 5.66L48 53.66l31.17 31.17 5.66-5.66L53.66 48l31.17-31.17z">
                        </path>
                    </svg>
                    <p>Delete all carts</p>
                </div>
            </div>
        `);
        response.data.cartDetail.forEach(function (object) {
            myCart.append(cartHtml(object));
        });
    } else {
        myCart.html(`
            <p>No product in your order cart!</p>
        `);
    }
    originTotalCartValue = response.data.totalString;
    $('#update_total_value_of_cart').append(reverseCommaToDot(originTotalCartValue));
});







var settings = {
    "url": `${API_Server_Address}/Api/Client/AboutClient`,
    "method": "GET",
    "headers": {
        "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
    },
    "dataType": "json"
};
var defaultAddress = "";
$.ajax(settings).done(function (response) {
    defaultAddress = response.data.address;
    $('#show_name').append(response.data.name);
    $('#show_email').append(response.data.email);
    $('#show_phone').append(response.data.phone);
    var addressCustomDiv = $('#address_custom');
    if (defaultAddress != null) {
        addressCustomDiv.html(`
            <div class="break input_radio_width_label">
                <input type="radio" name="delivery_address" id="default_address" checked value="default_address">
                <label for="default_address">Saved Address: <span id="show_address"></span></label>
            </div>
            <p id="default_address_text"></p>
            <div class="break input_radio_width_label">
                <input type="radio" name="delivery_address" id="custom_address" value="custom_address">
                <label for="custom_address">Other Address: </label>
            </div>
            <input type="text" name="address" id="address_input">
            <p style="color:red; display:none" id="errortext">Please enter your address!</p>
        `);
        $('#show_address').append(response.data.address);
    } else {
        addressCustomDiv.html(`
            <input type="text" name="address" id="address_input">
            <p style="color:red; display:none" id="errortext">Please enter your address!</p>
        `);
    }
});




$(document).on("change", 'input[name="delivery_address"]', function () {
    var selectedOption = $('input[name="delivery_address"]:checked').val();
    if (selectedOption === "default_address") {
        $('#errortext').hide();
    } else if (selectedOption === "custom_address") {
        $(document).on("keyup", "#address_input", function () {
            if ($('#address_input').val().trim() === "") {
                $('#errortext').show();
            } else {
                $('#errortext').hide();
            }
        });
    }
});




$("#confirm_to_order").click(function () {
    if ($('input[name="delivery_address"]').length > 0) {
        var selectedOption = $('input[name="delivery_address"]:checked').val();
        if (selectedOption === "default_address") {
            confirmOrder(defaultAddress)
        } else if (selectedOption === "custom_address") {
            if ($('#address_input').val().trim() === "") {
                $('#errortext').show();
            } else {
                confirmOrder($('#address_input').val());
            }
        }
    } else {
        if ($('#address_input').val().trim() === "") {
            $('#errortext').show();
        } else {
            confirmOrder($('#address_input').val());
        }
    }
});





function confirmOrder(address) {
    var form = new FormData();
    form.append("Address", address);

    var settings = {
        "url": `${API_Server_Address}/Api/Purchase/Create`,
        "method": "POST",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        alertify.alert(response.message).setHeader('Notice!');
        window.location.href = `${Application_Server_Address}/order-detail/${response.data.id}/`;
    }).fail(function (response) {
        alertify.alert(response.responseJSON.message).setHeader('Notice!');
    });
}







$(document).on('click', '.decrease, .increase', function () {
    var decandinc = $(this).closest('.decandinc').find('.pr_nb');
    var index = $('.itemId', decandinc.closest('.item')).val();
    var isDecrease = $(this).hasClass('decrease');
    var valueUpdate = isDecrease ? parseInt(decandinc.val()) - 1 : parseInt(decandinc.val()) + 1;

    var max = $('.max_amount', decandinc.closest('.item')).text();
    if (valueUpdate > 0 && valueUpdate <= max) {
        updateQuantity(index, valueUpdate);
        decandinc.val(valueUpdate);
        var total = $('.priceString', decandinc.closest('.item')).text();
        var result = multiplyCurrencyString(total, decandinc.val());
        $('.update-totalConvert', decandinc.closest('.item')).text(result);
    }

    var totalConvertValues = $('.update-totalConvert').map(function () {
        return $(this).text();
    }).get();

    $('#update_total_value_of_cart').text(addCurrencyStrings(totalConvertValues));
});






$(document).on('click', '.remove_edition', function () {
    var remove = $(this).closest('.remove_edition');
    var index = $('.itemId', remove.closest('.item')).val();
    var itemToRemove = $(this).closest('.item');
    var settings = {
        "url": `${API_Server_Address}/Api/Cart/Delete/${index}`,
        "method": "DELETE",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
        },
        "dataType": "json"
    };

    itemToRemove.remove();

    $.ajax(settings).done(function (response) { });

    var totalConvertValues = $('.update-totalConvert').map(function () {
        return $(this).text();
    }).get();
    var allTotalValue = addCurrencyStrings(totalConvertValues);
    if (allTotalValue == 0) {
        $('#update_total_value_of_cart').text(multiplyCurrencyString(originTotalCartValue, 0));
        var myCart = $('#my-cart');
        myCart.html(`
            <p>No product in your order cart!</p>
        `);
    } else {
        $('#update_total_value_of_cart').text(allTotalValue);
    }
});





$(document).on('click', '#clear_all_cart', function () {
    var settings = {
        "url": `${API_Server_Address}/Api/Cart/DeleteAllOfClient`,
        "method": "DELETE",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
        },
        "dataType": "json"
    };
    var itemToRemove = $('.item');
    itemToRemove.remove();

    $.ajax(settings).done(function (response) { });

    var totalConvertValues = $('.update-totalConvert').map(function () {
        return $(this).text();
    }).get();
    var allTotalValue = addCurrencyStrings(totalConvertValues);
    if (allTotalValue == 0) {
        $('#update_total_value_of_cart').text(multiplyCurrencyString(originTotalCartValue, 0));
        var myCart = $('#my-cart');
        myCart.html(`
            <p>No product in your order cart!</p>
        `);
    } else {
        $('#update_total_value_of_cart').text(allTotalValue);
    }
});


function updateQuantity(id, quantity) {
    var form = new FormData();
    form.append("Quantity", quantity);

    var settings = {
        "url": `${API_Server_Address}/Api/Cart/Update/${id}`,
        "method": "PUT",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) { });
}






function parseCurrencyString(inputString) {
    const currencySymbol = inputString.match(/[^0-9.,]+/);
    const currencyPosition = inputString.indexOf(currencySymbol);

    const resultArray = [
        parseFloat(inputString.replace(/[^0-9.]/g, '')),
        currencySymbol[0],
        currencyPosition === 0 || currencyPosition === inputString.length - currencySymbol[0].length - 1 ? true : false
    ];

    return resultArray;
}

function multiplyCurrencyString(inputString, factor) {
    const [numericValue, currencySymbol, isCurrencySymbolAtStart] = parseCurrencyString(inputString);

    if (!isNaN(numericValue)) {
        const resultValue = numericValue * factor;
        let resultString = resultValue.toLocaleString(undefined, { minimumFractionDigits: 2 });
        if (isCurrencySymbolAtStart) {
            resultString = currencySymbol + resultString;
        } else {
            resultString += currencySymbol;
        }
        return resultString;
    }
    return "Invalid Input";
}

function addCurrencyStrings(strings) {
    if (Array.isArray(strings) && strings.length > 0) {
        const [firstValue, firstSymbol, firstIsSymbolAtStart] = parseCurrencyString(strings[0]);
        if (strings.length >= 2) {
            let resultValue = firstValue;
            let resultSymbol = firstSymbol;
            let resultIsSymbolAtStart = firstIsSymbolAtStart;

            for (let i = 1; i < strings.length; i++) {
                const [value, symbol, isSymbolAtStart] = parseCurrencyString(strings[i]);

                if (!isNaN(value) && symbol === resultSymbol && isSymbolAtStart === resultIsSymbolAtStart) {
                    resultValue += value;
                } else {
                    return "Invalid Input";
                }
            }

            let resultString = resultValue.toLocaleString(undefined, { minimumFractionDigits: 2 });

            if (resultIsSymbolAtStart) {
                resultString = resultSymbol + resultString;
            } else {
                resultString += resultSymbol;
            }

            return resultString;
        }
        return strings[0];
    }

    return 0;
}

