var currentURL = window.location.href;
var match = currentURL.match(/\/educational-event-detail\/(\d+)/);
var number = match[1];

var settings = {
    "url": `${API_Server_Address}/Api/EducationalEvent/GetOne/${number}`,
    "method": "GET",
    "dataType": "json"
};

$.ajax(settings).done(function (response) {
    var data = response.data;
    $('#subTitle').html(data.educationalEvent.name);
    $('#educationalEvent_name').html(data.educationalEvent.name);
    $('#educationalEvent_createdAt').html(moment(data.educationalEvent.createdAt).format('YYYY/MM/DD HH:mm:ss'));
    $('#subject_name').html(data.subject.name);
    $('#staffMember_name').html(data.staffMember.name);
    $('#staffMember_phone').html(data.staffMember.phone);
    $('#staffMember_email').html(data.staffMember.email);
    $('#staffMember_personalBiography').html(data.staffMember.personalBiography);
    $('#educationalEvent_videoPath').attr('src', data.educationalEvent.videoPath);
    $('#educationalEvent_description').html(data.educationalEvent.description);
});

const player = new Plyr("#educationalEvent_videoPath");


var settings = {
    "url": `${API_Server_Address}/Api/Comment/GetListByEducationalEventId/${number}`,
    "method": "GET",
    "dataType": "json"
};

CommentX();
function CommentX(){
    $.ajax(settings).done(function (response) {
        var ul = $('#comment-list');
        ul.html('');
        response.data.forEach(function (object) {
            if (!object.parentId) {
                ul.append(commentDetail(object, response.data));
            }
        });
    });
}

var clientAccessToken2 = localStorage.getItem('client_access_token');
if (clientAccessToken2 !== null) {
    $('#comment').append(`
        <div id="comment-form">
            <div>
                <input id="detail">
                <button id="cancel-feedback-comment">X Cancel feedback</button>
            </div>
            <input type="hidden" id="parent-id">
            <button id="send-comment">SEND</button>
        </div>
    `);
} 

$(document).on('click', '#send-comment', function () {
    if ($('#detail').val() !== '') {
        var form = new FormData();
        form.append("Content", $('#detail').val());
        form.append("EducationalEventId", number);
        if ($('#parent-id').val()) {
            form.append("ParentId", $('#parent-id').val());
        }

        var settings = {
            "url": `${API_Server_Address}/Api/Comment/Create`,
            "method": "POST",
            "headers": {
                "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
            },
            "processData": false,
            "mimeType": "multipart/form-data",
            "contentType": false,
            "data": form,
            "dataType": "json"
        };

        $.ajax(settings).done(function (response) {
            CommentX();            
            $('#detail').val(null);
            $('#parent-id').val(null);
            $('#cancel-feedback-comment').hide();
        });

    } else {
        alertify.alert('Content is null!').setHeader('Error!!!');
    }
})

$(document).on('click', '.feedback-comment', function () {
    var commemtIdVal = $(this).closest('.comment').find('.comment-id').val();
    $('#parent-id').val(commemtIdVal);
    $('#cancel-feedback-comment').show();
});

$(document).on('click', '#cancel-feedback-comment', function () {
    $('#parent-id').val(null);
    $('#cancel-feedback-comment').hide();
});
