var currentURL = window.location.href;
var match = currentURL.match(/\/subject-detail\/(\d+)/);
var number = match[1];


var settings = {
    "url": `${API_Server_Address}/Api/Subject/GetOne/${number}`,
    "method": "GET",
};

$.ajax(settings).done(function (response) {
    $('#subTitle').html(response.data.name);
    $('#imagePath').attr('src', response.data.imagePath);
    $('#name').html(response.data.name);
    $('#briefDescription').html(response.data.briefDescription);
    appendOrdersToTab(response, [1], "#lecture-tab");
    appendOrdersToTab(response, [2], "#practical-tab");
    appendOrdersToTab(response, [3], "#seminar-tab");
});


var settings = {
    "url": `${API_Server_Address}/Api/Feedback/CheckExist/3/${number}`,
    "method": "GET",
    "headers": {
        "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
    },
    "dataType": "json"
};

$.ajax(settings).done(function (response) {
    if (!response.data) {
        $('.feedback').append(`
            <div id="feedback-form">
                <label for="detail">Feedback:</label>
                <textarea id="detail" rows="4" cols="50"></textarea>
                <label for="rating">Rating:</label>
                <p>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                </p>
                <input type="hidden" id="rating">
                <br>
                <button id="send-feedback">SEND</button>
            </div>
        `);
    }
});

function Feedback() {
    var settings = {
        "url": `${API_Server_Address}/Api/Feedback/GetListByItem/3/${number}`,
        "method": "GET",
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        var ul = $('#feedback-list');
        ul.html('');
        response.data.forEach(function (object) {
            ul.append(feedbackDetail(object));
        });
    }).fail(function () {
        window.location.href = `${Application_Server_Address}/login`
    });
}

Feedback();

$(document).on('click', '#feedback-form .fa-star', function () {
    var index = $(this).index();

    $('#feedback-form .fa-star').each(function (i) {
        if (i <= index) {
            $(this).addClass('checked');
        } else {
            $(this).removeClass('checked');
        }
    });

    $('#rating').val(index + 1);
});

$(document).on('click', '#send-feedback', function () {
    if ($('#detail').val() !== '' & $('#rating').val() !== '') {
        var form = new FormData();
        form.append("Detail", $('#detail').val());
        form.append("ItemId", number);
        form.append("Rating", $('#rating').val());
        form.append("ItemType", "3");

        var settings = {
            "url": `${API_Server_Address}/Api/Feedback/Create`,
            "method": "POST",
            "headers": {
                "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
            },
            "processData": false,
            "mimeType": "multipart/form-data",
            "contentType": false,
            "data": form,
            "dataType": "json"
        };

        $.ajax(settings).done(function (response) {
            Feedback();
            alertify.alert(response.message).setHeader('Thank!!!');
            $('#feedback-form').remove();
        }).fail(function (response) {
            alertify.alert(response.responseJSON.message).setHeader('Notice!');
        });

    } else {
        alertify.alert('Content is null!').setHeader('Error!!!');
    }
})







let rememberLocation = 0;
let navigationItem = $('.navigationItem');
let tabContent = $('.tabContent');

function clickNavigationItemEvent(index) {
    navigationItem.eq(index).click(function () {
        if (index !== rememberLocation) {
            navigationItem.eq(index).css('background-color', '#171d1b');
            tabContent.eq(index).show();

            navigationItem.eq(rememberLocation).css('background-color', '#505050');
            tabContent.eq(rememberLocation).hide();
            rememberLocation = index;
        }
    });
}

navigationItem.each(function (index) {
    clickNavigationItemEvent(index);
});

function appendOrdersToTab(response, statuses, tabSelector) {
    var filteredData = response.data.educationalEvents.filter(function (item) {
        return statuses.includes(item.educationalEventType);
    });
    var tab = $(tabSelector);
    if (filteredData.length > 0) {
        filteredData.forEach(function (object) {
            tab.append(educationalEventHtml(object));
        });
    } else {
        tab.append(`
            <tr>
                <th colspan="3">Lesson not found</th>
            </tr>
        `);
    }
}
