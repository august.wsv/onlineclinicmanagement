function Init() {
    var settings = {
        "url": `${API_Server_Address}/Api/Client/AboutClient`,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
        },
        "dataType": "json"
    };
    var defaultSelectVal;
    $.ajax(settings).done(function (response) {
        $('#name').html(response.data.name);
        $('#phone').html(response.data.phone);
        $('#address').html(response.data.address);
        $('#clinic-name').html(response.data.aboutClinic.name);

        $('input[name="name"]').val(response.data.name);
        $('input[name="phone"]').val(response.data.phone);
        $('input[name="address"]').val(response.data.address);
        defaultSelectVal = response.data.clinicId;

        var settings = {
            "url": `${API_Server_Address}/Api/Clinic/GetAll`,
            "method": "GET",
            "dataType": "json"
        };

        $.ajax(settings).done(function (response) {
            var select = $('select[name="clinicId"]');
            select.html('')
            response.data.forEach(function (object) {
                select.append(clinicHtml(object));
            });
            select.val(defaultSelectVal);
        });
    });
}

Init();

$(document).on('click', '#editButton', function () {
    toggleEdit()
})

$(document).on('click', '#cancelButton', function () {
    cancelEdit()
})

function toggleEdit() {
    $('.editable').show();
    $('.non-editable').hide();
    $('#editButton').hide();
    $('#cancelButton').show();
    $('#saveButton').show();
}

function cancelEdit() {
    $('.editable').hide();
    $('.non-editable').show();
    $('#cancelButton').hide();
    $('#saveButton').hide();
    $('#editButton').show();
}


$(document).on('click', '#saveButton', function () {
    var form = new FormData();
    form.append("Name", $('input[name="name"]').val());
    form.append("Phone", $('input[name="phone"]').val());
    form.append("Address", $('input[name="address"]').val());
    form.append("ClinicId", $('select[name="clinicId"]').val());

    var settings = {
        "url": `${API_Server_Address}/Api/Client/Update`,
        "method": "PUT",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json"
    };

    $.ajax(settings).done(function (response) {
        Init();
        cancelEdit()
    });
})



