var defaultPageSize = '?pageSize=5'

init(defaultPageSize);
var meta;
function init(param) {
    var settings = {
        "url": `${API_Server_Address}/Api/Subject/GetList${param}`,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}



function ajaxLoad(settings) {
    $.ajax(settings).done(function (response) {
        var myCart = $('.divMC_T');
        if (response.data != '') {
            response.data.forEach(function (object) {
                myCart.append(subjectHtml(object));
            });
        } else {
            myCart.html(`
                <p>No products!</p>
            `);
        }
        meta = response.meta;

        $('#page-now').html(`${meta.page}/${meta.pageCount}`);

        if(meta.prevPageLink == null){
            $('#prevPageLink').prop("disabled", true);
        } else {
            $('#prevPageLink').prop("disabled", false);
        }

        if(meta.nextPageLink == null){
            $('#nextPageLink').prop("disabled", true);
        } else {
            $('#nextPageLink').prop("disabled", false);
        }
    });
}





var settings = {
    "url": `${API_Server_Address}/Api/MedicineType/GetAll`,
    "method": "GET",
    "headers": {
        "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
    },
    "dataType": "json"
};

$.ajax(settings).done(function (response) {
    var select = $('select[name="medicineTypeId"]');
    response.data.forEach(function (object) {
        select.append(medicineTypeHtml(object));
    });
});

$('#filter').click(function () {
    let param = defaultPageSize;
    $('.divMC_T').html('');

    const filterFields = [
        'name'
    ];

    for (const field of filterFields) {
        const value = $(`input[name="${field}"], select[name="${field}"]`).val();
        if (value !== '') {
            param += `&${field}=${value}`;
        }
    }
    if (param !== defaultPageSize) {
        param = `${param}`;
    }
    init(param);
    
});

$('#resetfilter').click(function () {
    $('.calculator input, .calculator select').val('');
    $('.divMC_T').html('');
    init(defaultPageSize);
});

function loadData(url) {
    $('.divMC_T').html('');
    var settings = {
        "url": url,
        "method": "GET",
        "headers": {
            "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
        },
        "dataType": "json"
    };

    ajaxLoad(settings)
}

$('#firstPageLink').click(function () {
    loadData(meta.firstPageLink);
});

$('#prevPageLink').click(function () {
    loadData(meta.prevPageLink);
});

$('#nextPageLink').click(function () {
    loadData(meta.nextPageLink);
});

$('#lastPageLink').click(function () {
    loadData(meta.lastPageLink);
});