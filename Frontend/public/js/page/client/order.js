
let rememberLocation = 0;
let navigationItem = $('.navigationItem');
let tabContent = $('.tabContent');

function clickNavigationItemEvent(index) {
    navigationItem.eq(index).click(function () {
        if (index !== rememberLocation) {
            navigationItem.eq(index).css('background-color', '#171d1b');
            tabContent.eq(index).show();

            navigationItem.eq(rememberLocation).css('background-color', '#505050');
            tabContent.eq(rememberLocation).hide();
            rememberLocation = index;
        }
    });
}

navigationItem.each(function (index) {
    clickNavigationItemEvent(index);
});

var settings = {
    "url": `${API_Server_Address}/Api/Purchase/GetListByClient`,
    "method": "GET",
    "headers": {
        "Authorization": `Bearer ${localStorage.getItem('client_access_token')}`
    },
    "dataType": "json"
};

function appendOrdersToTab(response, statuses, tabSelector) {
    var filteredData = response.data.filter(function (item) {
        return statuses.includes(item.status);
    });
    var tab = $(tabSelector);
    if (filteredData.length > 0) {
        filteredData.forEach(function (object) {
            tab.append(orderGetOneHtml(object));
        });
    } else {
        tab.append(`
            <tr>
                <th colspan="5">No order found</th>
            </tr>
        `);
    }
}


$.ajax(settings).done(function (response) {
    appendOrdersToTab(response, [0], "#pending-tab");
    appendOrdersToTab(response, [1], "#confirmed-and-shipping-tab");
    appendOrdersToTab(response, [2], "#delivered-tab");
    appendOrdersToTab(response, [3, 4], "#order-canceled-tab");
});