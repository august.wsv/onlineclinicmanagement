if (!navigator.onLine) {
    alertify.alert('Không có kết nối Internet, các tài nguyên bên thứ 3 sử dụng không thể tải được!').setHeader('Notice!');
}
function checkInternetConnection() {
    alertify.alert(navigator.onLine ? 'Đã kết nối Internet!' : 'Không có kết nối Internet!').setHeader('Notice!');
}
window.addEventListener('online', checkInternetConnection);
window.addEventListener('offline', checkInternetConnection);

window.addEventListener('pageshow', function (event) {
    if (event.persisted) { location.reload(); }
});


var currentLocation = window.location;
const Application_Server_Address = `${currentLocation.protocol}//${currentLocation.hostname}`;
const API_Server_Address = 'https://localhost';

function reverseCommaToDot(numberString) {
    if (numberString.includes(',')) {
        numberString = numberString.replace(/\./g, ',').replace(/,([^,]*)$/, ".$1");
    }
    return numberString;
}
