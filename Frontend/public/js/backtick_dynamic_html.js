function cartHtml(object) {
    return `
        <div class="item">
            <input type="hidden" value="${object.id}" class="itemId">
            <div class="div1">
                <img src="${object.imagePath}" alt="">
            </div>
            <div class="div2">
                <p>${object.itemName}</p>
                <p>${object.itemTypeName}</p>
                <p>Price: <span class="priceString">${reverseCommaToDot(object.priceString)}</span></p>
                <p>Remaining quantity: <span class="max_amount">${object.storeQuantity}</span></p>            
                <input type="hidden" value="${object.price}" class="edition_price">
                
            </div>
            <div class="div3">
                <p class="update-totalConvert">${reverseCommaToDot(object.totalString)}</p>
                <div class="decandinc">
                    <button class="decrease">-</button>
                    <input class="pr_nb" type="number" value="${object.quantity}" disabled="disabled">
                    <button class="increase">+</button>
                </div>
                <svg fill="#000000" viewBox="0 0 32.00 32.00" class="remove_edition">
                    <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                    <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>

                    <g id="SVGRepo_iconCarrier">
                        <title>trash-can</title>
                        <path
                            d="M30 7.249h-5.598l-3.777-5.665c-0.137-0.202-0.366-0.334-0.625-0.334h-8c-0 0-0.001 0-0.001 0-0.259 0-0.487 0.131-0.621 0.331l-0.002 0.003-3.777 5.665h-5.599c-0.414 0-0.75 0.336-0.75 0.75s0.336 0.75 0.75 0.75v0h3.315l1.938 21.319c0.036 0.384 0.356 0.682 0.747 0.682 0 0 0 0 0.001 0h16c0 0 0.001 0 0.001 0 0.39 0 0.71-0.298 0.745-0.679l0-0.003 1.938-21.319h3.316c0.414 0 0.75-0.336 0.75-0.75s-0.336-0.75-0.75-0.75v0zM12.401 2.75h7.196l2.999 4.499h-13.195zM23.314 29.25h-14.63l-1.863-20.5 18.358-0.001zM11 11.25c-0.414 0-0.75 0.336-0.75 0.75v0 14c0 0.414 0.336 0.75 0.75 0.75s0.75-0.336 0.75-0.75v0-14c-0-0.414-0.336-0.75-0.75-0.75v0zM16 11.25c-0.414 0-0.75 0.336-0.75 0.75v0 14c0 0.414 0.336 0.75 0.75 0.75s0.75-0.336 0.75-0.75v0-14c-0-0.414-0.336-0.75-0.75-0.75v0zM21 11.25c-0.414 0-0.75 0.336-0.75 0.75v0 14c0 0.414 0.336 0.75 0.75 0.75s0.75-0.336 0.75-0.75v0-14c-0-0.414-0.336-0.75-0.75-0.75v0z">
                        </path>
                    </g>
                </svg>
            </div>
        </div>
    `;
};

function orderDetailHtml(object) {
    return `
        <tr>
            <th class="for-image"><img src="${object.imagePath}" alt="">
            </th>
            <td class="nopadding">${object.itemName}</td>
            <td>${object.itemTypeName}</td>
            <td>${reverseCommaToDot(object.priceString)}</td>
            <td>${object.quantity}</td>
            <td class="bold">${reverseCommaToDot(object.totalString)}</td>
        </tr>
    `;
}

function orderGetOneHtml(object) {
    switch (object.status) {
        case 0:
            var statusText = 'Waiting for processing...';
            break;
        case 1:
            var statusText = 'Confirmed and on the way for delivery...';
            break;
        case 2:
            var statusText = 'Received the item...';
            break;
        case 3:
            var statusText = 'Cancelled...';
            break;
        case 4:
            var statusText = 'Cancelled by admin...';
            break;
        case 5:
            var statusText = 'User do not received';
            break;
        case 6:
            var statusText = 'Error item';
            break;
        default:
            var statusText = 'Undefined status';
    }

    return `
        <tr>
            <td>${moment(object.createdAt).format('YYYY/MM/DD HH:mm:ss')}</th>
            <td>${object.address}</th>
            <td>${statusText}</th>
            <td>${object.totalString}</th>
            <td><a href="${Application_Server_Address}/order-detail/${object.id}">Detail</a></th>
        </tr>
    `
}

function medicineHtml(object) {
    return `
        <div class="item">
            <div class="image">
                <img src="${object.imagePath}">
            </div>
            <div class="content">
                <h4>${object.name}</h4>
                <p> - Code: ${object.code}</p>
                <p> - Price: ${object.priceString}</p>
                <p> - EXP: ${moment(object.expDate).format('YYYY/MM/DD HH:mm:ss')}</p>
                <p> - Type: ${object.aboutMedicineType.name}</p>
                <a href="${Application_Server_Address}/medicine-detail/${object.id}"><button>View</button></a>
            </div>  
        </div>
    `;
}

function scientificApparatusHtml(object) {
    return `
        <div class="item">
            <div class="image">
                <img src="${object.imagePath}">
            </div>
            <div class="content">
                <h4>${object.name}</h4>
                <p> - Code: ${object.code}</p>
                <p> - Price: ${object.priceString}</p>
                <p> - EXP: ${moment(object.expDate).format('YYYY/MM/DD HH:mm:ss')}</p>
                <p> - Origin: ${object.origin}</p>
                <a href="${Application_Server_Address}/scientific-apparatus-detail/${object.id}"><button>View</button></a>
            </div>  
        </div>
    `;
}

function medicineTypeHtml(object) {
    return `
        <option value="${object.id}">${object.name}</option>
    `;
}

function clinicHtml(object) {
    return `
        <option value="${object.id}">${object.name}</option>
    `;
}

function feedbackDetail(object) {
    const stars = Array(5).fill('<span class="fa fa-star"></span>');
    for (let i = 0; i < object.rating; i++) {
        stars[i] = '<span class="fa fa-star checked"></span>';
    }

    return `
        <li>    
            <p>${object.aboutClient.name}</p>
            <p>${moment(object.createdAt).format('YYYY/MM/DD HH:mm:ss')}</p>
            <p>${stars.join('')}</p>
            <p>${object.detail}</p>
        </li>
    `;
}

function subjectHtml(object) {
    return `
        <div class="item">
            <div class="div1">
                <img src="${object.imagePath}" alt="">
            </div>
            <div class="div2">
                <p>${object.name}</p>
                <div class="brief_description">${object.briefDescription}</div>
                <a href="${Application_Server_Address}/subject-detail/${object.id}" class="aaa"><button>View</button></a>
            </div>
        </div>
    `;
}

function educationalEventHtml(object) {
    return `
        <tr>
            <td>${object.name}</th>
            <td>${object.briefDescription}</th>
            <td><a href="${Application_Server_Address}/educational-event-detail/${object.id}">Detail</a></th>
        </tr>
    `
}

function educationalEventHtml2(object) {
    return `
        <tr>
            <td>${object.name}</th>
            <td>${object.briefDescription}</th>
            <td>
                <button value="${object.id}" class="updateEE btn_green">Update</button>
                <button value="${object.id}" class="deleteEE btn_red">Delete</button>
                <a href="${Application_Server_Address}/staff-member/educational-event-detail/${object.id}">
                    <button class="deleteEE btn_black">Detail</button>
                </a>
            </th>
        </tr>
    `
}

function commentDetail(object, data) {
    var subComments = getSubComments(object.id, data);
    var subCommentsHtml = '';

    if (subComments.length > 0) {
        subCommentsHtml = '<ul>';
        subComments.forEach(function (subComment) {
            subCommentsHtml += subCommentDetail(subComment);
        });
        subCommentsHtml += '</ul>';
    }

    return `
        <li class="comment">    
            <p>${object.personName}</p>
            <p>${object.content}</p>
            <p>${moment(object.createdAt).format('YYYY/MM/DD HH:mm:ss')}</p>
            <p class="feedback-comment">Feedback</p>
            <input type="hidden" value="${object.id}" class="comment-id">
            ${subCommentsHtml}
        </li>
    `;
}

function getSubComments(parentId, data) {
    return data.filter(function (comment) {
        return comment.parentId === parentId;
    });
}

function subCommentDetail(object) {
    return `
        <li>    
            <p>${object.personName}</p>
            <p>${object.content}</p>
            <p>${moment(object.createdAt).format('YYYY/MM/DD HH:mm:ss')}</p>
        </li>
    `;
}

function adminHtml(object){
    return `
        <tr>
            <td class="name">${object.name}</td>
            <td>${object.username}</td>
            <td>${object.rolename}</td>
            <td>
                <button value="${object.id}" class="updateAccount"><i class="bi bi-pencil-fill"></i></button>
                <button value="${object.id}" class="deleteAccount"><i class="bi bi-trash3"></i></button>
            </td>
        </tr>
    `;
}

 
function staffMemberHtml(object){
    return `
        <tr>
            <td class="name">${object.name}</td>
            <td>${object.phone}</td>
            <td>${object.email}</td>
            <td>
                <button value="${object.id}" class="updateAccount"><i class="bi bi-pencil-fill"></i></button>
                <button value="${object.id}" class="deleteAccount"><i class="bi bi-trash3"></i></button>
            </td>
        </tr>
    `;
}

function clientHtml(object){
    return `
        <tr>
            <td class="name">${object.name}</td>
            <td>${object.phone}</td>
            <td>${object.email}</td>
            <td>${object.address}</td>
            <td>${object.aboutClinic.name}</td>
            <td>
                <button value="${object.id}" class="deleteAccount"><i class="bi bi-trash3"></i></button>
            </td>
        </tr>
    `;
}

function ClientMessageHtml(object){
    return `
        <tr>
            <td>${object.title}</td>
            <td>${object.aboutClient.name}</td>
            <td><a href="tel:${object.aboutClient.phone}">${object.aboutClient.phone}</a></td>
            <td><a href="mailto:${object.aboutClient.email}">${object.aboutClient.email}</a></td>
            <td>${moment(object.createdAt).format('YYYY/MM/DD HH:mm:ss')}</td>
            <td>
                <button value="${object.id}" class="getOneMessage"><i class="bi bi-eye"></i></button>
            </td>
        </tr>
    `;
}

function clinicHtml2(object) {
    return `
        <tr>
            <td class="name">${object.name}</td>
            <td>${object.phone}</td>
            <td>${object.email}</td>
            <td>${object.address}</td>
            <td>
                <button value="${object.id}" class="updateClinic"><i class="bi bi-pencil-fill"></i></button>
                <button value="${object.id}" class="deleteClinic"><i class="bi bi-trash3"></i></button>
            </td>
        </tr>
    `;
}

function medicineTypeHtml2 (object) {
    return `
        <tr>
            <td class="name">${object.name}</td>
            <td>
                <button value="${object.id}" class="updateMedicineType"><i class="bi bi-pencil-fill"></i></button>
                <button value="${object.id}" class="deleteMedicineType"><i class="bi bi-trash3"></i></button>
            </td>
        </tr>
    `;
}

function subjectHtml2 (object) {
    return `
        <tr>
            <td class="name">${object.name}</td>
            <td><img src="${object.imagePath}" alt=""></td>
            <td>${object.briefDescription}</td>
            <td>
                <button value="${object.id}" class="updateSubject"><i class="bi bi-pencil-fill"></i></button>
                <button value="${object.id}" class="deleteSubject"><i class="bi bi-trash3"></i></button>
            </td>
        </tr>
    `;
}

function medicineHtml2 (object) {
    return `
        <tr>
            <td class="name">${object.name}</td>
            <td><img src="${object.imagePath}" alt=""></td>
            <td>${object.code}</td>
            <td>${object.priceString}</td>            
            <td>${object.quantity}</td>
            <td>${moment(object.expDate).format('YYYY/MM/DD HH:mm:ss')}</td>
            <td>${object.aboutMedicineType.name}</td>
            <td>
                <button value="${object.id}" class="updateMedicine"><i class="bi bi-pencil-fill"></i></button>
                <button value="${object.id}" class="deleteMedicine"><i class="bi bi-trash3"></i></button>
            </td>
        </tr>
    `;
}

function scientificApparatusHtml2 (object) {
    return `
        <tr>
            <td class="name">${object.name}</td>
            <td><img src="${object.imagePath}" alt=""></td>
            <td>${object.code}</td>
            <td>${object.priceString}</td>            
            <td>${object.quantity}</td>
            <td>${moment(object.expDate).format('YYYY/MM/DD HH:mm:ss')}</td>
            <td>${object.origin}</td>
            <td>
                <button value="${object.id}" class="updateScientificApparatus"><i class="bi bi-pencil-fill"></i></button>
                <button value="${object.id}" class="deleteScientificApparatus"><i class="bi bi-trash3"></i></button>
            </td>
        </tr>
    `;
}

function purchaseHtml2 (object) {
    switch (object.status) {
        case 0:
            var statusText = 'Waiting for processing...';
            break;
        case 1:
            var statusText = 'Confirmed and on the way for delivery...';
            break;
        case 2:
            var statusText = 'Received the item...';
            break;
        case 3:
            var statusText = 'Cancelled by user';
            break;
        case 4:
            var statusText = 'Cancelled ...';
            break;
        case 5:
            var statusText = 'User do not received';
            break;
        case 6:
            var statusText = 'Error item';
            break;
        default:
            var statusText = 'Undefined status';
    }
    return `
        <tr>
            <td>${object.aboutClient.name}</td>
            <td>${object.aboutClient.phone}</td>
            <td>${object.aboutClient.email}</td>
            <td>${object.address}</td>
            <td>${moment(object.createdAt).format('YYYY/MM/DD HH:mm:ss')}</td>
            <td>${statusText}</td>
            <td>
                <button value="${object.id}" class="getOnePurchase"><i class="bi bi-eye"></i></button>
                <button value="${object.id}" class="updatePurchase"><i class="bi bi-pencil-fill"></i></button>
            </td>
        </tr>
    `;
}

function purchaseDetailHtml(object) {
    return `
        <tr>
            <td>${object.itemName}</td>
            <td>${object.itemTypeName}</td>
            <td>${object.priceString}</td>
            <td>${object.quantity}</td>
            <td>${object.totalString}</td>
        </tr>
    `;
}

function aboutUsHtml2(object) {
    return `
        <tr>
            <td>${object.id}</td>
            <td>${object.description}</td>
            <td>
                <button value="${object.id}" class="updateAboutUs"><i class="bi bi-pencil-fill"></i></button>
            </td>
        </tr>
    `;
}

function subjectHtml3(object) {
    return `
        <div class="item">
            <div class="div1">
                <img src="${object.imagePath}" alt="">
            </div>
            <div class="div2">
                <p>${object.name}</p>
                <div class="brief_description">${object.briefDescription}</div>
                <a href="${Application_Server_Address}/staff-member/subject-detail/${object.id}" class="aaa"><button>View</button></a>
            </div>
        </div>
    `;
}