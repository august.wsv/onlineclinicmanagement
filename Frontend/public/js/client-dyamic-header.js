let clientAccessToken = localStorage.getItem('client_access_token');
let ulElement =document.getElementById("Header-AccountTab-List");
if (clientAccessToken !== null) {    
    var linkListHTML = `
        <a href="${Application_Server_Address}/my-account">
            <li>My account</li>
        </a>
        <a href="${Application_Server_Address}/order">
            <li>Order</li>
        </a>
        <a href="${Application_Server_Address}/contact">
            <li>Contact</li>
        </a>
        <div id="logout-btn">
            <li>Logout</li>
        </div>
        
    `;
    
} else {
    var linkListHTML = `
        <a href="${Application_Server_Address}/login">
            <li>Login</li>
        </a>
        <a href="${Application_Server_Address}/register">
            <li>Register</li>
        </a>
    `;
}

ulElement.innerHTML = linkListHTML;

let rememberZIndex;
let openAccountTabGroupButton = document.getElementById("Header-MenuBtnGrp-AccountBtn");
let accountTabGroup = document.getElementById("Header-AccountTab");
let totalCartUpdate = document.getElementById("totalCartUpdate");
if (totalCartUpdate.innerHTML !== '') {
    totalCartUpdate.style.display = "block"
}

openAccountTabGroupButton.addEventListener("click", function () {
    if (accountTabGroup.style.display != "block") {
        accountTabGroup.style.display = "block";
    } else {
        accountTabGroup.style.display = "none";
    }
})


$(document).on('click', '#logout-btn', function() {
    var settings = {
        "url": `${Application_Server_Address}/remove-client-token`,
        "method": "POST"
    };
    $.ajax(settings).done();
    localStorage.removeItem('client_access_token');
    window.location.reload();
})