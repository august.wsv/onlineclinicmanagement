let staffMemberAccessToken = localStorage.getItem('staff_member_access_token');
let ulElement =document.getElementById("Header-AccountTab-List");
if (staffMemberAccessToken !== null) {    
    var linkListHTML = `
        <a href="${Application_Server_Address}/staff-member/my-account">
            <li>My account</li>
        </a>
        <div id="logout-btn">
            <li>Logout</li>
        </div>
        
    `;
    
} else {
    var linkListHTML = `
        <a href="${Application_Server_Address}/staff-member/login">
            <li>Login</li>
        </a>
    `;
}

ulElement.innerHTML = linkListHTML;

let rememberZIndex;
let openAccountTabGroupButton = document.getElementById("Header-MenuBtnGrp-AccountBtn");
let accountTabGroup = document.getElementById("Header-AccountTab");

openAccountTabGroupButton.addEventListener("click", function () {
    if (accountTabGroup.style.display != "block") {
        accountTabGroup.style.display = "block";
    } else {
        accountTabGroup.style.display = "none";
    }
})


$(document).on('click', '#logout-btn', function() {
    var settings = {
        "url": `${Application_Server_Address}/remove-staff-member-token`,
        "method": "POST"
    };
    $.ajax(settings).done();
    localStorage.removeItem('staff_member_access_token');
    window.location.reload();
})