var settings = {
    "url":  `${API_Server_Address}/Api/AboutUs/GetAll`,
    "method": "GET",
};

$.ajax(settings).done(function (response) {
    const data = response.data;
    $('#Xaddress').html(data.find(item => item.id === "address").description)
    $('#Xauth').html(data.find(item => item.id === "auth").description)
    $('#Xcompany').html(data.find(item => item.id === "company").description)
    $('#Xcopyright').html(data.find(item => item.id === "copyright").description)
    $('#Xfacebook').attr("href", data.find(item => item.id === "facebook").description);
    $('#Xphone').html(data.find(item => item.id === "phone").description)
    $('#Xyoutube').attr("href", data.find(item => item.id === "youtube").description);
});


