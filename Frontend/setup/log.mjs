import winston from 'winston';

class Log {
    errorLog(content) {
        return winston.createLogger({
            level: 'error',
            format: winston.format.simple(),
            transports: [new winston.transports.File({ filename: 'log/error.log' })]
        }).error(content);
    }

    debugLog(content) {
        return winston.createLogger({
            level: 'debug',
            format: winston.format.simple(),
            transports: [new winston.transports.File({ filename: 'log/debug.log' })]
        }).error(content.stack);
    }
}

export default new Log();