﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Backend.Data;
using Backend.Providers;
using System.Text;
using Microsoft.AspNetCore.Http.Features;
using Backend.Cron.Contracts;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
string? MyConnectString = builder.Configuration.GetConnectionString("DefaultConnectString");
ConfigurationManager configurationManager = builder.Configuration;
string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

//---CUSTOM---
//@Add Database Context
builder.Services.AddDbContext<DataContext>(option => option.UseSqlServer(MyConnectString));

//@Add
//  DI: Service, Repository, Cron
//  Auth: Policy
builder.Services.AddServices();

//@Add Limit Upload file size
builder.Services.Configure<FormOptions>(option =>
{
    option.MultipartBodyLengthLimit = (long)Math.Pow(1024, 3) * 2; //Limit 1GB
});

//@Add Authentication JWT
builder.Services
    .AddAuthentication()
    .AddJwtBearer(options => {
        options.SaveToken = true;
        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = new TokenValidationParameters()
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidAudience = configurationManager["JWT:ValidAudience"],
            ValidIssuer = configurationManager["JWT:ValidIssuer"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configurationManager["JWT:Secret"])),
            ValidateLifetime = true,
            ClockSkew = TimeSpan.Zero
        };
    }
);

//@Add Cors
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins, policy =>
    {
        policy.WithOrigins(configurationManager["ApplicationAddress"]).AllowAnyHeader().AllowAnyMethod();
    });
});

//@Add HttpContextAccessor
builder.Services.AddHttpContextAccessor();

//@Turn off default return validation
builder.Services.Configure<ApiBehaviorOptions>(options =>
{
    options.SuppressModelStateInvalidFilter = true;
});

//---DEFAULT---
builder.Services.AddControllers();

//___________________________________________________________________________________________________

WebApplication app = builder.Build();

//---CUSTOM---
//@Create table and add data
using (var scope = app.Services.CreateScope())
{
    IServiceProvider services = scope.ServiceProvider;
    DataContext context = services.GetRequiredService<DataContext>();
    DataInitializer.Seed(context);
}

//Use navigate status code
app.UseStatusCodePages(async context =>
{
    await StatusCodeErrorProvider.HandleUnauthorized(context.HttpContext);
});

//Use CORS
app.UseCors(MyAllowSpecificOrigins);

//Use static file
app.UseStaticFiles();

//Use authorization
app.UseAuthorization();

//@Run cron
app.Services.GetRequiredService<IBusinessReportCron>().DailyReport();

//----DEFAULT----
app.UseHttpsRedirection();

app.MapControllers();

app.Run();