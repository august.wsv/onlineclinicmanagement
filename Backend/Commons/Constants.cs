﻿namespace Backend.Commons
{
    public class Constants
    {
        public static Dictionary<string, int> Purchase = new Dictionary<string, int>
        {
            { "PENDING", 0 },
            { "CONFIRMEDANDSHIPPING", 1 },
            { "DELIVERED", 2 },
            { "ORDERCANCELEDBYUSER", 3 },
            { "ORDERCANCELEDBYADMIN", 4 },
            { "ROLLBACKBYADMIN", 5 },
            { "ROLLBACKBYUSER", 6 },
        };

        public static Dictionary<string, int> AdminRole = new Dictionary<string, int>
        {
            { "COLLABORATOR", 0 },
            { "SUPERADMIN", 1 }
        };

        public const int AdminRoleIsCollaborator = 0;
        public const int AdminRoleIsSuperAdmin = 1;

        public static Dictionary<string, int> PersonTypeId = new Dictionary<string, int>
        {
            { "ADMIN", 1 },
            { "CLIENT", 2 },
            { "STAFFMEMBER", 3}
        };

        public const int PersonTypeIsClient = 2;
        public const int PersonTypeIsStaffMember = 3;

        public const int DefaultPage = 1;
        public const int DefaultPageSize = 10;

        public const int ItemTypeIsMedicine = 1;
        public const int ItemTypeIsScientificApparatus = 2;
    }
}
