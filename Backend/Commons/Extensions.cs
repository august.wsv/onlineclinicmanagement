﻿namespace Backend.Commons
{
    public static class Extensions
    {
        public static IQueryable<T> Filter<T>(this IQueryable<T> query, bool condition, Func<IQueryable<T>, IQueryable<T>> predicate)
        {
            if (condition)
            { query = predicate(query); }
            return query;
        }
    }
}
