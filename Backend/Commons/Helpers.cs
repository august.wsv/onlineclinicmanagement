﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.Extensions;
using X.PagedList;

namespace Backend.Commons
{
    public class Helpers
    {
        public static JsonResult Respond(int status = 200, string message = "Success", object? data = null, object? meta = null, object? errors = null)
        {
            var responseObj = new
            {
                Status = status,
                Response = new
                {
                    Message = message,
                    Data = data,
                    Meta = meta,
                    Errors = errors
                }
            };

            return new JsonResult(responseObj.Response)
            {
                StatusCode = responseObj.Status,
                ContentType = "application/json"
            };
        }

        public static JsonResult GetErrorResponse(ModelStateDictionary modelState)
        {
            Dictionary<string, string> errors = modelState
                .Where(x => x.Value != null && x.Value.Errors.Count > 0)
                .ToDictionary(k => k.Key, v => v.Value.Errors.Select(e => e.ErrorMessage).First());
            return Respond(status: 422, message: "Error checking data", errors: errors);
        }

        private static string GetPageLink(int page, int pageSize, IHttpContextAccessor httpContextAccessor)
        {
            var httpcontext = httpContextAccessor.HttpContext;
            if (httpcontext == null)
            {
                return "URL_NOT_AVAILABLE";
            }
            HttpRequest request = httpcontext.Request;
            string baseUrl = $"{request.Scheme}://{request.Host}{request.PathBase}{request.Path}";
            IQueryCollection queryParameters = request.Query;
            var filteredQueryParameters = queryParameters
                .Where(q => q.Key != "page" && q.Key != "pageSize")
                .ToDictionary(q => q.Key, q => q.Value);
            QueryString queryString = new QueryBuilder(filteredQueryParameters).ToQueryString();
            string querySeparator = queryString.Value != "" ? "&" : "?";
            return $"{baseUrl}{queryString}{querySeparator}page={page}&pageSize={pageSize}";
        }

        public static object GetPaginationMetadata(IPagedList<object> objectPaged, int pageNumber, int pageSize, IHttpContextAccessor httpContextAccessor)
        {
            return new
            {
                page = pageNumber,
                pageCount = objectPaged.PageCount == 0 ? 1 : objectPaged.PageCount,
                prevPageLink = objectPaged.HasPreviousPage ? GetPageLink(pageNumber - 1, pageSize, httpContextAccessor) : null,
                nextPageLink = objectPaged.HasNextPage ? GetPageLink(pageNumber + 1, pageSize, httpContextAccessor) : null,
                firstPageLink = GetPageLink(1, pageSize, httpContextAccessor),
                lastPageLink = GetPageLink(objectPaged.PageCount == 0 ? 1: objectPaged.PageCount, pageSize, httpContextAccessor)
            };
        }

        public static async Task<string> UploadFile<TFile>(TFile? fileField, string folderName, IWebHostEnvironment env) where TFile : IFormFile
        {
            if (fileField == null)
            { return "ERROR"; }
            string folderPath = Path.Combine(env.WebRootPath, folderName);
            if (!Directory.Exists(folderPath)) 
            { Directory.CreateDirectory(folderPath); }
            string fileName = $"{DateTime.Now:yyyyMMddHHmmss}_{fileField.FileName}";
            using (var stream = new FileStream(Path.Combine(env.WebRootPath, folderName, fileName), FileMode.Create))
            { await fileField.CopyToAsync(stream); }
            return $"/{folderName}/{fileName}";
        }
    }
}
