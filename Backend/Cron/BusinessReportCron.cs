﻿using Backend.Cron.Action;
using Backend.Cron.Contracts;

namespace Backend.Cron
{
    public class BusinessReportCron : IBusinessReportCron
    {
        private readonly BusinessReportAction _businessReportCronJob;

        public BusinessReportCron(IServiceProvider serviceProvider)
        {
            _businessReportCronJob = new BusinessReportAction(serviceProvider);
        }

        public void DailyReport()
        {
            //DateTime now = DateTime.Now;
            //TimerCallback mailCronJobEventTimerCallback = new TimerCallback(new BusinessReportCronJob(_serviceProvider).Event);
            //DateTime dailyScheduledTime = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
            //if (now > dailyScheduledTime)
            //{
            //    dailyScheduledTime = dailyScheduledTime.AddDays(1);
            //}
            //int dailyDelay = (int)(dailyScheduledTime - now).TotalMilliseconds;
            //Timer dailyTimer = new Timer(mailCronJobEventTimerCallback, null, dailyDelay, (int)TimeSpan.FromHours(24).TotalMilliseconds);

            TimerCallback reportCronJobEventTimerCallback = new TimerCallback(_businessReportCronJob.Event);
            Timer timer = new Timer(reportCronJobEventTimerCallback, null, 0, (int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        }
    }
}