﻿using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;

namespace Backend.Cron.Action

{
    public class BusinessReportAction
    {
        private readonly IServiceProvider _serviceProvider;

        public BusinessReportAction(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async void Event(object state)
        {
            Console.WriteLine("BusinessReport record added to the database at" + DateTime.Now);
            using (var scope = _serviceProvider.CreateScope())
            {
                var businessReportRepository = scope.ServiceProvider.GetRequiredService<IBusinessReportRepository>();
                var purchaseRepository = scope.ServiceProvider.GetRequiredService<IPurchaseRepository>();
                BusinessReportCreateRequest businessReportCreateRequest = await purchaseRepository.GetTotalDaily();
                await businessReportRepository.Create(
                    new BusinessReport
                    {
                        TotalSales = businessReportCreateRequest.TotalSales,
                        OrderCount = businessReportCreateRequest.OrderCount
                    }
                );
            }
        }
    }
}