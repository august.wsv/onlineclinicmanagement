﻿namespace Backend.Cron.Contracts
{
    public interface IBusinessReportCron
    {
        void DailyReport();
    }
}
