﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Backend.Models
{
    public class Clinic
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name must not be null!")]
        public string? Name { get; set; }    

        [Required(ErrorMessage = "Phone must not be null!")]
        public string? Phone { get; set; }

        [Required(ErrorMessage = "Email must not be null!")]
        [EmailAddress(ErrorMessage = "Email is invalid!")]
        public string? Email { get; set; }

        [Required(ErrorMessage = "Address must not be null!")]
        public string? Address { get; set; }

        [JsonIgnore]
        public DateTime? DeletedAt { get; set; }

        [JsonIgnore]
        public virtual ICollection<Client>? Clients { get; }
    }
}
