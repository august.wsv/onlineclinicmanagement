﻿using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public interface IEntity
    {
        long Id { get; set; }
    }
}
