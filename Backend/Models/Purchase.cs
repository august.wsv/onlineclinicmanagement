﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Backend.Commons;

namespace Backend.Models
{
    public class Purchase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "ClientId must not be null!")]
        [ForeignKey(nameof(Client.Id))]
        [JsonIgnore]
        public int ClientId { get; set; }

        [Required(ErrorMessage = "Status must not be null!")]
        public int Status { get; set; } = Constants.Purchase["PENDING"];

        [Required(ErrorMessage = "Address must not be null!")]
        public string? Address { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [NotMapped]
        [JsonIgnore]
        public decimal? TotalSales { get; set; }

        [NotMapped]
        [JsonIgnore]
        public int? OrderCount { get; set; }


        [NotMapped]
        public string? TotalString { get; set; }

        public virtual ICollection<PurchaseDetail>? PurchaseDetails { get; }

        public virtual Client? AboutClient { get; set; }
    }
}
