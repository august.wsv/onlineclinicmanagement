﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Backend.Models
{
    public class Client
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name must not be null!")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "Phone must not be null!")]
        public string? Phone { get; set; }

        [Required(ErrorMessage = "Email must not be null!")]
        [EmailAddress(ErrorMessage = "Email is invalid!")]
        public string? Email { get; set; }

        public string? Address { get; set; }

        [Required(ErrorMessage = "Password must not be null!")]
        [MinLength(8)]
        [JsonIgnore]
        public string? Password { get; set; }

        [Required(ErrorMessage = "ClinicId must not be null!")]
        [ForeignKey(nameof(Clinic.Id))]        
        public int ClinicId { get; set; }

        [JsonIgnore]
        public DateTime? DeletedAt { get; set; }

        [JsonIgnore]
        public virtual ICollection<Cart>? Carts { get; }

        [JsonIgnore]
        public virtual ICollection<ClientMessage>? ClientMessages { get; }

        [JsonIgnore]
        public virtual ICollection<Feedback>? Feedbacks { get; }

        [JsonIgnore]
        public virtual ICollection<Purchase>? Purchases { get; }

        public virtual Clinic? AboutClinic { get; set; }
    }
}
