﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Backend.Models
{
    public class ClientMessage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "ClientId must not be null!")]
        [ForeignKey(nameof(Client.Id))]
        public int ClientId { get; set; }

        [Required(ErrorMessage = "Title must not be null!")]
        public string? Title { get; set; }

        [Required(ErrorMessage = "Content must not be null!")]
        public string? Content { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public virtual Client? AboutClient { get; set; }
    }
}
