﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Backend.Models
{
    public class EducationalEvent
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name must not be null!")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "Description must not be null!")]
        public string? BriefDescription { get; set; }

        [Required(ErrorMessage = "Description must not be null!")]
        public string? Description { get; set; }

        [Required(ErrorMessage = "VideoPath must not be null!")]
        public string? VideoPath { get; set; }

        [Required(ErrorMessage = "EducationalEventType must not be null!")]
        public int? EducationalEventType { get; set; }

        [Required(ErrorMessage = "SubjectId must not be null!")]
        [ForeignKey(nameof(Subject.Id))]
        public int SubjectId { get; set; }

        [Required(ErrorMessage = "StaffMemberId must not be null!")]
        [ForeignKey(nameof(StaffMember.Id))]
        public int StaffMemberId { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [JsonIgnore]
        public DateTime? DeletedAt { get; set; }

        [JsonIgnore]
        public virtual ICollection<Comment>? Comments { get; }
    }
}
