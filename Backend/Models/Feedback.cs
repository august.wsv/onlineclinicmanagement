﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Backend.Models
{
    public class Feedback
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey(nameof(Client.Id))]
        [Required(ErrorMessage = "ClientId must not be null!")]
        public int ClientId { get; set; }

        [Required(ErrorMessage = "Detail must not be null!")]
        public string? Detail { get; set; }

        [Required(ErrorMessage = "Rating must not be null!")]
        [Range(1, 5, ErrorMessage = "Rating must be between 0 and 5.")]
        public int? Rating { get; set; }

        [Required(ErrorMessage = "ItemId must not be null!")]
        public int? ItemId { get; set; }

        [Required(ErrorMessage = "ItemType must not be null!")]
        public int? ItemType { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public virtual Client? AboutClient { get; set; }
    }
}
