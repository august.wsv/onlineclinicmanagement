﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Backend.Models
{
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "EducationalEventId must not be null!")]
        [ForeignKey(nameof(EducationalEvent.Id))]
        public int? EducationalEventId { get; set; }

        [JsonIgnore]
        [Required(ErrorMessage = "PersonId must not be null!")]
        public int PersonId { get; set; }

        [Required(ErrorMessage = "PersonType must not be null!")]
        public int PersonType { get; set; }

        [NotMapped]
        public string? PersonName { get; set; }

        [Required(ErrorMessage = "Content must not be null!")]
        public string? Content { get; set; }

        public int? ParentId { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
