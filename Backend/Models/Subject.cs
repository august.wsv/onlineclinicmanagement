﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Backend.Models
{
    public class Subject
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name must not be null!")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "Phone must not be null!")]
        public string? ImagePath { get; set; }

        [Required(ErrorMessage = "Description must not be null!")]
        public string? BriefDescription { get; set; }

        [JsonIgnore]
        public DateTime? DeletedAt { get; set; }

        public virtual ICollection<EducationalEvent>? EducationalEvents { get; set; }
    }
}
