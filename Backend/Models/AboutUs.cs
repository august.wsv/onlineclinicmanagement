﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class AboutUs : BaseModel, IEntity
    {
        [Required]
        public string? Key { get; set; }

        [Required]
        public string? Value { get; set; }
    }
}
