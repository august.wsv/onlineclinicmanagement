﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Backend.Models
{
    public class Medicine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name must not be null!")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "ImagePath must not be null!")]
        public string? ImagePath { get; set; }

        [Required(ErrorMessage = "Code must not be null!")]
        public string? Code { get; set; }

        [Required(ErrorMessage = "Quantity must not be null!")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "Price must not be null!")]
        public decimal Price { get; set; }

        [NotMapped]
        public string PriceString { get { return string.Format("{0:C}", Price); } }

        [Required(ErrorMessage = "Description must not be null!")]
        public string? Description { get; set; }

        [Required(ErrorMessage = "MedicineTypeId must not be null!")]
        [ForeignKey(nameof(MedicineType.Id))]
        public int MedicineTypeId { get; set; }

        [Required(ErrorMessage = "Date must not be null!")]
        public DateTime? ExpDate { get; set; }

        [JsonIgnore]
        public DateTime? DeletedAt { get; set; }

        public virtual MedicineType? AboutMedicineType { get; set; }
    }
}
