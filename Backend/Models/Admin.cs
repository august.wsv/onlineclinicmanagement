﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Backend.Models
{
    public class Admin : BaseModel
    {
        [Required(ErrorMessage = "Name must not be null!")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "Username must not be null!")]
        public string? Username { get; set; }

        [Required(ErrorMessage = "Password must not be null!")]
        [MinLength(8)]
        [JsonIgnore]
        public string? Password { get; set; }

        [Required]
        public int? Role { get; set; }

        [NotMapped]
        public string? Rolename { get; set; }
    }
}
