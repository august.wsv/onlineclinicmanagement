﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Backend.Models
{
    public class Cart
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "ClientId must not be null!")]
        [ForeignKey(nameof(Client.Id))]
        [JsonIgnore]
        public int ClientId { get; set; }

        [NotMapped]
        public string ItemName { get; set; }

        [NotMapped]
        public string ItemTypeName { get; set; }

        [Required(ErrorMessage = "ItemId must not be null!")]
        public int ItemId { get; set; }

        [Required(ErrorMessage = "ItemType must not be null!")]
        public int ItemType { get; set; }

        [Required(ErrorMessage = "Quantity must not be null!")]
        public int Quantity { get; set; } = 1;

        [Required(ErrorMessage = "Price must not be null!")]
        [JsonIgnore]
        public decimal Price { get; set; }

        [NotMapped]
        public string PriceString { get { return string.Format("{0:C}", Price); } }

        [NotMapped]
        public string TotalString { get { return string.Format("{0:C}", (Price * Quantity)); } }

        [NotMapped]
        public string ImagePath { get; set; }

        [NotMapped]
        public int StoreQuantity { get; set; }
    }
}
