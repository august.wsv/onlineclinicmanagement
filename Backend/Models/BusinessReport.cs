﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Models
{
    public class BusinessReport
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public decimal? TotalSales { get; set; }

        [Required]
        public int? OrderCount { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
