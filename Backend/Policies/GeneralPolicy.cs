﻿using Microsoft.AspNetCore.Authorization;
using Backend.Commons;

namespace Backend.Policies
{
    public class GeneralPolicy
    {
        public static void CanUpdatePurchaseStatus(AuthorizationPolicyBuilder policy)
        {
            policy.RequireAssertion(context =>
            {
                var personTypeClaim = context.User.FindFirst("PersonType");
                return personTypeClaim != null && (personTypeClaim.Value.Equals("Client") || personTypeClaim.Value.Equals("Admin"));
            });
        }

        public static void CanUpdateStaffMember(AuthorizationPolicyBuilder policy)
        {
            policy.RequireAssertion(context =>
            {
                var personTypeClaim = context.User.FindFirst("PersonType");
                if (personTypeClaim != null && personTypeClaim.Value.Equals("Admin"))
                {
                    var adminRoleClaim = context.User.FindFirst("AdminRole");

                    if (adminRoleClaim != null && int.TryParse(adminRoleClaim.Value, out int adminRole))
                    {
                        return adminRole == Constants.AdminRole["SUPERADMIN"];
                    }
                } else if(personTypeClaim != null && personTypeClaim.Value.Equals("StaffMember"))
                {
                    return true;
                }               

                return false;
            });
        }

        public static void CanCreateComment(AuthorizationPolicyBuilder policy)
        {
            policy.RequireAssertion(context =>
            {
                var personTypeClaim = context.User.FindFirst("PersonType");
                return personTypeClaim != null && (personTypeClaim.Value.Equals("Client") || personTypeClaim.Value.Equals("StaffMember"));
            });
        }
    }
}
