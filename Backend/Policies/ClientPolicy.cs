﻿using Microsoft.AspNetCore.Authorization;

namespace Backend.Policies
{
    public class ClientPolicy
    {
        public static void Client(AuthorizationPolicyBuilder policy)
        {
            policy.RequireAssertion(context =>
            {
                var personTypeClaim = context.User.FindFirst("PersonType");  
                return personTypeClaim != null && personTypeClaim.Value.Equals("Client");
            });
        }
    }
}
