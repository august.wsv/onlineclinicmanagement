﻿using Microsoft.AspNetCore.Authorization;
using Backend.Commons;

namespace Backend.Policies
{
    public class AdminPolicy
    {
        public static void JustSuperAdmin(AuthorizationPolicyBuilder policy)
        {
            policy.RequireAssertion(context =>
            {
                var adminRoleClaim = context.User.FindFirst("AdminRole");

                if (adminRoleClaim != null && int.TryParse(adminRoleClaim.Value, out int adminRole))
                {
                    return adminRole == Constants.AdminRole["SUPERADMIN"];
                }

                return false;
            });
        }

        public static void JustCollaborator(AuthorizationPolicyBuilder policy)
        {
            policy.RequireAssertion(context =>
            {
                var adminRoleClaim = context.User.FindFirst("AdminRole");

                if (adminRoleClaim != null && int.TryParse(adminRoleClaim.Value, out int adminRole))
                {
                    return adminRole == Constants.AdminRole["COLLABORATOR"];
                }

                return false;
            });
        }

        public static void AllAdmin(AuthorizationPolicyBuilder policy)
        {
            policy.RequireAssertion(context =>
            {
                var adminRoleClaim = context.User.FindFirst("AdminRole");

                if (adminRoleClaim != null && int.TryParse(adminRoleClaim.Value, out int adminRole))
                {
                    return adminRole == Constants.AdminRole["COLLABORATOR"] || adminRole == Constants.AdminRole["SUPERADMIN"];
                }

                return false;
            });
        }
    }
}
