﻿using Microsoft.AspNetCore.Authorization;

namespace Backend.Policies
{
    public class StaffMemberPolicy
    {
        public static void StaffMember(AuthorizationPolicyBuilder policy)
        {
            policy.RequireAssertion(context =>
            {
                var personTypeClaim = context.User.FindFirst("PersonType");  
                return personTypeClaim != null && personTypeClaim.Value.Equals("StaffMember");
            });
        }
    }
}
