﻿using Backend.Models;
using Backend.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Request
{
    public class MedicineGetListRequest
    {
        public string? Name { get; set; }

        public string? Code { get; set; }

        public decimal? MaxPrice { get; set; }

        public decimal? MinPrice { get; set; }

        public int? MedicineTypeId { get; set; }

        public DateTime? MaxExpDate { get; set; }

        public DateTime? MinExpDate { get; set; }

        public int Page { get; set; }
        
        public int PageSize { get; set; }
    }

    public class MedicineGetOneRequest
    {
        [Required]
        public int? Id { get; set; }
    }

    public class MedicineCreateRequest
    {
        [Required(ErrorMessage = "Name must not be null!")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "ImagePath must not be null!")]
        [MaxFileSize(5 * 1024 * 1024, ErrorMessage = "The file size should not exceed 5 MB.")]
        [AllowedImageFile(ErrorMessage = "Only image files (JPEG, PNG, GIF) are allowed.")]
        public IFormFile? ImagePath { get; set; }

        [Required(ErrorMessage = "Code must not be null!")]
        public string? Code { get; set; }

        [Required(ErrorMessage = "Quantity must not be null!")]
        public int? Quantity { get; set; }

        [Required(ErrorMessage = "Price must not be null!")]
        public decimal? Price { get; set; }

        [Required(ErrorMessage = "Description must not be null!")]
        public string? Description { get; set; }

        [Required(ErrorMessage = "MedicineTypeId must not be null!")]
        [ForeignKey(nameof(MedicineType.Id))]
        public int? MedicineTypeId { get; set; }

        [Required(ErrorMessage = "Date must not be null!")]
        public DateTime? ExpDate { get; set; }
    }

    public class MedicineUpdateRequest
    {
        public string? Name { get; set; }

        [MaxFileSize(5 * 1024 * 1024, ErrorMessage = "The file size should not exceed 5 MB.")]
        [AllowedImageFile(ErrorMessage = "Only image files (JPEG, PNG, GIF) are allowed.")]
        public IFormFile? ImagePath { get; set; }

        public string? Code { get; set; }

        public int? Quantity { get; set; }

        public float? Price { get; set; }

        public string? Description { get; set; }

        [ForeignKey(nameof(MedicineType.Id))]
        public int? MedicineTypeId { get; set; }

        public DateTime? ExpDate { get; set; }
    }
}
