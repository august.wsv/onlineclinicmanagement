﻿using System.ComponentModel.DataAnnotations;

namespace Backend.Request
{
    public class ClinicGetListRequest
    {
        public string? Name { get; set; }

        public string? Phone { get; set; }

        public string? Email { get; set; }

        public string? Address { get; set; }

        public int Page { get; set; }
        
        public int PageSize { get; set; }
    }

    public class ClinicGetOneRequest
    {
        [Required]
        public int? Id { get; set; }
    }

    public class ClinicCreateRequest
    {
        [Required(ErrorMessage = "Name must not be null!")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "Phone must not be null!")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "Phone number is not valid.")]
        public string? Phone { get; set; }

        [Required(ErrorMessage = "Email must not be null!")]
        [EmailAddress(ErrorMessage = "Email is invalid!")]
        [RegularExpression(@"^.+@clinic\.com$", ErrorMessage = "Email is invalid or does not end with @clinic.com")]
        public string? Email { get; set; }

        [Required(ErrorMessage = "Address must not be null!")]
        public string? Address { get; set; }
    }

    public class ClinicUpdateRequest
    {
        public string? Name { get; set; }

        [RegularExpression(@"^\d{10}$", ErrorMessage = "Phone number is not valid.")]
        public string? Phone { get; set; }

        [EmailAddress(ErrorMessage = "Email is invalid!")]
        [RegularExpression(@"^.+@clinic\.com$", ErrorMessage = "Email is invalid or does not end with @clinic.com")]
        public string? Email { get; set; }

        public string? Address { get; set; }
    }
}
