﻿using Backend.Validation;
using System.ComponentModel.DataAnnotations;

namespace Backend.Request
{
    public class FileUploadDto
    {
        [Required(ErrorMessage = "File must not be null!")]
        [MaxFileSize(6 * 1024 * 1024, ErrorMessage = "The file size should not exceed 6 MB.")]
        public IFormFile? File { get; set; }

        //[Required(ErrorMessage = "File must not be null!")]
        //[AllowedImageFile(ErrorMessage = "Only image files (JPEG, PNG, GIF) are allowed.")]
        [MaxFileSize(1 * 1024 * 1024, ErrorMessage = "The file size should not exceed 1 MB.")]
        public List<IFormFile>? Files { get; set; }
    }
}
