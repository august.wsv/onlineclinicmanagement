﻿using Backend.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Request
{
    public class ClientLoginRequest
    {
        [Required(ErrorMessage = "Email must not be null!")]
        [EmailAddress(ErrorMessage = "Email is invalid!")]
        public string? Email { get; set; }

        [Required(ErrorMessage = "Password must not be null!")]
        [MinLength(8)]
        public string? Password { get; set; }
    }

    public class ClientEmailVerificationRequest
    {
        [Required(ErrorMessage = "Name is required.")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "Phone must not be null!")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "Phone number is not valid.")]
        public string? Phone { get; set; }

        [Required(ErrorMessage = "Email must not be null!")]
        [EmailAddress(ErrorMessage = "Email is invalid!")]
        public string? Email { get; set; }

        [Required(ErrorMessage = "Password must not be null!")]
        [MinLength(8)]
        public string? Password { get; set; }

        [Required(ErrorMessage = "ClinicId must not be null!")]
        [ForeignKey(nameof(Clinic.Id))]
        public int? ClinicId { get; set; }
    }

    public class ClientRegisterRequest
    {
        [Required(ErrorMessage = "Code is required.")]
        public string Code { get; set; }
    }

    public class ClientGetOneByEmailRequest
    {
        [Required(ErrorMessage = "Email must not be null!")]
        [EmailAddress(ErrorMessage = "Email is invalid!")]
        public string? Email { get; set; }
    }

    public class ClientWithCodeRequest
    {
        public Client Client { get; set; }
        public string Code { get; set; }
    }

    public class ClientGetOneRequest
    {
        [Required]
        public int? Id { get; set; }
    }

    public class ClientUpdateRequest
    {
        public string? Name { get; set; }

        [RegularExpression(@"^\d{10}$", ErrorMessage = "Phone number is not valid.")]
        public string? Phone { get; set; }

        public string? Address { get; set; }

        [MinLength(8)]
        public string? Password { get; set; }

        [ForeignKey(nameof(Clinic.Id))]
        public int? ClinicId { get; set; }
    }

    public class ClientGetListRequest
    {
        public string? Name { get; set; }

        public string? Phone { get; set; }

        public string? Email { get; set; }

        public string? Address { get; set; }

        public string? ClinicName { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }
    }
}
