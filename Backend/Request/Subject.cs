﻿using Backend.Validation;
using System.ComponentModel.DataAnnotations;

namespace Backend.Request
{
    public class SubjectGetListRequest
    {
        public string? Name { get; set; }

        public int Page { get; set; }
        
        public int PageSize { get; set; }
    }

    public class SubjectGetOneRequest
    {
        [Required]
        public int? Id { get; set; }
    }

    public class SubjectCreateRequest
    {
        [Required(ErrorMessage = "Name must not be null!")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "ImagePath must not be null!")]
        [MaxFileSize(5 * 1024 * 1024, ErrorMessage = "The file size should not exceed 5 MB.")]
        [AllowedImageFile(ErrorMessage = "Only image files (JPEG, PNG, GIF) are allowed.")]
        public IFormFile? ImagePath { get; set; }

        [Required(ErrorMessage = "BriefDescription must not be null!")]
        public string? BriefDescription { get; set; }
    }

    public class SubjectUpdateRequest
    {
        public string? Name { get; set; }

        [MaxFileSize(5 * 1024 * 1024, ErrorMessage = "The file size should not exceed 5 MB.")]
        [AllowedImageFile(ErrorMessage = "Only image files (JPEG, PNG, GIF) are allowed.")]
        public IFormFile? ImagePath { get; set; }

        public string? BriefDescription { get; set; }
    }
}
