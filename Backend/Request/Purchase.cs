﻿using System.ComponentModel.DataAnnotations;

namespace Backend.Request
{
    public class PurchaseGetPagedListRequest
    {
        public string? ClientName { get; set; }

        public string? ClientEmail { get; set; }

        public int? Status { get; set; }

        public string? Address { get; set; }

        public DateTime MaxCreatedAt { get; set; }

        public DateTime MinCreatedAt { get; set; }
        
        public int Page { get; set; }
        
        public int PageSize { get; set; }
    }

    public class PurchaseGetListRequest
    {
        [Required(ErrorMessage = "ClientId must not be null!")]
        public int? ClientId { get; set; }
    }

    public class PurchaseGetOneRequest
    {
        [Required]
        public int? Id { get; set; }
    }

    public class PurchaseCreateRequest
    {
        [Required(ErrorMessage = "Address must not be null!")]
        public string? Address { get; set; }
    }

    public class PurchaseUpdateRequest
    {
        [Required(ErrorMessage = "Status must not be null!")]
        public int Status { get; set; }
    }
}
