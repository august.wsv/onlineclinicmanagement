﻿using Backend.Commons;
using System.ComponentModel.DataAnnotations;

namespace Backend.Request
{
    public class AdminLoginRequest
    {
        [Required(ErrorMessage = "Username must not be null!")]
        public string? Username { get; set; }

        [Required(ErrorMessage = "Password must not be null!")]
        [MinLength(8)]
        public string? Password { get; set; }
    }

    public class AdminRegisterRequest
    {
        [Required(ErrorMessage = "Name is required.")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "Username must not be null!")]
        public string? Username { get; set; }

        [Required(ErrorMessage = "Password must not be null!")]
        [MinLength(8)]
        public string? Password { get; set; }

        public int Role { get; set; } = Constants.AdminRole["COLLABORATOR"];
    }

    public class AdminGetOneByUsernameRequest
    {
        [Required(ErrorMessage = "Username must not be null!")]
        public string? Username { get; set; }
    }

    public class AdminGetOneRequest
    {
        [Required]
        public int? Id { get; set; }
    }

    public class AdminGetListRequest
    {
        public string? Name { get; set; }

        public string? Username { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }
    }

    public class AdminUpdateRequest
    {
        public string? Name { get; set; }

        public string? Username { get; set; }

        [MinLength(8)]
        public string? Password { get; set; }
    }
}
