﻿using System.ComponentModel.DataAnnotations;

namespace Backend.Request
{
    public class BusinessReportGetPagedListRequest
    {
        public DateTime MaxCreatedAt { get; set; }

        public DateTime MinCreatedAt { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }
    }

    public class BusinessReportGetOneRequest
    {
        [Required]
        public int? Id { get; set; }
    }

    public class BusinessReportCreateRequest
    {
        [Required]
        public decimal? TotalSales { get; set; }

        [Required]
        public int? OrderCount { get; set; }
    }
}
