﻿using System.ComponentModel.DataAnnotations;

namespace Backend.Request
{
    public class CartGetListRequest
    {
        [Required(ErrorMessage = "ClientId must not be null!")]
        public int? ClientId { get; set; }
    }

    public class CartGetOneRequest
    {
        [Required]
        public int? Id { get; set; }
    }

    public class CartCreateRequest
    {
        [Required(ErrorMessage = "ItemId must not be null!")]
        public int? ItemId { get; set; }

        [Required(ErrorMessage = "ItemType must not be null!")]
        public int? ItemType { get; set; }
    }

    public class CartCheckItemRequest
    {
        [Required(ErrorMessage = "ClientId must not be null!")]
        public int? ClientId { get; set; }

        [Required(ErrorMessage = "ItemId must not be null!")]
        public int? ItemId { get; set; }

        [Required(ErrorMessage = "ItemType must not be null!")]
        public int? ItemType { get; set; }

    }

    public class CartUpdateRequest
    {
        [Required(ErrorMessage = "Quantity must not be null!")]
        public int? Quantity { get; set; }
    }
}
