﻿using Backend.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Request
{
    public class FeedbackGetListRequest
    {
        public int? ItemId { get; set; }

        public int? ItemType { get; set; }
    }
    
    public class FeedbackCreateRequest
    {
        [Required(ErrorMessage = "Detail must not be null!")]
        public string? Detail { get; set; }

        [Required(ErrorMessage = "Rating must not be null!")]
        [Range(1, 5, ErrorMessage = "Rating must be between 0 and 5.")]
        public int? Rating { get; set; }

        [Required(ErrorMessage = "ItemId must not be null!")]
        public int? ItemId { get; set; }

        [Required(ErrorMessage = "ItemType must not be null!")]
        public int? ItemType { get; set; }
    }

    public class FeedbacktCheckExistRequest 
    {
        [Required]
        public int? ClientId { get; set; }

        [Required]
        public int? ItemId { get; set; }

        [Required]
        public int? ItemType { get; set; }
    }
}
