﻿using Backend.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Request
{
    public class ClientMessageGetListRequest
    {
        public string? ClientName { get; set; }

        public string? ClientPhone { get; set; }

        public string? ClientEmail { get; set; }

        public string? Title { get; set; }

        public DateTime MaxCreatedAt { get; set; }

        public DateTime MinCreatedAt { get; set; }

        public int Page { get; set; }
        
        public int PageSize { get; set; }
    }

    public class ClientMessageGetOneRequest
    {
        [Required]
        public int? Id { get; set; }
    }

    public class ClientMessageCreateRequest
    {
        [Required(ErrorMessage = "Title must not be null!")]
        public string? Title { get; set; }

        [Required(ErrorMessage = "Content must not be null!")]
        public string? Content { get; set; }
    }
}
