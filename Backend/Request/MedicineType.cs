﻿using System.ComponentModel.DataAnnotations;

namespace Backend.Request
{
    public class MedicineTypeGetListRequest
    {
        public string? Name { get; set; }

        public int Page { get; set; }
        
        public int PageSize { get; set; }
    }

    public class MedicineTypeGetOneRequest
    {
        [Required]
        public int? Id { get; set; }
    }

    public class MedicineTypeCreateRequest
    {
        [Required(ErrorMessage = "Name must not be null!")]
        public string? Name { get; set; }
    }

    public class MedicineTypeUpdateRequest
    {
        public string? Name { get; set; }
    }
}
