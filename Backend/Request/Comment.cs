﻿using Backend.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Request
{
    public class CommentGetListRequest
    {
        public int? EducationalEventId { get; set; }
    }
    
    public class CommentCreateRequest
    {
        [Required(ErrorMessage = "EducationalEventId must not be null!")]
        [ForeignKey(nameof(EducationalEvent.Id))]
        public int? EducationalEventId { get; set; }

        [Required(ErrorMessage = "Content must not be null!")]
        public string? Content { get; set; }

        public int? ParentId { get; set; }
    }
}
