﻿using System.ComponentModel.DataAnnotations;

namespace Backend.Request
{
    public class AboutUsGetOneRequest
    {
        [Required]
        public string? Id { get; set; }
    }

    public class AboutUsUpdateRequest
    {
        public string? Description { get; set; }
    }
}
