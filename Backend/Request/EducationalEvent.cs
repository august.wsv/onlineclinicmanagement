﻿using Backend.Models;
using Backend.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Request
{
    public class EducationalEventGetListRequest
    {
        public string? Name { get; set; }

        public int? EducationalEventType { get; set; }

        public int? SubjectId { get; set; }

        public int? StaffMemberId { get; set; }

        public DateTime? MaxCreatedAt { get; set; }

        public DateTime? MinCreatedAt { get; set; }

        public int Page { get; set; }
        
        public int PageSize { get; set; }
    }

    public class EducationalEventGetOneRequest
    {
        [Required]
        public int? Id { get; set; }
    }

    public class EducationalEventCreateRequest
    {
        [Required(ErrorMessage = "Name must not be null!")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "Description must not be null!")]
        public string? BriefDescription { get; set; }

        [Required(ErrorMessage = "Description must not be null!")]
        public string? Description { get; set; }

        [Required(ErrorMessage = "VideoPath must not be null!")]
        [MaxFileSize(1024 * 1024 * 1024, ErrorMessage = "The file size should not exceed 1GB.")]
        [AllowedVideoFile(ErrorMessage = "Only image files (MP4,...) are allowed.")]
        public IFormFile? VideoPath { get; set; }

        [Required(ErrorMessage = "EducationalEventType must not be null!")]
        public int? EducationalEventType { get; set; }

        [Required(ErrorMessage = "SubjectId must not be null!")]
        [ForeignKey(nameof(Subject.Id))]
        public int? SubjectId { get; set; }
    }

    public class EducationalEventUpdateRequest
    {
        public string? Name { get; set; }

        public string? BriefDescription { get; set; }

        public string? Description { get; set; }

        [MaxFileSize(100 * 1024 * 1024, ErrorMessage = "The file size should not exceed 50 MB.")]
        [AllowedVideoFile(ErrorMessage = "Only image files (MP4,...) are allowed.")]
        public IFormFile? VideoPath { get; set; }

        public int? EducationalEventType { get; set; }

        [ForeignKey(nameof(Subject.Id))]
        public int? SubjectId { get; set; }
    }
}
