﻿using System.ComponentModel.DataAnnotations;

namespace Backend.Request
{
    public class StaffMemberLoginRequest
    {
        [Required(ErrorMessage = "Email must not be null!")]
        [EmailAddress(ErrorMessage = "Email is invalid!")]
        public string? Email { get; set; }

        [Required(ErrorMessage = "Password must not be null!")]
        [MinLength(8)]
        public string? Password { get; set; }
    }

    public class StaffMemberRegisterRequest
    {
        [Required(ErrorMessage = "Name is required.")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "Phone must not be null!")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "Phone number is not valid.")]
        public string? Phone { get; set; }

        [Required(ErrorMessage = "Email must not be null!")]
        [EmailAddress(ErrorMessage = "Email is invalid!")]
        public string? Email { get; set; }

        [Required(ErrorMessage = "Password must not be null!")]
        [MinLength(8)]
        public string? Password { get; set; }
    }

    public class StaffMemberGetOneByEmailRequest
    {
        [Required(ErrorMessage = "Email must not be null!")]
        [EmailAddress(ErrorMessage = "Email is invalid!")]
        public string? Email { get; set; }
    }

    public class StaffMemberGetOneRequest
    {
        [Required]
        public int? Id { get; set; }
    }

    public class StaffMemberGetListRequest
    {
        public string? Name { get; set; }

        public string? Phone { get; set; }

        public string? Email { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }
    }

    public class StaffMemberUpdateRequest
    {
        public string? Name { get; set; }

        [RegularExpression(@"^\d{10}$", ErrorMessage = "Phone number is not valid.")]
        public string? Phone { get; set; }

        [EmailAddress(ErrorMessage = "Email is invalid!")]
        public string? Email { get; set; }

        [MinLength(8)]
        public string? Password { get; set; }

        public string? PersonalBiography { get; set; }
    }
}
