﻿using System.ComponentModel.DataAnnotations;

namespace Backend.Validation
{
    public class MaxFileSizeAttribute : ValidationAttribute
    {
        private readonly long _maxFileSize;

        public MaxFileSizeAttribute(long maxFileSize)
        {
            _maxFileSize = maxFileSize;
        }

        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }

            var files = value as List<IFormFile>;

            if (files == null)
            {
                files = new List<IFormFile> { value as IFormFile };
            }

            if (files == null || files.Count == 0)
            {
                return ValidationResult.Success;
            }

            foreach (var file in files)
            {
                if (file == null || file.Length > _maxFileSize)
                {
                    return new ValidationResult(ErrorMessage);
                }
            }

            return ValidationResult.Success;
        }
    }
}
