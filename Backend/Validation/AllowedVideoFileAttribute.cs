﻿using System.ComponentModel.DataAnnotations;

namespace Backend.Validation
{
    public class AllowedVideoFileAttribute : ValidationAttribute
    {
        private readonly string[] _allowedMimeTypes = { "video/mp4", "video/quicktime", "video/x-msvideo" };

        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }

            var files = value as List<IFormFile>;

            if (files == null)
            {
                files = new List<IFormFile> { value as IFormFile };
            }

            if (files == null || files.Count == 0)
            {
                return ValidationResult.Success;
            }

            foreach (var file in files)
            {
                if (file == null || !_allowedMimeTypes.Contains(file.ContentType))
                {
                    return new ValidationResult(ErrorMessage);
                }
            }

            return ValidationResult.Success;
        }
    }
}
