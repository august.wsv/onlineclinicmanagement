﻿using System.Text.Json;

namespace Backend.Providers
{
    public static class StatusCodeErrorProvider
    {
        public static async Task HandleUnauthorized(HttpContext context)
        {
            string message;
            switch (context.Response.StatusCode)
            {
                case 401:
                    message = "Missing or invalid Authorization Bearer token";
                    break;
                case 403:
                    message = "Deny access to resources";
                    break;
                case 404:
                    message = "Resource Not Found";
                    break;
                default:
                    message = "Unknown Error";
                    break;
            }

            context.Response.ContentType = "application/json";

            var jsonResponse = JsonSerializer.Serialize(new
            {
                message = message,
                data = null as object,
                meta = null as object,
                errors = null as object
            });

            await context.Response.WriteAsync(jsonResponse);
        }
    }
}
