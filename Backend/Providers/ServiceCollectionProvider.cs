﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Backend.Cron.Contracts;
using Backend.Cron;
using Backend.Policies;
using Backend.Repositorys.Contracts;
using Backend.Repositorys.Definitions;
using Backend.Services.Contracts;
using Backend.Services.Definitions;

namespace Backend.Providers
{
    public static class ServiceCollectionProvider
    {
        public static void AddServices(this IServiceCollection services)
        {
            // Register Services
            services
                .AddScoped<IAdminService, AdminService>()
                .AddScoped<IClientService, ClientService>()
                .AddScoped<IStaffMemberService, StaffMemberService>()
                .AddScoped<IClinicService, ClinicService>()
                .AddScoped<IMedicineService, MedicineService>()
                .AddScoped<IMedicineTypeService, MedicineTypeService>()
                .AddScoped<ISubjectService, SubjectService>()
                .AddScoped<IScientificApparatusService, ScientificApparatusService>()
                .AddScoped<ICartService, CartService>()
                .AddScoped<IPurchaseService, PurchaseService>()
                .AddScoped<ICommentService, CommentService>()
                .AddScoped<IFeedbackService, FeedbackService>()
                .AddScoped<IEducationalEventService, EducationalEventService>()
                .AddScoped<IClientMessageService, ClientMessageService>()
                .AddScoped<IAboutUsService, AboutUsService>()
                .AddScoped<IBusinessReportService, BusinessReportService>();
            // Register Repositories
            services
                .AddScoped<IAdminRepository, AdminRepository>()
                .AddScoped<IClientRepository, ClientRepository>()
                .AddScoped<IStaffMemberRepository, StaffMemberRepository>()
                .AddScoped<IClinicRepository, ClinicRepository>()
                .AddScoped<IMedicineRepository, MedicineRepository>()
                .AddScoped<IMedicineTypeRepository, MedicineTypeRepository>()
                .AddScoped<ISubjectRepository, SubjectRepository>()
                .AddScoped<IScientificApparatusRepository, ScientificApparatusRepository>()
                .AddScoped<ICartRepository, CartRepository>()
                .AddScoped<IPurchaseRepository, PurchaseRepository>()
                .AddScoped<IPurchaseDetailRepository, PurchaseDetailRepository>()
                .AddScoped<ICommentRepository, CommentRepository>()
                .AddScoped<IFeedbackRepository, FeedbackRepository>()
                .AddScoped<IEducationalEventRepository, EducationalEventRepository>()
                .AddScoped<IClientMessageRepository, ClientMessageRepository>()
                .AddScoped<IAboutUsRepository, AboutUsRepository>()
                .AddScoped<IBusinessReportRepository, BusinessReportRepository>();
            // Register Crons
            services
                .AddSingleton<IBusinessReportCron, BusinessReportCron>();
            // Register Policy
            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .Build();
                options.AddPolicy("JustSuperAdmin", AdminPolicy.JustSuperAdmin);
                options.AddPolicy("JustCollaborator", AdminPolicy.JustCollaborator);
                options.AddPolicy("AllAdmin", AdminPolicy.AllAdmin);
                options.AddPolicy("Client", ClientPolicy.Client);
                options.AddPolicy("StaffMember", StaffMemberPolicy.StaffMember);
                options.AddPolicy("CanUpdatePurchaseStatus", GeneralPolicy.CanUpdatePurchaseStatus);
                options.AddPolicy("CanCreateComment", GeneralPolicy.CanCreateComment);
                options.AddPolicy("CanUpdateStaffMember", GeneralPolicy.CanUpdateStaffMember);
            });
        }
    }
}
