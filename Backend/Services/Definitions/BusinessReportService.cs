﻿using Backend.Commons;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;

namespace Backend.Services.Definitions
{
    public class BusinessReportService : IBusinessReportService
    {
        private readonly IBusinessReportRepository _BusinessReportRepository;

        public BusinessReportService(
            IBusinessReportRepository BusinessReportRepository
        )
        {
            _BusinessReportRepository = BusinessReportRepository;
        }

        public async Task<object> GetThisMonthReport()
        {
            List<BusinessReport> businessReportList = await _BusinessReportRepository.GetThisMonthReport();
            return Helpers.Respond(data: businessReportList);
        }

        public async Task<object> GetThisWeekReport()
        {
            List<BusinessReport> businessReportList = await _BusinessReportRepository.GetThisWeekReport();
            return Helpers.Respond(data: businessReportList);
        }
    }
}
