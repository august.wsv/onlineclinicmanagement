﻿using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;
using X.PagedList;

namespace Backend.Services.Definitions
{
    public class ClientMessageService : IClientMessageService
    {
        private readonly IClientMessageRepository _ClientMessageRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ClientMessageService(
            IClientMessageRepository ClientMessageRepository,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _ClientMessageRepository = ClientMessageRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<object> GetList(ClientMessageGetListRequest clientMessageGetListRequest)
        {
            IPagedList<ClientMessage> clientMessagePagedList = await _ClientMessageRepository.GetList(clientMessageGetListRequest);

            object meta = Helpers.GetPaginationMetadata(clientMessagePagedList, clientMessageGetListRequest.Page, clientMessageGetListRequest.PageSize, _httpContextAccessor);

            return Helpers.Respond(data: clientMessagePagedList, meta: meta);
        }

        public async Task<object> GetOne(ClientMessageGetOneRequest clientMessageGetOneRequest)
        {
            ClientMessage clientMessage = await _ClientMessageRepository.GetOne(clientMessageGetOneRequest);

            if (clientMessage is not null)
            {
                return Helpers.Respond(data: clientMessage);
            }
            return Helpers.Respond(status: 404, message: "ClientMessage not found!");
        }

        public async Task<object> Create(ClientMessageCreateRequest clientMessageCreateRequest, int clientId)
        {
            ClientMessage clientMessageToCreate = new ClientMessage
            {
                ClientId = clientId,
                Title = clientMessageCreateRequest.Title,
                Content = clientMessageCreateRequest.Content,
            };
            bool createStatus = await _ClientMessageRepository.Create(clientMessageToCreate);

            return createStatus
                ? Helpers.Respond(message: "ClientMessage successfully created")
                : Helpers.Respond(status: 401, message: "ClientMessage creation failed");
        }
    }
}
