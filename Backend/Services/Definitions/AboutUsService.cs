﻿using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;

namespace Backend.Services.Definitions
{
    public class AboutUsService : IAboutUsService
    {
        private readonly IAboutUsRepository _AboutUsRepository;

        public AboutUsService(
            IAboutUsRepository AboutUsRepository
        )
        {
            _AboutUsRepository = AboutUsRepository;
        }

        public async Task<object> GetAll()
        {
            List<AboutUs> aboutUss = await _AboutUsRepository.GetAll();

            return Helpers.Respond(data: aboutUss);

        }

        public async Task<object> GetOne(AboutUsGetOneRequest aboutUsGetOneRequest)
        {
            AboutUs aboutUs = await _AboutUsRepository.GetOne(aboutUsGetOneRequest);

            if (aboutUs is not null)
            {
                return Helpers.Respond(data: aboutUs);
            }
            return Helpers.Respond(status: 404, message: "AboutUs not found!");
        }

        public async Task<object> Update(AboutUsGetOneRequest aboutUsGetOneRequest, AboutUsUpdateRequest aboutUsUpdateRequest)
        {
            AboutUs aboutUsToUpdate = await _AboutUsRepository.GetOne(aboutUsGetOneRequest);
            if (aboutUsToUpdate is not null)
            {
                if (aboutUsUpdateRequest.Description != null)
                { aboutUsToUpdate.Key = aboutUsUpdateRequest.Description; }
                bool updateStatus = await _AboutUsRepository.Update(aboutUsToUpdate);

                return updateStatus
                    ? Helpers.Respond(message: "AboutUs successfully updated")
                    : Helpers.Respond(status: 401, message: "AboutUs updation failed");
            }
            return Helpers.Respond(status: 404, message: "AboutUs not found!");
        }
    }
}
