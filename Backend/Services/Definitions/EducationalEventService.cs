﻿using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;
using X.PagedList;
using Backend.Repositorys;

namespace Backend.Services.Definitions
{
    public class EducationalEventService : IEducationalEventService
    {
        private readonly IEducationalEventRepository _EducationalEventRepository;
        private readonly ISubjectRepository _SubjectRepository;
        private readonly IStaffMemberRepository _StaffMemberRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IWebHostEnvironment _env;

        public EducationalEventService(
            IEducationalEventRepository EducationalEventRepository,
            ISubjectRepository SubjectRepository,
            IStaffMemberRepository StaffMemberRepository,
            IHttpContextAccessor httpContextAccessor,
            IWebHostEnvironment env
        )
        {
            _EducationalEventRepository = EducationalEventRepository;
            _SubjectRepository = SubjectRepository;
            _StaffMemberRepository = StaffMemberRepository;
            _httpContextAccessor = httpContextAccessor;
            _env = env;
        }

        public async Task<object> GetList(EducationalEventGetListRequest educationalEventGetListRequest)
        {
            IPagedList<EducationalEvent> educationalEventPagedList = await _EducationalEventRepository.GetList(educationalEventGetListRequest);

            HttpRequest request = _httpContextAccessor.HttpContext.Request;

            foreach (var educationalEvent in educationalEventPagedList)
            {
                educationalEvent.VideoPath = $"{request.Scheme}://{request.Host}{educationalEvent.VideoPath}";
            }

            object meta = Helpers.GetPaginationMetadata(educationalEventPagedList, educationalEventGetListRequest.Page, educationalEventGetListRequest.PageSize, _httpContextAccessor);

            return Helpers.Respond(data: educationalEventPagedList, meta: meta);
        }

        public async Task<object> GetListBySubject(EducationalEventGetListRequest educationalEventGetListRequest)
        {
            List<EducationalEvent> educationalEventList = await _EducationalEventRepository.GetListBySubject(educationalEventGetListRequest);

            HttpRequest request = _httpContextAccessor.HttpContext.Request;

            foreach (var educationalEvent in educationalEventList)
            {
                educationalEvent.VideoPath = $"{request.Scheme}://{request.Host}{educationalEvent.VideoPath}";
            }

            return Helpers.Respond(data: educationalEventList);
        }

        public async Task<object> GetOne(EducationalEventGetOneRequest educationalEventGetOneRequest)
        {
            EducationalEvent educationalEvent = await _EducationalEventRepository.GetOne(educationalEventGetOneRequest);

            if (educationalEvent is not null)
            {
                Subject subject = await _SubjectRepository.GetOne(new SubjectGetOneRequest { Id = educationalEvent.SubjectId });
                subject.EducationalEvents = null;
                StaffMember staffMember = await _StaffMemberRepository.GetOne(new StaffMemberGetOneRequest { Id = educationalEvent.StaffMemberId });

                HttpRequest request = _httpContextAccessor.HttpContext.Request;
                educationalEvent.VideoPath = $"{request.Scheme}://{request.Host}{educationalEvent.VideoPath}";

                var data = new
                {
                    EducationalEvent = educationalEvent,
                    Subject = subject,
                    StaffMember = staffMember
                };
                return Helpers.Respond(data: data);
            }
            return Helpers.Respond(status: 404, message: "EducationalEvent not found!");
        }

        public async Task<object> Create(EducationalEventCreateRequest educationalEventCreateRequest, int staffMembertId)
        {
            EducationalEvent educationalEventToCreate = new EducationalEvent
            {
                Name = educationalEventCreateRequest.Name,
                BriefDescription = educationalEventCreateRequest.BriefDescription,
                Description = educationalEventCreateRequest.Description,
                VideoPath = await Helpers.UploadFile(educationalEventCreateRequest.VideoPath, "EducationalEvent", _env),
                EducationalEventType = (int)educationalEventCreateRequest.EducationalEventType,
                SubjectId = (int)educationalEventCreateRequest.SubjectId,
                StaffMemberId = staffMembertId,
            };
            bool createStatus = await _EducationalEventRepository.Create(educationalEventToCreate);

            return createStatus
                ? Helpers.Respond(message: "EducationalEvent successfully created")
                : Helpers.Respond(status: 401, message: "EducationalEvent creation failed");
        }

        public async Task<object> Update(EducationalEventGetOneRequest educationalEventGetOneRequest, EducationalEventUpdateRequest educationalEventUpdateRequest)
        {
            EducationalEvent educationalEvent = await _EducationalEventRepository.GetOne(educationalEventGetOneRequest);
            if (educationalEvent is not null)
            {
                bool updateStatus = await _EducationalEventRepository.Update(educationalEventGetOneRequest, educationalEventUpdateRequest);

                return updateStatus
                    ? Helpers.Respond(message: "EducationalEvent successfully updated")
                    : Helpers.Respond(status: 401, message: "EducationalEvent updation failed");
            }
            return Helpers.Respond(status: 404, message: "EducationalEvent not found!");
        }

        public async Task<object> Delete(EducationalEventGetOneRequest educationalEventGetOneRequest)
        {
            EducationalEvent educationalEvent = await _EducationalEventRepository.GetOne(educationalEventGetOneRequest);
            if (educationalEvent is not null)
            {
                bool updateStatus = await _EducationalEventRepository.Delete(educationalEventGetOneRequest);

                return updateStatus
                    ? Helpers.Respond(message: "EducationalEvent successfully deleted")
                    : Helpers.Respond(status: 401, message: "EducationalEvent deletion failed");
            }
            return Helpers.Respond(status: 404, message: "EducationalEvent not found!");
        }
    }
}
