﻿using Microsoft.IdentityModel.Tokens;
using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Backend.Mail;
using X.PagedList;

namespace Backend.Services.Definitions
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository _ClientRepository;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ClientService
        (
            IClientRepository ClientRepository,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _ClientRepository = ClientRepository;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
        }

        private JwtSecurityToken GetJwtToken(List<Claim> authClaim)
        {
            SymmetricSecurityKey authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
            JwtSecurityToken token = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddDays(7),
                claims: authClaim,
                signingCredentials: new SigningCredentials(
                    authSigningKey,
                    SecurityAlgorithms.HmacSha256
                )
            );
            return token;
        }

        private JwtSecurityToken GetJwtTokenForEmail(List<Claim> authClaim)
        {
            SymmetricSecurityKey authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
            JwtSecurityToken token = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddMinutes(10),
                claims: authClaim,
                signingCredentials: new SigningCredentials(
                    authSigningKey,
                    SecurityAlgorithms.HmacSha256
                )
            );
            return token;
        }

        public async Task<object> Login(ClientLoginRequest clientLoginRequest)
        {
            ClientGetOneByEmailRequest clientGetOneByEmailRequest = new ClientGetOneByEmailRequest
            {
                Email = clientLoginRequest.Email
            };
            Client client = await _ClientRepository.GetOneByEmail(clientGetOneByEmailRequest);

            if (client is not null && client.Password.Length > 0 && BCrypt.Net.BCrypt.Verify(clientLoginRequest.Password, client.Password))
            {
                List<Claim> authClaims = new List<Claim>
                {
                    new Claim("PersonType", "Client"),
                    new Claim("ClientId", client.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };
                JwtSecurityToken token = GetJwtToken(authClaims);

                var meta = new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token)
                };

                return Helpers.Respond(data: client, meta: meta);
            }
            return Helpers.Respond(status: 401, message: "Username or Password is not correct");
        }

        public async Task<object> EmailVerification(ClientEmailVerificationRequest clientEmailVerificationRequest)
        {
            ClientGetOneByEmailRequest clientGetOneByEmailRequest = new ClientGetOneByEmailRequest
            {
                Email = clientEmailVerificationRequest.Email
            };
            Client client = await _ClientRepository.GetOneByEmail(clientGetOneByEmailRequest);

            if (client is null)
            {
                Client clientToCreate = new Client
                {
                    Name = clientEmailVerificationRequest.Name,
                    Phone = clientEmailVerificationRequest.Phone,
                    Email = clientEmailVerificationRequest.Email,
                    Password = BCrypt.Net.BCrypt.HashPassword(clientEmailVerificationRequest.Password),
                    ClinicId = (int)clientEmailVerificationRequest.ClinicId
                };
                string code = GenerateRandomCode(6);
                _ = Task.Run(() => ToClient.EmailConfirmationAsync(clientToCreate.Email, code));
                List<Claim> authClaims = new List<Claim>
                {
                    new Claim("Name", clientEmailVerificationRequest.Name),
                    new Claim("Phone", clientEmailVerificationRequest.Phone),
                    new Claim("Email", clientEmailVerificationRequest.Email),
                    new Claim("HashPassword", BCrypt.Net.BCrypt.HashPassword(clientEmailVerificationRequest.Password)),
                    new Claim("ClinicId", clientEmailVerificationRequest.ClinicId.ToString()),
                    new Claim("HashCode", BCrypt.Net.BCrypt.HashPassword(code)),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };
                JwtSecurityToken token = GetJwtTokenForEmail(authClaims);

                var meta = new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token)
                };

                return Helpers.Respond(message: "One email send to you", meta: meta);
            }
            return Helpers.Respond(status: 401, message: "Email already exists, cannot be created!");
        }

        public async Task<object> Register(ClientRegisterRequest clientRegisterRequest)
        {
            var hca = _httpContextAccessor.HttpContext.User;
            if (BCrypt.Net.BCrypt.Verify(clientRegisterRequest.Code, _httpContextAccessor.HttpContext.User?.FindFirst("HashCode").Value))
            {
                Client clientToCreate = new Client
                {
                    Name = hca.FindFirst("Name").Value,
                    Phone = hca?.FindFirst("Phone").Value,
                    Email = hca?.FindFirst("Email").Value,
                    Password = hca?.FindFirst("HashPassword").Value,
                    ClinicId = int.Parse(hca?.FindFirst("ClinicId").Value),
                };
                bool CreateStatus = await _ClientRepository.Create(clientToCreate);
                return CreateStatus
                    ? Helpers.Respond(message: "Account successfully created")
                    : Helpers.Respond(status: 401, message: "Account creation failed");

            }
            return Helpers.Respond(status: 401, message: "Code is not right!");
        }

        private static string GenerateRandomCode(int length)
        {
            const string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            Random random = new Random();
            StringBuilder code = new StringBuilder();

            for (int i = 0; i < length; i++)
            {
                int index = random.Next(characters.Length);
                code.Append(characters[index]);
            }

            return code.ToString();
        }

        public async Task<object> AboutClient(ClientGetOneRequest clientGetOneRequest)
        {
            Client client = await _ClientRepository.GetOne(clientGetOneRequest);

            if (client is not null)
            {
                return Helpers.Respond(data: client);
            }
            return Helpers.Respond(status: 404, message: "Client not found!");
        }

        public async Task<object> Update(ClientGetOneRequest clientGetOneRequest, ClientUpdateRequest clientUpdateRequest)
        {
            Client client = await _ClientRepository.GetOne(clientGetOneRequest);
            if (client is not null)
            {
                bool updateStatus = await _ClientRepository.Update(clientGetOneRequest, clientUpdateRequest);

                return updateStatus
                    ? Helpers.Respond(message: "Client successfully updated")
                    : Helpers.Respond(status: 401, message: "Client updation failed");
            }
            return Helpers.Respond(status: 404, message: "Client not found!");
        }

        public async Task<object> GetList(ClientGetListRequest clientGetListRequest)
        {
            IPagedList<Client> clientPagedList = await _ClientRepository.GetList(clientGetListRequest);

            object meta = Helpers.GetPaginationMetadata(clientPagedList, clientGetListRequest.Page, clientGetListRequest.PageSize, _httpContextAccessor);

            return Helpers.Respond(data: clientPagedList, meta: meta);
        }

        public async Task<object> Delete(ClientGetOneRequest clientGetOneRequest)
        {
            Client client = await _ClientRepository.GetOne(clientGetOneRequest);
            if (client is not null)
            {
                bool updateStatus = await _ClientRepository.Delete(clientGetOneRequest);

                return updateStatus
                    ? Helpers.Respond(message: "Client successfully deleted")
                    : Helpers.Respond(status: 401, message: "Client deletion failed");
            }
            return Helpers.Respond(status: 404, message: "Client not found!");
        }
    }
}
