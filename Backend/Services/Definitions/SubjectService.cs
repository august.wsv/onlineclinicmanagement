﻿using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;
using X.PagedList;

namespace Backend.Services.Definitions
{
    public class SubjectService : ISubjectService
    {
        private readonly ISubjectRepository _SubjectRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IWebHostEnvironment _env;

        public SubjectService(
            ISubjectRepository SubjectRepository,
            IHttpContextAccessor httpContextAccessor,
            IWebHostEnvironment env
        )
        {
            _SubjectRepository = SubjectRepository;
            _httpContextAccessor = httpContextAccessor;
            _env = env;
        }

        public async Task<object> GetList(SubjectGetListRequest subjectGetListRequest)
        {
            IPagedList<Subject> subjectPagedList = await _SubjectRepository.GetList(subjectGetListRequest);

            HttpRequest request = _httpContextAccessor.HttpContext.Request;

            foreach (var subject in subjectPagedList)
            {
                subject.ImagePath = $"{request.Scheme}://{request.Host}{subject.ImagePath}";
            }

            object meta = Helpers.GetPaginationMetadata(subjectPagedList, subjectGetListRequest.Page, subjectGetListRequest.PageSize, _httpContextAccessor);

            return Helpers.Respond(data: subjectPagedList, meta: meta);
        }

        public async Task<object> GetOne(SubjectGetOneRequest subjectGetOneRequest)
        {
            Subject subject = await _SubjectRepository.GetOne(subjectGetOneRequest);

            if (subject is not null)
            {
                HttpRequest request = _httpContextAccessor.HttpContext.Request;
                subject.ImagePath = $"{request.Scheme}://{request.Host}{subject.ImagePath}";
                subject.EducationalEvents = subject.EducationalEvents
                    .Where(e => e.DeletedAt == null)
                    .ToList();
                return Helpers.Respond(data: subject);
            }
            return Helpers.Respond(status: 404, message: "Subject not found!");
        }

        public async Task<object> Create(SubjectCreateRequest subjectCreateRequest)
        {
            Subject subjectToCreate = new Subject
            {
                Name = subjectCreateRequest.Name,
                ImagePath = await Helpers.UploadFile(subjectCreateRequest.ImagePath, "Subject", _env),
                BriefDescription = subjectCreateRequest.BriefDescription
            };
            bool createStatus = await _SubjectRepository.Create(subjectToCreate);

            return createStatus
                ? Helpers.Respond(message: "Subject successfully created")
                : Helpers.Respond(status: 401, message: "Subject creation failed");
        }

        public async Task<object> Update(SubjectGetOneRequest subjectGetOneRequest, SubjectUpdateRequest subjectUpdateRequest)
        {
            Subject subject = await _SubjectRepository.GetOne(subjectGetOneRequest);
            if (subject is not null)
            {
                bool updateStatus = await _SubjectRepository.Update(subjectGetOneRequest, subjectUpdateRequest);

                return updateStatus
                    ? Helpers.Respond(message: "Subject successfully updated")
                    : Helpers.Respond(status: 401, message: "Subject updation failed");
            }
            return Helpers.Respond(status: 404, message: "Subject not found!");
        }

        public async Task<object> Delete(SubjectGetOneRequest subjectGetOneRequest)
        {
            Subject subject = await _SubjectRepository.GetOne(subjectGetOneRequest);
            if (subject is not null)
            {
                bool updateStatus = await _SubjectRepository.Delete(subjectGetOneRequest);

                return updateStatus
                    ? Helpers.Respond(message: "Subject successfully deleted")
                    : Helpers.Respond(status: 401, message: "Subject deletion failed");
            }
            return Helpers.Respond(status: 404, message: "Subject not found!");
        }
    }
}
