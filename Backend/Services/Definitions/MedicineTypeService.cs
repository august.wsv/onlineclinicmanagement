﻿using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;
using X.PagedList;

namespace Backend.Services.Definitions
{
    public class MedicineTypeService : IMedicineTypeService
    {
        private readonly IMedicineTypeRepository _MedicineTypeRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public MedicineTypeService(
            IMedicineTypeRepository MedicineTypeRepository,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _MedicineTypeRepository = MedicineTypeRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<object> GetList(MedicineTypeGetListRequest medicineTypeGetListRequest)
        {
            IPagedList<MedicineType> medicineTypePagedList = await _MedicineTypeRepository.GetList(medicineTypeGetListRequest);

            object meta = Helpers.GetPaginationMetadata(medicineTypePagedList, medicineTypeGetListRequest.Page, medicineTypeGetListRequest.PageSize, _httpContextAccessor);

            return Helpers.Respond(data: medicineTypePagedList, meta: meta);
        }

        public async Task<object> GetAll()
        {
            List<MedicineType> medicineTypes = await _MedicineTypeRepository.GetAll();

            return Helpers.Respond(data: medicineTypes);

        }

        public async Task<object> GetOne(MedicineTypeGetOneRequest medicineTypeGetOneRequest)
        {
            MedicineType medicineType = await _MedicineTypeRepository.GetOne(medicineTypeGetOneRequest);

            if (medicineType is not null)
            {
                return Helpers.Respond(data: medicineType);
            }
            return Helpers.Respond(status: 404, message: "MedicineType not found!");
        }

        public async Task<object> Create(MedicineTypeCreateRequest medicineTypeCreateRequest)
        {
            MedicineType medicineTypeToCreate = new MedicineType
            {
                Name = medicineTypeCreateRequest.Name
            };
            bool createStatus = await _MedicineTypeRepository.Create(medicineTypeToCreate);

            return createStatus
                ? Helpers.Respond(message: "MedicineType successfully created")
                : Helpers.Respond(status: 401, message: "MedicineType creation failed");
        }

        public async Task<object> Update(MedicineTypeGetOneRequest medicineTypeGetOneRequest, MedicineTypeUpdateRequest medicineTypeUpdateRequest)
        {
            MedicineType medicineType = await _MedicineTypeRepository.GetOne(medicineTypeGetOneRequest);
            if (medicineType is not null)
            {
                bool updateStatus = await _MedicineTypeRepository.Update(medicineTypeGetOneRequest, medicineTypeUpdateRequest);

                return updateStatus
                    ? Helpers.Respond(message: "MedicineType successfully updated")
                    : Helpers.Respond(status: 401, message: "MedicineType updation failed");
            }
            return Helpers.Respond(status: 404, message: "MedicineType not found!");
        }

        public async Task<object> Delete(MedicineTypeGetOneRequest medicineTypeGetOneRequest)
        {
            MedicineType medicineType = await _MedicineTypeRepository.GetOne(medicineTypeGetOneRequest);
            if (medicineType is not null)
            {
                bool updateStatus = await _MedicineTypeRepository.Delete(medicineTypeGetOneRequest);

                return updateStatus
                    ? Helpers.Respond(message: "MedicineType successfully deleted")
                    : Helpers.Respond(status: 401, message: "MedicineType deletion failed");
            }
            return Helpers.Respond(status: 404, message: "MedicineType not found!");
        }


    }
}
