﻿using Microsoft.IdentityModel.Tokens;
using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using X.PagedList;

namespace Backend.Services.Definitions
{
    public class StaffMemberService : IStaffMemberService
    {
        private readonly IStaffMemberRepository _StaffMemberRepository;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public StaffMemberService
        (
            IStaffMemberRepository StaffMemberRepository,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _StaffMemberRepository = StaffMemberRepository;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
        }

        private JwtSecurityToken GetJwtToken(List<Claim> authClaim)
        {
            SymmetricSecurityKey authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
            JwtSecurityToken token = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddDays(7),
                claims: authClaim,
                signingCredentials: new SigningCredentials(
                    authSigningKey,
                    SecurityAlgorithms.HmacSha256
                )
            );
            return token;
        }

        public async Task<object> Login(StaffMemberLoginRequest staffMemberLoginRequest)
        {
            StaffMemberGetOneByEmailRequest staffMemberGetOneByEmailRequest = new StaffMemberGetOneByEmailRequest
            {
                Email = staffMemberLoginRequest.Email
            };
            StaffMember staffMember = await _StaffMemberRepository.GetOneByEmail(staffMemberGetOneByEmailRequest);

            if (staffMember is not null && staffMember.Password.Length > 0 && BCrypt.Net.BCrypt.Verify(staffMemberLoginRequest.Password, staffMember.Password))
            {
                List<Claim> authClaims = new List<Claim>
                {
                    new Claim("PersonType", "StaffMember"),
                    new Claim("StaffMemberId", staffMember.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };
                JwtSecurityToken token = GetJwtToken(authClaims);

                var meta = new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token)
                };

                return Helpers.Respond(data: staffMember, meta: meta);
            }
            return Helpers.Respond(status: 401, message: "Username or Password is not correct");
        }

        public async Task<object> Register(StaffMemberRegisterRequest staffMemberRegisterRequest)
        {
            StaffMemberGetOneByEmailRequest staffMemberGetOneByEmailRequest = new StaffMemberGetOneByEmailRequest
            {
                Email = staffMemberRegisterRequest.Email
            };
            StaffMember staffMember = await _StaffMemberRepository.GetOneByEmail(staffMemberGetOneByEmailRequest);

            if (staffMember is null)
            {
                StaffMember staffMemberToCreate = new StaffMember
                {
                    Name = staffMemberRegisterRequest.Name,
                    Phone = staffMemberRegisterRequest.Phone,
                    Email = staffMemberRegisterRequest.Email,
                    Password = BCrypt.Net.BCrypt.HashPassword(staffMemberRegisterRequest.Password)
                };
                bool CreateStatus = await _StaffMemberRepository.Create(staffMemberToCreate);

                return CreateStatus
                    ? Helpers.Respond(message: "Account successfully created")
                    : Helpers.Respond(status: 401, message: "Account creation failed");
            }
            return Helpers.Respond(status: 401, message: "Email already exists, cannot be created!");
        }

        public async Task<object> GetList(StaffMemberGetListRequest staffMemberGetListRequest)
        {
            IPagedList<StaffMember> staffMemberPagedList = await _StaffMemberRepository.GetList(staffMemberGetListRequest);

            object meta = Helpers.GetPaginationMetadata(staffMemberPagedList, staffMemberGetListRequest.Page, staffMemberGetListRequest.PageSize, _httpContextAccessor);

            return Helpers.Respond(data: staffMemberPagedList, meta: meta);
        }

        public async Task<object> Update(StaffMemberGetOneRequest staffMemberGetOneRequest, StaffMemberUpdateRequest staffMemberUpdateRequest)
        {
            StaffMember staffMember = await _StaffMemberRepository.GetOne(staffMemberGetOneRequest);
            if (staffMember is not null)
            {
                bool updateStatus = await _StaffMemberRepository.Update(staffMemberGetOneRequest, staffMemberUpdateRequest);

                return updateStatus
                    ? Helpers.Respond(message: "StaffMember successfully updated")
                    : Helpers.Respond(status: 401, message: "StaffMember updation failed");
            }
            return Helpers.Respond(status: 404, message: "StaffMember not found!");
        }

        public async Task<object> Delete(StaffMemberGetOneRequest staffMemberGetOneRequest)
        {
            StaffMember staffMember = await _StaffMemberRepository.GetOne(staffMemberGetOneRequest);
            if (staffMember is not null)
            {
                bool updateStatus = await _StaffMemberRepository.Delete(staffMemberGetOneRequest);

                return updateStatus
                    ? Helpers.Respond(message: "StaffMember successfully deleted")
                    : Helpers.Respond(status: 401, message: "StaffMember deletion failed");
            }
            return Helpers.Respond(status: 404, message: "StaffMember not found!");
        }

        public async Task<object> AboutStaffMember(StaffMemberGetOneRequest staffMemberGetOneRequest)
        {
            StaffMember staffMember = await _StaffMemberRepository.GetOne(staffMemberGetOneRequest);

            if (staffMember is not null)
            {
                return Helpers.Respond(data: staffMember);
            }
            return Helpers.Respond(status: 404, message: "Client not found!");
        }
    }
}
