﻿using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;

namespace Backend.Services.Definitions
{
    public class CartService : ICartService
    {
        private readonly ICartRepository _CartRepository;
        private readonly IScientificApparatusRepository _ScientificApparatusRepository;
        private readonly IMedicineRepository _MedicineRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CartService(
            ICartRepository CartRepository,
            IScientificApparatusRepository ScientificApparatusRepository,
            IMedicineRepository MedicineRepository,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _CartRepository = CartRepository;
            _MedicineRepository = MedicineRepository;
            _ScientificApparatusRepository = ScientificApparatusRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<object> GetListByClient(CartGetListRequest cartGetListRequest)
        {
            List<Cart> cartList = await _CartRepository.GetListByClient(cartGetListRequest);

            HttpRequest request = _httpContextAccessor.HttpContext.Request;

            foreach (var cart in cartList)
            {
                switch (cart.ItemType)
                {
                    case Constants.ItemTypeIsMedicine:
                        var medicine = await _MedicineRepository
                            .GetOne(new MedicineGetOneRequest { Id = cart.ItemId });
                        cart.ItemName = medicine != null ? medicine.Name : "Unknown Medicine";
                        cart.ItemTypeName = "Medicine";
                        cart.ImagePath = $"{request.Scheme}://{request.Host}{medicine.ImagePath}";
                        cart.StoreQuantity = medicine.Quantity;
                        break;
                    case Constants.ItemTypeIsScientificApparatus:
                        var scientificApparatus = await _ScientificApparatusRepository
                            .GetOne(new ScientificApparatusGetOneRequest { Id = cart.ItemId });
                        cart.ItemName = scientificApparatus != null ? scientificApparatus.Name : "Unknown ScientificApparatus";
                        cart.ItemTypeName = "Scientific Apparatus";
                        cart.ImagePath = $"{request.Scheme}://{request.Host}{scientificApparatus.ImagePath}";
                        cart.StoreQuantity = scientificApparatus.Quantity;
                        break;
                    default:
                        break;
                }
            }




            string totalString = await _CartRepository.GetTotalValue(cartGetListRequest);
            var data = new
            {
                TotalString = totalString,
                CartDetail = cartList
            };

            return Helpers.Respond(data: data);
        }

        public async Task<object> Create(CartCreateRequest cartCreateRequest, int clientId)
        {
            CartCheckItemRequest cartCheckItemRequest = new CartCheckItemRequest
            {
                ClientId = clientId,
                ItemId = (int)cartCreateRequest.ItemId,
                ItemType = (int)cartCreateRequest.ItemType
            };
            Cart cart = await _CartRepository.CheckItem(cartCheckItemRequest);
            if (cart is null)
            {
                decimal itemPrice = cartCreateRequest.ItemType == Constants.ItemTypeIsMedicine
                    ? (await _MedicineRepository.GetOne(new MedicineGetOneRequest { Id = (int)cartCreateRequest.ItemId })).Price
                    : (await _ScientificApparatusRepository.GetOne(new ScientificApparatusGetOneRequest { Id = (int)cartCreateRequest.ItemId })).Price;
                Cart cartToCreate = new Cart
                {
                    ClientId = clientId,
                    Price = itemPrice,
                    ItemId = (int)cartCreateRequest.ItemId,
                    ItemType = (int)cartCreateRequest.ItemType
                };
                bool createStatus = await _CartRepository.Create(cartToCreate);

                return createStatus
                    ? Helpers.Respond(message: "Cart successfully created")
                    : Helpers.Respond(status: 401, message: "Cart creation failed");
            }
            else
            {
                bool updateStatus = await _CartRepository.UpdateInc(cart);

                return updateStatus
                    ? Helpers.Respond(message: "Cart successfully updated")
                    : Helpers.Respond(status: 401, message: "Cart updation failed");

            }

        }

        public async Task<object> Update(CartGetOneRequest cartGetOneRequest, CartUpdateRequest cartUpdateRequest, int clientId)
        {
            Cart cart = await _CartRepository.GetOne(cartGetOneRequest);

            if (cart is not null)
            {
                if (clientId != cart.ClientId)
                {
                    return Helpers.Respond(status: 403, message: "Deny access to resource, you do not own the record");
                }
                bool updateStatus = await _CartRepository.Update(cartGetOneRequest, cartUpdateRequest);

                return updateStatus
                    ? Helpers.Respond(message: "Cart successfully updated")
                    : Helpers.Respond(status: 401, message: "Cart updation failed");
            }
            return Helpers.Respond(status: 404, message: "Cart not found!");
        }

        public async Task<object> Delete(CartGetOneRequest cartGetOneRequest)
        {
            Cart cart = await _CartRepository.GetOne(cartGetOneRequest);
            if (cart is not null)
            {
                bool updateStatus = await _CartRepository.Delete(cartGetOneRequest);

                return updateStatus
                    ? Helpers.Respond(message: "Cart successfully deleted")
                    : Helpers.Respond(status: 401, message: "Cart deletion failed");
            }
            return Helpers.Respond(status: 404, message: "Cart not found!");
        }

        public async Task<dynamic> DeleteAllOfClient(CartGetListRequest cartGetListRequest)
        {
            bool deleteAllStatus = await _CartRepository.DeleteAllOfClient(cartGetListRequest);

            return deleteAllStatus
                ? Helpers.Respond(message: "All cart successfully deleted")
                : Helpers.Respond(status: 401, message: "All cart deletion failed");
        }
    }
}
