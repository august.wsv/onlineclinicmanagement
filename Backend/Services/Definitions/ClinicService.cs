﻿using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;
using X.PagedList;

namespace Backend.Services.Definitions
{
    public class ClinicService : IClinicService
    {
        private readonly IClinicRepository _ClinicRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ClinicService(
            IClinicRepository ClinicRepository,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _ClinicRepository = ClinicRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<object> GetList(ClinicGetListRequest clinicGetListRequest)
        {
            IPagedList<Clinic> clinicPagedList = await _ClinicRepository.GetList(clinicGetListRequest);

            object meta = Helpers.GetPaginationMetadata(clinicPagedList, clinicGetListRequest.Page, clinicGetListRequest.PageSize, _httpContextAccessor);

            return Helpers.Respond(data: clinicPagedList, meta: meta);
        }

        public async Task<object> GetAll()
        {
            List<Clinic> clinics = await _ClinicRepository.GetAll();

            return Helpers.Respond(data: clinics);

        }

        public async Task<object> GetOne(ClinicGetOneRequest clinicGetOneRequest)
        {
            Clinic clinic = await _ClinicRepository.GetOne(clinicGetOneRequest);

            if (clinic is not null)
            {
                return Helpers.Respond(data: clinic);
            }
            return Helpers.Respond(status: 404, message: "Clinic not found!");
        }

        public async Task<object> Create(ClinicCreateRequest clinicCreateRequest)
        {
            Clinic clinicToCreate = new Clinic
            {
                Name = clinicCreateRequest.Name,
                Phone = clinicCreateRequest.Phone,
                Email = clinicCreateRequest.Email,
                Address = clinicCreateRequest.Address
            };
            bool createStatus = await _ClinicRepository.Create(clinicToCreate);

            return createStatus ?
                Helpers.Respond(message: "Clinic successfully created") :
                Helpers.Respond(status: 401, message: "Clinic creation failed");
        }

        public async Task<object> Update(ClinicGetOneRequest clinicGetOneRequest, ClinicUpdateRequest clinicUpdateRequest)
        {
            Clinic clinic = await _ClinicRepository.GetOne(clinicGetOneRequest);
            if (clinic is not null)
            {
                bool updateStatus = await _ClinicRepository.Update(clinicGetOneRequest, clinicUpdateRequest);

                return updateStatus
                    ? Helpers.Respond(message: "Clinic successfully updated")
                    : Helpers.Respond(status: 401, message: "Clinic updation failed");
            }
            return Helpers.Respond(status: 404, message: "Clinic not found!");
        }

        public async Task<object> Delete(ClinicGetOneRequest clinicGetOneRequest)
        {
            Clinic clinic = await _ClinicRepository.GetOne(clinicGetOneRequest);
            if (clinic is not null)
            {
                bool updateStatus = await _ClinicRepository.Delete(clinicGetOneRequest);

                return updateStatus
                    ? Helpers.Respond(message: "Clinic successfully deleted")
                    : Helpers.Respond(status: 401, message: "Clinic deletion failed");
            }
            return Helpers.Respond(status: 404, message: "Clinic not found!");
        }
    }
}
