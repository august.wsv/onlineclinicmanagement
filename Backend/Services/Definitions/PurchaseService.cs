﻿using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;
using X.PagedList;
using Backend.Data;
using Backend.Mail;

namespace Backend.Services.Definitions
{
    public class PurchaseService : IPurchaseService
    {
        private readonly IPurchaseRepository _PurchaseRepository;
        private readonly ICartRepository _CartRepository;
        private readonly IPurchaseDetailRepository _PurchaseDetailRepository;
        private readonly IMedicineRepository _MedicineRepository;
        private readonly IScientificApparatusRepository _ScientificApparatusRepository;
        private readonly DataContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public PurchaseService(
            IPurchaseRepository PurchaseRepository,
            ICartRepository CartRepository,
            IPurchaseDetailRepository ProductRepository,
            IMedicineRepository MedicineRepository,
            IScientificApparatusRepository ScientificApparatusRepository,
            DataContext context,
            IHttpContextAccessor httpContextAccessor

        )
        {
            _PurchaseRepository = PurchaseRepository;
            _CartRepository = CartRepository;
            _PurchaseDetailRepository = ProductRepository;
            _MedicineRepository = MedicineRepository;
            _ScientificApparatusRepository = ScientificApparatusRepository;
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<object> GetPagedList(PurchaseGetPagedListRequest purchaseGetPagedListRequest, IHttpContextAccessor httpContextAccessor)
        {
            IPagedList<Purchase> purchasePagedList = await _PurchaseRepository.GetPagedList(purchaseGetPagedListRequest);

            object meta = Helpers.GetPaginationMetadata(purchasePagedList, purchaseGetPagedListRequest.Page, purchaseGetPagedListRequest.PageSize, httpContextAccessor);

            return Helpers.Respond(data: purchasePagedList, meta: meta);
        }

        public async Task<object> GetListByClient(PurchaseGetListRequest purchaseGetListRequest)
        {
            List<Purchase> purchaseList = await _PurchaseRepository.GetListByClient(purchaseGetListRequest);

            return Helpers.Respond(data: purchaseList);
        }

        public async Task<object> GetOne(PurchaseGetOneRequest purchaseGetOneRequest)
        {
            Purchase purchase = await _PurchaseRepository.GetOne(purchaseGetOneRequest);

            if (purchase is not null)
            {
                var purchaseDetails = purchase.PurchaseDetails;
                purchase.TotalString = string.Format("{0:C}", (decimal)purchaseDetails.Sum(purchaseDetails => purchaseDetails.Price * purchaseDetails.Quantity));

                HttpRequest request = _httpContextAccessor.HttpContext.Request;

                foreach (var detail in purchaseDetails)
                {
                    if (detail.ItemType == Constants.ItemTypeIsMedicine)
                    {
                        var medicine = await _MedicineRepository
                            .GetOne(new MedicineGetOneRequest { Id = detail.ItemId });
                        detail.ItemName = medicine != null ? medicine.Name : "Unknown Medicine";
                        detail.ItemTypeName = "Medicine";
                        detail.ImagePath = $"{request.Scheme}://{request.Host}{medicine.ImagePath}";
                    }
                    else if (detail.ItemType == Constants.ItemTypeIsScientificApparatus)
                    {
                        var scientificApparatus = await _ScientificApparatusRepository
                            .GetOne(new ScientificApparatusGetOneRequest { Id = detail.ItemId });
                        detail.ItemName = scientificApparatus != null ? scientificApparatus.Name : "Unknown ScientificApparatus";
                        detail.ItemTypeName = "Scientific Apparatus";
                        detail.ImagePath = $"{request.Scheme}://{request.Host}{scientificApparatus.ImagePath}";
                    }
                }

                string personTypeClaim = _httpContextAccessor.HttpContext.User?.FindFirst("PersonType").Value;
                if (personTypeClaim == "Client")
                {
                    string clientIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("ClientId").Value;
                    int clientId = int.Parse(clientIdClaim);
                    if (clientId != purchase.ClientId)
                    {
                        return Helpers.Respond(status: 403, message: "Deny access to resource, you do not own the record");
                    }
                }
                return Helpers.Respond(data: purchase);
            }
            return Helpers.Respond(status: 404, message: "Purchase not found!");
        }

        public async Task<object> Create(PurchaseCreateRequest purchaseCreateRequest, int clientId)
        {
            using var transaction = _context.Database.BeginTransaction();
            try
            {
                CartGetListRequest cartGetListRequest = new CartGetListRequest
                {
                    ClientId = clientId
                };
                List<Cart> cartList = await _CartRepository.GetListByClient(cartGetListRequest);

                if (cartList.Count == 0)
                {
                    return Helpers.Respond(status: 401, message: "Purchase creation failed, cart null");
                }
                Purchase purchaseToCreate = new Purchase
                {
                    ClientId = clientId,
                    Address = purchaseCreateRequest.Address
                };


                Purchase purchase = await _PurchaseRepository.Create(purchaseToCreate);
                foreach (var cart in cartList)
                {
                    await _PurchaseDetailRepository.Create(
                        new PurchaseDetail
                        {
                            PurchaseId = purchase.Id,
                            ItemId = cart.ItemId,
                            ItemType = cart.ItemType,
                            Quantity = cart.Quantity,
                            Price = cart.Price
                        }
                    );
                    await _CartRepository.Delete(
                        new CartGetOneRequest
                        {
                            Id = cart.Id
                        }
                    );
                }
                Purchase purchaseGetOne = await _PurchaseRepository.GetOne(new PurchaseGetOneRequest
                {
                    Id = purchase.Id
                });

                transaction.Commit();
                return Helpers.Respond(message: "Purchase successfully created", data: new { purchase.Id });
            }
            catch
            {
                transaction.Rollback();
                return Helpers.Respond(status: 401, message: "Purchase creation failed");
            }
        }

        public async Task<object> Update(PurchaseGetOneRequest purchaseGetOneRequest, PurchaseUpdateRequest purchaseUpdateRequest)
        {
            Purchase purchase = await _PurchaseRepository.GetOne(purchaseGetOneRequest);
            bool isAdmin = true;
            string personTypeClaim = _httpContextAccessor.HttpContext.User?.FindFirst("PersonType").Value;
            if (personTypeClaim == "Client")
            {
                isAdmin = false;
                string clientIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("ClientId").Value;
                int clientId = int.Parse(clientIdClaim);
                if (clientId != purchase.ClientId)
                {
                    return Helpers.Respond(status: 403, message: "Deny access to resource, you do not own the record");
                }
            }

            if (purchase is not null)
            {
                if (isAdmin)
                {
                    if
                    (
                        purchase.Status == Constants.Purchase["PENDING"] &&
                        (
                            purchaseUpdateRequest.Status == Constants.Purchase["CONFIRMEDANDSHIPPING"] ||
                            purchaseUpdateRequest.Status == Constants.Purchase["ORDERCANCELEDBYADMIN"]
                        )
                    )
                    {
                        if (purchaseUpdateRequest.Status == Constants.Purchase["CONFIRMEDANDSHIPPING"])
                        {
                            bool isTrue = true;
                            foreach (var purchaseDetails in purchase.PurchaseDetails)
                            {
                                if (purchaseDetails.ItemType == Constants.ItemTypeIsMedicine)
                                {
                                    Medicine medicine = await _MedicineRepository.GetOne(new MedicineGetOneRequest { Id = purchaseDetails.ItemId });
                                    if (medicine.Quantity < purchaseDetails.Quantity)
                                    {
                                        isTrue = false;
                                        break;
                                    }
                                }
                                else if (purchaseDetails.ItemType == Constants.ItemTypeIsScientificApparatus)
                                {
                                    ScientificApparatus scientificApparatus = await _ScientificApparatusRepository.GetOne(new ScientificApparatusGetOneRequest { Id = purchaseDetails.ItemId });
                                    if (scientificApparatus.Quantity < purchaseDetails.Quantity)
                                    {
                                        break;
                                    }
                                }
                            }

                            if (isTrue)
                            {
                                foreach (var purchaseDetails in purchase.PurchaseDetails)
                                {
                                    if (purchaseDetails.ItemType == Constants.ItemTypeIsMedicine)
                                    {
                                        await _PurchaseRepository.UpdateMedicineQuantity((int)purchaseDetails.ItemId, (int)purchaseDetails.Quantity);
                                    }
                                    else if (purchaseDetails.ItemType == Constants.ItemTypeIsScientificApparatus)
                                    {
                                        await _PurchaseRepository.UpdateScientificApparatusQuantity((int)purchaseDetails.ItemId, (int)purchaseDetails.Quantity);
                                    }
                                }
                                _ = Task.Run(() => ToClient.OrderDetailAsync(purchase.AboutClient.Email, purchase));
                            }
                            else
                            {
                                return Helpers.Respond(message: "Purchase enought not true");
                            }
                        }
                        bool updateStatus = await _PurchaseRepository.Update(purchaseGetOneRequest, purchaseUpdateRequest);

                        return updateStatus
                            ? Helpers.Respond(message: "Purchase successfully updated")
                            : Helpers.Respond(status: 401, message: "Purchase updation failed");
                    }

                    else if
                    (
                        purchase.Status == Constants.Purchase["CONFIRMEDANDSHIPPING"] &&
                        (
                            purchaseUpdateRequest.Status == Constants.Purchase["ROLLBACKBYADMIN"] ||
                            purchaseUpdateRequest.Status == Constants.Purchase["DELIVERED"]
                        )
                    )
                    {
                        if (purchaseUpdateRequest.Status == Constants.Purchase["ROLLBACKBYADMIN"])
                        {
                            foreach (var purchaseDetails in purchase.PurchaseDetails)
                            {
                                if (purchaseDetails.ItemType == Constants.ItemTypeIsMedicine)
                                {
                                    await _PurchaseRepository.UpdateMedicineQuantityRollback((int)purchaseDetails.ItemId, (int)purchaseDetails.Quantity);
                                }
                                else if (purchaseDetails.ItemType == Constants.ItemTypeIsScientificApparatus)
                                {
                                    await _PurchaseRepository.UpdateScientificApparatusQuantityRollback((int)purchaseDetails.ItemId, (int)purchaseDetails.Quantity);
                                }
                            }
                        }

                        bool updateStatus = await _PurchaseRepository.Update(purchaseGetOneRequest, purchaseUpdateRequest);

                        return updateStatus
                            ? Helpers.Respond(message: "Purchase successfully updated")
                            : Helpers.Respond(status: 401, message: "Purchase updation failed");
                    }

                    else if
                    (
                        purchase.Status == Constants.Purchase["CONFIRMEDANDSHIPPING"] &&
                        purchaseUpdateRequest.Status == Constants.Purchase["DELIVERED"]
                    )
                    {
                        bool updateStatus = await _PurchaseRepository.Update(purchaseGetOneRequest, purchaseUpdateRequest);

                        return updateStatus
                            ? Helpers.Respond(message: "Purchase successfully updated")
                            : Helpers.Respond(status: 401, message: "Purchase updation failed");
                    }
                }
                else if (!isAdmin)
                {
                    if (purchase.Status == Constants.Purchase["PENDING"] && purchaseUpdateRequest.Status == Constants.Purchase["ORDERCANCELEDBYUSER"])
                    {
                        bool updateStatus = await _PurchaseRepository.Update(purchaseGetOneRequest, purchaseUpdateRequest);

                        return updateStatus
                            ? Helpers.Respond(message: "Purchase successfully updated")
                            : Helpers.Respond(status: 401, message: "Purchase updation failed");
                    }
                    else if (purchase.Status == Constants.Purchase["CONFIRMEDANDSHIPPING"] && purchaseUpdateRequest.Status == Constants.Purchase["ROLLBACKBYUSER"])
                    {
                        foreach (var purchaseDetails in purchase.PurchaseDetails)
                        {
                            if (purchaseDetails.ItemType == Constants.ItemTypeIsMedicine)
                            {
                                await _PurchaseRepository.UpdateMedicineQuantityRollback((int)purchaseDetails.ItemId, (int)purchaseDetails.Quantity);
                            }
                            else if (purchaseDetails.ItemType == Constants.ItemTypeIsScientificApparatus)
                            {
                                await _PurchaseRepository.UpdateScientificApparatusQuantityRollback((int)purchaseDetails.ItemId, (int)purchaseDetails.Quantity);
                            }
                        }
                        bool updateStatus = await _PurchaseRepository.Update(purchaseGetOneRequest, purchaseUpdateRequest);

                        return updateStatus
                            ? Helpers.Respond(message: "Purchase successfully updated")
                            : Helpers.Respond(status: 401, message: "Purchase updation failed");
                    }

                }
                return Helpers.Respond(status: 401, message: "Purchase updation failed, cannot change");
            }
            return Helpers.Respond(status: 404, message: "Purchase not found!");
        }
    }
}
