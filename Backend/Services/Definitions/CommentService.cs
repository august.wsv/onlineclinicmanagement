﻿using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;

namespace Backend.Services.Definitions
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _CommentRepository;
        private readonly IClientRepository _ClientRepository;
        private readonly IStaffMemberRepository _StaffMemberRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CommentService(
            ICommentRepository CommentRepository,
            IClientRepository ClientRepository,
            IStaffMemberRepository StaMemberRepository,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _CommentRepository = CommentRepository;
            _ClientRepository = ClientRepository;
            _StaffMemberRepository = StaMemberRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<object> GetListByEducationalEventId(CommentGetListRequest commentGetListRequest)
        {
            List<Comment> commentList = await _CommentRepository.GetListByEducationalEventId(commentGetListRequest);

            foreach (var comment in commentList)
            {
                switch (comment.PersonType)
                {
                    case Constants.PersonTypeIsClient:
                        var client = await _ClientRepository.GetOne(new ClientGetOneRequest { Id = comment.PersonId });
                        comment.PersonName = client.Name;
                        break;
                    case Constants.PersonTypeIsStaffMember:
                        var staffMember = await _StaffMemberRepository.GetOne(new StaffMemberGetOneRequest { Id = comment.PersonId });
                        comment.PersonName = staffMember.Name;
                        break;
                    default:
                        break;
                }
            }

            return Helpers.Respond(data: commentList);
        }

        public async Task<object> Create(CommentCreateRequest commentCreateRequest)
        {
            string personTypeClaim = _httpContextAccessor.HttpContext.User?.FindFirst("PersonType").Value;
            int personId = personTypeClaim == "Client"
                ? int.Parse(_httpContextAccessor.HttpContext.User?.FindFirst("ClientId").Value)
                : int.Parse(_httpContextAccessor.HttpContext.User?.FindFirst("StaffMembertId").Value);
            int personType = personTypeClaim == "Client"
                ? Constants.PersonTypeId["CLIENT"]
                : Constants.PersonTypeId["STAFFMEMBER"];
            Comment commentToCreate = new Comment
            {
                EducationalEventId = commentCreateRequest.EducationalEventId,
                PersonId = personId,
                PersonType = personType,
                Content = commentCreateRequest.Content,
                ParentId = commentCreateRequest?.ParentId,
            };
            bool createStatus = await _CommentRepository.Create(commentToCreate);

            return createStatus
                ? Helpers.Respond(message: "Comment successfully created")
                : Helpers.Respond(status: 401, message: "Comment creation failed");
        }
    }
}
