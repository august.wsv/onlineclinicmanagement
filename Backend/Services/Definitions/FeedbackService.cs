﻿using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;

namespace Backend.Services.Definitions
{
    public class FeedbackService : IFeedbackService
    {
        private readonly IFeedbackRepository _FeedbackRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public FeedbackService(
            IFeedbackRepository FeedbackRepository,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _FeedbackRepository = FeedbackRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<object> GetListByItem(FeedbackGetListRequest feedbackGetListRequest)
        {
            List<Feedback> feedbackList = await _FeedbackRepository.GetListByItem(feedbackGetListRequest);

            return Helpers.Respond(data: feedbackList);
        }

        public async Task<object> CheckExist(FeedbacktCheckExistRequest feedbacktCheckExistRequest)
        {
            bool feedback = await _FeedbackRepository.CheckExist(feedbacktCheckExistRequest);
            return feedback
                ? Helpers.Respond(data: true)
                : Helpers.Respond(data: false);

        }

        public async Task<object> Create(FeedbackCreateRequest feedbackCreateRequest, int clientId)
        {

            bool feedback = await _FeedbackRepository.CheckExist(
                new FeedbacktCheckExistRequest
                {
                    ClientId = clientId,
                    ItemId = feedbackCreateRequest.ItemId,
                    ItemType = feedbackCreateRequest.ItemType
                }
            );
            if (!feedback)
            {
                Feedback feedbackToCreate = new Feedback
                {
                    ClientId = clientId,
                    Detail = feedbackCreateRequest.Detail,
                    Rating = feedbackCreateRequest.Rating,
                    ItemId = feedbackCreateRequest.ItemId,
                    ItemType = feedbackCreateRequest.ItemType
                };
                bool createStatus = await _FeedbackRepository.Create(feedbackToCreate);

                return createStatus
                    ? Helpers.Respond(message: "Thank you for feedback!")
                    : Helpers.Respond(status: 401, message: "Feedback creation failed");
            }
            return Helpers.Respond(status: 401, message: "Feedback creation failed, It is exist");

        }
    }
}
