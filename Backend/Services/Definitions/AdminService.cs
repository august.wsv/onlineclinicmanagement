﻿using Microsoft.IdentityModel.Tokens;
using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using X.PagedList;

namespace Backend.Services.Definitions
{
    public class AdminService : IAdminService
    {
        private readonly IAdminRepository _AdminRepository;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AdminService
        (
            IAdminRepository AdminRepository,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _AdminRepository = AdminRepository;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
        }

        private JwtSecurityToken GetJwtToken(List<Claim> authClaim)
        {
            SymmetricSecurityKey authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
            JwtSecurityToken token = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddDays(7),
                claims: authClaim,
                signingCredentials: new SigningCredentials(
                    authSigningKey,
                    SecurityAlgorithms.HmacSha256
                )
            );
            return token;
        }

        public async Task<object> Login(AdminLoginRequest adminLoginRequest)
        {
            AdminGetOneByUsernameRequest adminGetOneByUsernameRequest = new AdminGetOneByUsernameRequest
            {
                Username = adminLoginRequest.Username
            };
            Admin admin = await _AdminRepository.GetOneByUsername(adminGetOneByUsernameRequest);

            if (admin is not null && admin.Password.Length > 0 && BCrypt.Net.BCrypt.Verify(adminLoginRequest.Password, admin.Password))
            {
                switch (admin.Role)
                {
                    case Constants.AdminRoleIsCollaborator:
                        admin.Rolename = "Collaborator";
                        break;
                    case Constants.AdminRoleIsSuperAdmin:
                        admin.Rolename = "Super Admin";
                        break;
                }

                List<Claim> authClaims = new List<Claim>
                {
                    new Claim("PersonType", "Admin"),
                    new Claim("AdminRole", admin.Role.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };
                JwtSecurityToken token = GetJwtToken(authClaims);

                var meta = new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token)
                };

                return Helpers.Respond(data: admin, meta: meta);
            }
            return Helpers.Respond(status: 401, message: "Username or Password is not correct");
        }

        public async Task<object> Register(AdminRegisterRequest adminRegisterRequest)
        {
            AdminGetOneByUsernameRequest adminGetOneByUsernameRequest = new AdminGetOneByUsernameRequest
            {
                Username = adminRegisterRequest.Username
            };
            Admin admin = await _AdminRepository.GetOneByUsername(adminGetOneByUsernameRequest);

            if (admin is null)
            {
                Admin adminToCreate = new Admin
                {
                    Name = adminRegisterRequest.Name,
                    Username = adminRegisterRequest.Username,
                    Password = BCrypt.Net.BCrypt.HashPassword(adminRegisterRequest.Password),
                    Role = adminRegisterRequest.Role
                };
                bool CreateStatus = await _AdminRepository.Create(adminToCreate);

                return CreateStatus
                    ? Helpers.Respond(message: "Account successfully created")
                    : Helpers.Respond(status: 401, message: "Account creation failed");
            }
            return Helpers.Respond(status: 401, message: "Username already exists, cannot be created!");
        }

        public async Task<object> GetList(AdminGetListRequest adminGetListRequest)
        {
            IPagedList<Admin> adminPagedList = await _AdminRepository.GetList(adminGetListRequest);

            foreach (var admin in adminPagedList)
            {
                switch (admin.Role)
                {
                    case Constants.AdminRoleIsCollaborator:
                        admin.Rolename = "Collaborator";
                        break;
                    case Constants.AdminRoleIsSuperAdmin:
                        admin.Rolename = "Super Admin";
                        break;
                }
            }
            object meta = Helpers.GetPaginationMetadata(adminPagedList, adminGetListRequest.Page, adminGetListRequest.PageSize, _httpContextAccessor);

            return Helpers.Respond(data: adminPagedList, meta: meta);
        }

        public async Task<object> Delete(AdminGetOneRequest adminGetOneRequest)
        {
            Admin admin = await _AdminRepository.GetOne(adminGetOneRequest);
            if (admin.Role == Constants.AdminRole["SUPERADMIN"])
            {
                return Helpers.Respond(status: 403, message: "Cannot delete super admin!");
            }
            if (admin is not null)
            {
                bool updateStatus = await _AdminRepository.Delete(adminGetOneRequest);

                return updateStatus
                    ? Helpers.Respond(message: "Admin successfully deleted")
                    : Helpers.Respond(status: 401, message: "Admin deletion failed");
            }
            return Helpers.Respond(status: 404, message: "Admin not found!");
        }

        public async Task<object> Update(AdminGetOneRequest adminGetOneRequest, AdminUpdateRequest adminUpdateRequest)
        {
            Admin admin = await _AdminRepository.GetOne(adminGetOneRequest);
            if (admin is not null)
            {
                bool updateStatus = await _AdminRepository.Update(adminGetOneRequest, adminUpdateRequest);

                return updateStatus
                    ? Helpers.Respond(message: "StaffMember successfully updated")
                    : Helpers.Respond(status: 401, message: "StaffMember updation failed");
            }
            return Helpers.Respond(status: 404, message: "StaffMember not found!");
        }
    }
}
