﻿using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;
using X.PagedList;

namespace Backend.Services.Definitions
{
    public class MedicineService : IMedicineService
    {
        private readonly IMedicineRepository _MedicineRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IWebHostEnvironment _env;

        public MedicineService(
            IMedicineRepository MedicineRepository,
            IHttpContextAccessor httpContextAccessor,
            IWebHostEnvironment env
        )
        {
            _MedicineRepository = MedicineRepository;
            _httpContextAccessor = httpContextAccessor;
            _env = env;
        }

        public async Task<object> GetList(MedicineGetListRequest medicineGetListRequest)
        {
            IPagedList<Medicine> medicinePagedList = await _MedicineRepository.GetList(medicineGetListRequest);

            var request = _httpContextAccessor.HttpContext?.Request;

            foreach (var medicine in medicinePagedList)
            {
                medicine.ImagePath = $"{request?.Scheme}://{request?.Host}{medicine.ImagePath}";
            }

            object meta = Helpers.GetPaginationMetadata(medicinePagedList, medicineGetListRequest.Page, medicineGetListRequest.PageSize, _httpContextAccessor);

            return Helpers.Respond(data: medicinePagedList, meta: meta);
        }

        public async Task<object> GetOne(MedicineGetOneRequest medicineGetOneRequest)
        {
            Medicine? medicine = await _MedicineRepository.GetOne(medicineGetOneRequest);

            if (medicine is not null)
            {
                var request = _httpContextAccessor.HttpContext?.Request;
                medicine.ImagePath = $"{request?.Scheme}://{request?.Host}{medicine.ImagePath}";
                return Helpers.Respond(data: medicine);
            }
            return Helpers.Respond(status: 404, message: "Medicine not found!");
        }

        public async Task<object> Create(MedicineCreateRequest medicineCreateRequest)
        {
            Medicine medicineToCreate = new Medicine
            {
                Name = medicineCreateRequest.Name,
                ImagePath = await Helpers.UploadFile(medicineCreateRequest.ImagePath, "Medicine", _env),
                Code = medicineCreateRequest.Code,
                Quantity = (int)medicineCreateRequest.Quantity,
                Price = (decimal)medicineCreateRequest.Price,
                Description = medicineCreateRequest.Description,
                MedicineTypeId = (int)medicineCreateRequest.MedicineTypeId,
                ExpDate = medicineCreateRequest.ExpDate
            };
            bool createStatus = await _MedicineRepository.Create(medicineToCreate);

            return createStatus
                ? Helpers.Respond(message: "Medicine successfully created")
                : Helpers.Respond(status: 401, message: "Medicine creation failed");
        }

        public async Task<object> Update(MedicineGetOneRequest medicineGetOneRequest, MedicineUpdateRequest medicineUpdateRequest)
        {
            Medicine? medicineToUpdate = await _MedicineRepository.GetOne(medicineGetOneRequest);
            if (medicineToUpdate is not null)
            {
                if (medicineUpdateRequest.Name != null)
                { medicineToUpdate.Name = medicineUpdateRequest.Name; }

                if (medicineUpdateRequest.ImagePath != null && medicineUpdateRequest.ImagePath.Length > 0)
                { medicineToUpdate.ImagePath = await Helpers.UploadFile(medicineUpdateRequest.ImagePath, "Medicine", _env); }

                if (medicineUpdateRequest.Code != null)
                { medicineToUpdate.Code = medicineUpdateRequest.Code; }

                if (medicineUpdateRequest.Quantity != null)
                { medicineToUpdate.Quantity = (int)medicineUpdateRequest.Quantity; }

                if (medicineUpdateRequest.Price != null)
                { medicineToUpdate.Price = (int)medicineUpdateRequest.Price; }

                if (medicineUpdateRequest.Description != null)
                { medicineToUpdate.Description = medicineUpdateRequest.Description; }

                if (medicineUpdateRequest.MedicineTypeId != null)
                { medicineToUpdate.MedicineTypeId = (int)medicineUpdateRequest.MedicineTypeId; }

                if (medicineUpdateRequest.ExpDate >= DateTime.MinValue)
                { medicineToUpdate.ExpDate = medicineUpdateRequest.ExpDate; }

                bool updateStatus = await _MedicineRepository.Update(medicineToUpdate);

                return updateStatus
                    ? Helpers.Respond(message: "Medicine successfully updated")
                    : Helpers.Respond(status: 401, message: "Medicine updation failed");
            }
            return Helpers.Respond(status: 404, message: "Medicine not found!");
        }

        public async Task<object> Delete(MedicineGetOneRequest medicineGetOneRequest)
        {
            Medicine? medicineToDelete = await _MedicineRepository.GetOne(medicineGetOneRequest);
            if (medicineToDelete is not null)
            {
                medicineToDelete.DeletedAt = DateTime.Now;
                bool updateStatus = await _MedicineRepository.Update(medicineToDelete);

                return updateStatus
                    ? Helpers.Respond(message: "Medicine successfully deleted")
                    : Helpers.Respond(status: 401, message: "Medicine deletion failed");
            }
            return Helpers.Respond(status: 404, message: "Medicine not found!");
        }
    }
}
