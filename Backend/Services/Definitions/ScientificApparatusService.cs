﻿using Backend.Commons;
using Backend.Request;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Services.Contracts;
using X.PagedList;

namespace Backend.Services.Definitions
{
    public class ScientificApparatusService : IScientificApparatusService
    {
        private readonly IScientificApparatusRepository _ScientificApparatusRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IWebHostEnvironment _env;

        public ScientificApparatusService(
            IScientificApparatusRepository ScientificApparatusRepository,
            IHttpContextAccessor httpContextAccessor,
            IWebHostEnvironment env
        )
        {
            _ScientificApparatusRepository = ScientificApparatusRepository;
            _httpContextAccessor = httpContextAccessor;
            _env = env;
        }

        public async Task<object> GetList(ScientificApparatusGetListRequest scientificApparatusGetListRequest)
        {
            IPagedList<ScientificApparatus> scientificApparatusPagedList = await _ScientificApparatusRepository.GetList(scientificApparatusGetListRequest);

            HttpRequest request = _httpContextAccessor.HttpContext.Request;

            foreach (var medicine in scientificApparatusPagedList)
            {
                medicine.ImagePath = $"{request.Scheme}://{request.Host}{medicine.ImagePath}";
            }

            object meta = Helpers.GetPaginationMetadata(scientificApparatusPagedList, scientificApparatusGetListRequest.Page, scientificApparatusGetListRequest.PageSize, _httpContextAccessor);

            return Helpers.Respond(data: scientificApparatusPagedList, meta: meta);
        }

        public async Task<object> GetOne(ScientificApparatusGetOneRequest scientificApparatusGetOneRequest)
        {
            ScientificApparatus scientificApparatus = await _ScientificApparatusRepository.GetOne(scientificApparatusGetOneRequest);

            if (scientificApparatus is not null)
            {
                HttpRequest request = _httpContextAccessor.HttpContext.Request;
                scientificApparatus.ImagePath = $"{request.Scheme}://{request.Host}{scientificApparatus.ImagePath}";
                return Helpers.Respond(data: scientificApparatus);
            }
            return Helpers.Respond(status: 404, message: "ScientificApparatus not found!");
        }

        public async Task<object> Create(ScientificApparatusCreateRequest scientificApparatusCreateRequest)
        {
            ScientificApparatus scientificApparatusToCreate = new ScientificApparatus
            {
                Name = scientificApparatusCreateRequest.Name,
                ImagePath = await Helpers.UploadFile(scientificApparatusCreateRequest.ImagePath, "ScientificApparatus", _env),
                Code = scientificApparatusCreateRequest.Code,
                Quantity = (int)scientificApparatusCreateRequest.Quantity,
                Price = (int)scientificApparatusCreateRequest.Price,
                Description = scientificApparatusCreateRequest.Description,
                ExpDate = scientificApparatusCreateRequest.ExpDate,
                Origin = scientificApparatusCreateRequest.Origin,
            };
            bool createStatus = await _ScientificApparatusRepository.Create(scientificApparatusToCreate);

            return createStatus
                ? Helpers.Respond(message: "ScientificApparatus successfully created")
                : Helpers.Respond(status: 401, message: "ScientificApparatus creation failed");
        }

        public async Task<object> Update(ScientificApparatusGetOneRequest scientificApparatusGetOneRequest, ScientificApparatusUpdateRequest scientificApparatusUpdateRequest)
        {
            ScientificApparatus scientificApparatus = await _ScientificApparatusRepository.GetOne(scientificApparatusGetOneRequest);
            if (scientificApparatus is not null)
            {
                bool updateStatus = await _ScientificApparatusRepository.Update(scientificApparatusGetOneRequest, scientificApparatusUpdateRequest);

                return updateStatus
                    ? Helpers.Respond(message: "ScientificApparatus successfully updated")
                    : Helpers.Respond(status: 401, message: "ScientificApparatus updation failed");
            }
            return Helpers.Respond(status: 404, message: "ScientificApparatus not found!");
        }

        public async Task<object> Delete(ScientificApparatusGetOneRequest scientificApparatusGetOneRequest)
        {
            ScientificApparatus scientificApparatus = await _ScientificApparatusRepository.GetOne(scientificApparatusGetOneRequest);
            if (scientificApparatus is not null)
            {
                bool updateStatus = await _ScientificApparatusRepository.Delete(scientificApparatusGetOneRequest);

                return updateStatus
                    ? Helpers.Respond(message: "ScientificApparatus successfully deleted")
                    : Helpers.Respond(status: 401, message: "ScientificApparatus deletion failed");
            }
            return Helpers.Respond(status: 404, message: "ScientificApparatus not found!");
        }
    }
}
