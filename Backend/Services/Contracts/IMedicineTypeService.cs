﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface IMedicineTypeService
    {
        Task<object> GetList(MedicineTypeGetListRequest medicineTypeGetListRequest);
        Task<object> GetAll();
        Task<object> GetOne(MedicineTypeGetOneRequest medicineTypeGetOneRequest);
        Task<object> Create(MedicineTypeCreateRequest medicineTypeCreateRequest);
        Task<object> Update(MedicineTypeGetOneRequest medicineTypeGetOneRequest, MedicineTypeUpdateRequest medicineTypeUpdateRequest);
        Task<object> Delete(MedicineTypeGetOneRequest medicineTypeGetOneRequest);
        
    }
}
