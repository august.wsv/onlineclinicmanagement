﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface IEducationalEventService
    {
        Task<object> GetList(EducationalEventGetListRequest educationalEventGetListRequest);
        Task<object> GetListBySubject(EducationalEventGetListRequest educationalEventGetListRequest);
        Task<object> GetOne(EducationalEventGetOneRequest educationalEventGetOneRequest);
        Task<object> Create(EducationalEventCreateRequest educationalEventCreateRequest, int staffMembertId);
        Task<object> Update(EducationalEventGetOneRequest educationalEventGetOneRequest, EducationalEventUpdateRequest educationalEventUpdateRequest);
        Task<object> Delete(EducationalEventGetOneRequest educationalEventGetOneRequest);
        
    }
}
