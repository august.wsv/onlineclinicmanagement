﻿namespace Backend.Services.Contracts
{
    public interface IBusinessReportService
    {
        Task<object> GetThisWeekReport();
        Task<object> GetThisMonthReport();
    }
}
