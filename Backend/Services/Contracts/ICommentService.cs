﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface ICommentService
    {
        Task<object> GetListByEducationalEventId(CommentGetListRequest commentGetListRequest);
        Task<object> Create(CommentCreateRequest commentCreateRequest);
    }
}
