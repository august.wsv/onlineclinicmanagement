﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface IPurchaseService
    {
        Task<object> GetPagedList(PurchaseGetPagedListRequest purchaseGetPagedListRequest, IHttpContextAccessor httpContextAccessor);
        Task<object> GetListByClient(PurchaseGetListRequest purchaseGetListRequest);
        Task<object> GetOne(PurchaseGetOneRequest purchaseGetOneRequest);
        Task<object> Create(PurchaseCreateRequest purchaseCreateRequest, int clientId);
        Task<object> Update(PurchaseGetOneRequest purchaseGetOneRequest, PurchaseUpdateRequest purchaseUpdateRequest);        
    }
}
