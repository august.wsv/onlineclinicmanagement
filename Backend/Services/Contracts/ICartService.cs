﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface ICartService
    {
        Task<object> GetListByClient(CartGetListRequest cartGetListRequest);
        Task<object> Create(CartCreateRequest cartCreateRequest, int clientId);
        Task<object> Update(CartGetOneRequest cartGetOneRequest, CartUpdateRequest cartUpdateRequest, int clientId);
        Task<object> Delete(CartGetOneRequest cartGetOneRequest);
        Task<dynamic> DeleteAllOfClient(CartGetListRequest cartGetListRequest);
    }
}
