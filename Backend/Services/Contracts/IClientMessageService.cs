﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface IClientMessageService
    {
        Task<object> GetList(ClientMessageGetListRequest clientMessageGetListRequest);
        Task<object> GetOne(ClientMessageGetOneRequest clientMessageGetOneRequest);
        Task<object> Create(ClientMessageCreateRequest clientMessageCreateRequest, int clientId);
    }
}
