﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface IMedicineService
    {
        Task<object> GetList(MedicineGetListRequest medicineGetListRequest);
        Task<object> GetOne(MedicineGetOneRequest medicineGetOneRequest);
        Task<object> Create(MedicineCreateRequest medicineCreateRequest);
        Task<object> Update(MedicineGetOneRequest medicineGetOneRequest, MedicineUpdateRequest medicineUpdateRequest);
        Task<object> Delete(MedicineGetOneRequest medicineGetOneRequest);        
    }
}
