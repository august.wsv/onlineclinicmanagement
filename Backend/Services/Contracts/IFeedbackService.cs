﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface IFeedbackService
    {
        Task<object> GetListByItem(FeedbackGetListRequest feedbackGetListRequest);
        Task<object> CheckExist(FeedbacktCheckExistRequest feedbacktCheckExistRequest);
        Task<object> Create(FeedbackCreateRequest feedbackCreateRequest, int clientId);        
    }
}
