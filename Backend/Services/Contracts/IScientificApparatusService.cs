﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface IScientificApparatusService
    {
        Task<object> GetList(ScientificApparatusGetListRequest scientificApparatusGetListRequest);
        Task<object> GetOne(ScientificApparatusGetOneRequest scientificApparatusGetOneRequest);
        Task<object> Create(ScientificApparatusCreateRequest scientificApparatusCreateRequest);
        Task<object> Update(ScientificApparatusGetOneRequest scientificApparatusGetOneRequest, ScientificApparatusUpdateRequest scientificApparatusUpdateRequest);
        Task<object> Delete(ScientificApparatusGetOneRequest scientificApparatusGetOneRequest);        
    }
}
