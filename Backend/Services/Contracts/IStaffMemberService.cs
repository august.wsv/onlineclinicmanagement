﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface IStaffMemberService
    {
        Task<object> GetList(StaffMemberGetListRequest staffMemberGetListRequest);
        Task<object> Login(StaffMemberLoginRequest staffMemberLoginRequest);
        Task<object> Register(StaffMemberRegisterRequest staffMemberRegisterRequest);
        Task<object> Update(StaffMemberGetOneRequest staffMemberGetOneRequest, StaffMemberUpdateRequest staffMemberUpdateRequest);
        Task<object> Delete(StaffMemberGetOneRequest staffMemberGetOneRequest);
        Task<object> AboutStaffMember(StaffMemberGetOneRequest staffMemberGetOneRequest);
    }
}
