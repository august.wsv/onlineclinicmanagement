﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface IAdminService
    {
        Task<object> GetList(AdminGetListRequest adminGetListRequest);
        Task<object> Login(AdminLoginRequest adminLoginRequest);
        Task<object> Register(AdminRegisterRequest adminRegisterRequest);
        Task<object> Delete(AdminGetOneRequest adminGetOneRequest);
        Task<object> Update(AdminGetOneRequest adminGetOneRequest, AdminUpdateRequest adminUpdateRequest);
    }
}
