﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface IClinicService
    {
        Task<object> GetList(ClinicGetListRequest clinicGetListRequest);
        Task<object> GetAll();
        Task<object> GetOne(ClinicGetOneRequest clinicGetOneRequest);
        Task<object> Create(ClinicCreateRequest clinicCreateRequest);
        Task<object> Update(ClinicGetOneRequest clinicGetOneRequest, ClinicUpdateRequest clinicUpdateRequest);
        Task<object> Delete(ClinicGetOneRequest clinicGetOneRequest);        
    }
}
