﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface ISubjectService
    {
        Task<object> GetList(SubjectGetListRequest subjectGetListRequest);
        Task<object> GetOne(SubjectGetOneRequest subjectGetOneRequest);
        Task<object> Create(SubjectCreateRequest subjectCreateRequest);
        Task<object> Update(SubjectGetOneRequest subjectGetOneRequest, SubjectUpdateRequest subjectUpdateRequest);
        Task<object> Delete(SubjectGetOneRequest subjectGetOneRequest);        
    }
}
