﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface IAboutUsService
    {
        Task<object> GetAll();
        Task<object> GetOne(AboutUsGetOneRequest aboutUsGetOneRequest);
        Task<object> Update(AboutUsGetOneRequest aboutUsGetOneRequest, AboutUsUpdateRequest aboutUsUpdateRequest);
    }
}
