﻿using Backend.Request;

namespace Backend.Services.Contracts
{
    public interface IClientService
    {
        Task<object> GetList(ClientGetListRequest clientGetListRequest);
        Task<object> Login(ClientLoginRequest clientLoginRequest);
        Task<object> EmailVerification(ClientEmailVerificationRequest clientEmailVerificationRequest);
        Task<object> Register(ClientRegisterRequest clientRegisterRequest);
        Task<object> AboutClient(ClientGetOneRequest clientGetOneRequest);
        Task<object> Update(ClientGetOneRequest clientGetOneRequest, ClientUpdateRequest clientUpdateRequest);
        Task<object> Delete(ClientGetOneRequest clientGetOneRequest);
    }
}