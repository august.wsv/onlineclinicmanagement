﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class ScientificApparatusController : ControllerBase
    {
        private readonly IScientificApparatusService _scientificApparatusService;

        public ScientificApparatusController(IScientificApparatusService scientificApparatusService)
        {
            _scientificApparatusService = scientificApparatusService;
        }

        [HttpGet]
        public async Task<IActionResult> GetList(string? name, string? code, decimal? maxPrice, decimal? minPrice, DateTime maxExpDate, DateTime minExpDate, string? origin, int? page, int? pageSize)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            ScientificApparatusGetListRequest scientificApparatusGetListRequest = new ScientificApparatusGetListRequest
            {
                Name = name,
                Code = code,
                MaxPrice = maxPrice,
                MinPrice = minPrice,
                MaxExpDate = maxExpDate,
                MinExpDate = minExpDate,
                Origin = origin,
                Page = page ?? Constants.DefaultPage,
                PageSize = pageSize ?? Constants.DefaultPageSize
            };

            dynamic result = await _scientificApparatusService.GetList(scientificApparatusGetListRequest);
            return result;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetOne(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            ScientificApparatusGetOneRequest scientificApparatusGetOneRequest = new ScientificApparatusGetOneRequest
            {
                Id = id
            };
            dynamic result = await _scientificApparatusService.GetOne(scientificApparatusGetOneRequest);
            return result;
        }

        [HttpPost]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Create([FromForm] ScientificApparatusCreateRequest scientificApparatusCreateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            dynamic result = await _scientificApparatusService.Create(scientificApparatusCreateRequest);
            return result;
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Update(int id, [FromForm] ScientificApparatusUpdateRequest scientificApparatusUpdateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            ScientificApparatusGetOneRequest scientificApparatusGetOneRequest = new ScientificApparatusGetOneRequest
            {
                Id = id
            };
            dynamic result = await _scientificApparatusService.Update(scientificApparatusGetOneRequest, scientificApparatusUpdateRequest);
            return result;
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            ScientificApparatusGetOneRequest scientificApparatusGetOneRequest = new ScientificApparatusGetOneRequest
            {
                Id = id
            };
            dynamic result = await _scientificApparatusService.Delete(scientificApparatusGetOneRequest);
            return result;
        }
    }
}
