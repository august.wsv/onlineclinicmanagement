﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class FeedbackController : ControllerBase
    {
        private readonly IFeedbackService _feedbackService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public FeedbackController
        (
            IFeedbackService feedbackService,
            IHttpContextAccessor httpContextAccessor

        )
        {
            _feedbackService = feedbackService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        [Route("{itemType}/{itemId}")]      
        public async Task<IActionResult> GetListByItem(int itemId, int itemType)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            FeedbackGetListRequest feedbackGetListRequest = new FeedbackGetListRequest
            {
                ItemId = itemId,
                ItemType = itemType
            };

            dynamic result = await _feedbackService.GetListByItem(feedbackGetListRequest);
            return result;
        }

        [HttpGet]
        [Route("{itemType}/{itemId}")]
        [Authorize]
        [Authorize(Policy = "Client")]
        public async Task<IActionResult> CheckExist(int itemId, int itemType)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            string clientIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("ClientId").Value;
            int clientId = int.Parse(clientIdClaim);
            FeedbacktCheckExistRequest feedbacktCheckExistRequest = new FeedbacktCheckExistRequest
            {
                ClientId = clientId,
                ItemId = itemId,
                ItemType = itemType
            };

            dynamic result = await _feedbackService.CheckExist(feedbacktCheckExistRequest);
            return result;
        }

        [HttpPost]
        [Authorize]
        [Authorize(Policy = "Client")]
        public async Task<IActionResult> Create([FromForm] FeedbackCreateRequest feedbackCreateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            string clientIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("ClientId").Value;
            int clientId = int.Parse(clientIdClaim);
            dynamic result = await _feedbackService.Create(feedbackCreateRequest, clientId);
            return result;
        }
    }
}
