﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class EducationalEventController : ControllerBase
    {
        private readonly IEducationalEventService _educationalEventService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public EducationalEventController
        (
            IEducationalEventService educationalEventService,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _educationalEventService = educationalEventService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public async Task<IActionResult> GetList(string? name, int? educationalEventType, int? subjectId, int? staffMemberId, DateTime maxCreatedAt, DateTime minCreatedAt, int? page, int? pageSize)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            EducationalEventGetListRequest educationalEventGetListRequest = new EducationalEventGetListRequest
            {
                Name = name,
                EducationalEventType = educationalEventType,
                SubjectId = subjectId,
                StaffMemberId = staffMemberId,
                MaxCreatedAt = maxCreatedAt,
                MinCreatedAt = minCreatedAt,
                Page = page ?? Constants.DefaultPage,
                PageSize = pageSize ?? Constants.DefaultPageSize
            };

            dynamic result = await _educationalEventService.GetList(educationalEventGetListRequest);
            return result;
        }

        [HttpGet]
        [Route("{subjectId}")]      
        public async Task<IActionResult> GetListBySubject(int subjectId)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            EducationalEventGetListRequest educationalEventGetListRequest = new EducationalEventGetListRequest
            {
                SubjectId = subjectId
            };

            dynamic result = await _educationalEventService.GetListBySubject(educationalEventGetListRequest);
            return result;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetOne(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            EducationalEventGetOneRequest educationalEventGetOneRequest = new EducationalEventGetOneRequest
            {
                Id = id
            };
            dynamic result = await _educationalEventService.GetOne(educationalEventGetOneRequest);
            return result;
        }

        [HttpPost]
        [Authorize]
        [Authorize(Policy = "StaffMember")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> Create([FromForm] EducationalEventCreateRequest educationalEventCreateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            string staffMemberIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("StaffMemberId").Value;
            int staffMemberId = int.Parse(staffMemberIdClaim);
            dynamic result = await _educationalEventService.Create(educationalEventCreateRequest, staffMemberId);
            return result;
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "StaffMember")]
        public async Task<IActionResult> Update(int id, [FromForm] EducationalEventUpdateRequest educationalEventUpdateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            EducationalEventGetOneRequest educationalEventGetOneRequest = new EducationalEventGetOneRequest
            {
                Id = id
            };
            dynamic result = await _educationalEventService.Update(educationalEventGetOneRequest, educationalEventUpdateRequest);
            return result;
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "StaffMember")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            EducationalEventGetOneRequest educationalEventGetOneRequest = new EducationalEventGetOneRequest
            {
                Id = id
            };
            dynamic result = await _educationalEventService.Delete(educationalEventGetOneRequest);
            return result;
        }
    }
}
