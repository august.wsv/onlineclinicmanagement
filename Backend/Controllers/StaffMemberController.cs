﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class StaffMemberController : ControllerBase
    {
        private readonly IStaffMemberService _staffMemberService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public StaffMemberController
        (
            IStaffMemberService staffMemberService, 
            IHttpContextAccessor httpContextAccessor
        )
        {
            _staffMemberService = staffMemberService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromForm] StaffMemberLoginRequest staffMemberLoginRequest)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            dynamic result = await _staffMemberService.Login(staffMemberLoginRequest);
            return result;
        }

        [HttpPost]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Register([FromForm] StaffMemberRegisterRequest staffMemberRegisterRequest)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            dynamic result = await _staffMemberService.Register(staffMemberRegisterRequest);
            return result;
        }

        [HttpGet]
        [Authorize]
        [Authorize(Policy = "AllAdmin")]
        public async Task<IActionResult> GetList(string? name, string? phone, string? email, int? page, int? pageSize)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            StaffMemberGetListRequest staffMemberGetListRequest = new StaffMemberGetListRequest
            {
                Name = name,
                Email = email,
                Phone = phone,
                Page = page ?? Constants.DefaultPage,
                PageSize = pageSize ?? Constants.DefaultPageSize
            };

            dynamic result = await _staffMemberService.GetList(staffMemberGetListRequest);
            return result;
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "CanUpdateStaffMember")]
        public async Task<IActionResult> Update(int id, [FromForm] StaffMemberUpdateRequest staffMemberUpdateRequest)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            StaffMemberGetOneRequest staffMemberGetOneRequest = new StaffMemberGetOneRequest
            {
                Id = id
            };
            dynamic result = await _staffMemberService.Update(staffMemberGetOneRequest, staffMemberUpdateRequest);
            return result;
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            StaffMemberGetOneRequest staffMemberGetOneRequest = new StaffMemberGetOneRequest
            {
                Id = id
            };
            dynamic result = await _staffMemberService.Delete(staffMemberGetOneRequest);
            return result;
        }

        [HttpGet]
        [Authorize]
        [Authorize(Policy = "StaffMember")]
        public async Task<IActionResult> AboutStaffMember()
        {
            string staffMemberIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("StaffMemberId").Value;
            int staffMemberId = int.Parse(staffMemberIdClaim);
            StaffMemberGetOneRequest staffMemberGetOneRequest = new StaffMemberGetOneRequest
            {
                Id = staffMemberId
            };
            dynamic result = await _staffMemberService.AboutStaffMember(staffMemberGetOneRequest);
            return result;
        }
    }
}
