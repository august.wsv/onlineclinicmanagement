﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class ClientController : ControllerBase
    {
        private readonly IClientService _clientService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ClientController
        (
            IClientService clientService,
            IHttpContextAccessor httpContextAccessor

        )
        {
            _clientService = clientService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromForm] ClientLoginRequest clientLoginRequest)
        {
            if (!ModelState.IsValid) { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            dynamic result = await _clientService.Login(clientLoginRequest);
            return result;
        }

        [HttpPost]
        public async Task<IActionResult> EmailVerification([FromForm] ClientEmailVerificationRequest clientEmailVerificationRequest)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            dynamic result = await _clientService.EmailVerification(clientEmailVerificationRequest);
            return result;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Register([FromForm] ClientRegisterRequest clientRegisterRequest)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            dynamic result = await _clientService.Register(clientRegisterRequest);
            return result;
        }

        [HttpGet]
        [Authorize]
        [Authorize(Policy = "Client")]
        public async Task<IActionResult> AboutClient()
        {
            string clientIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("ClientId").Value;
            int clientId = int.Parse(clientIdClaim);
            ClientGetOneRequest clientGetOneRequest = new ClientGetOneRequest
            {
                Id = clientId
            };
            dynamic result = await _clientService.AboutClient(clientGetOneRequest);
            return result;
        }

        [HttpPut]
        [Authorize]
        [Authorize(Policy = "Client")]
        public async Task<IActionResult> Update([FromForm] ClientUpdateRequest clientUpdateRequest)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            string clientIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("ClientId").Value;
            int clientId = int.Parse(clientIdClaim);
            ClientGetOneRequest clientGetOneRequest = new ClientGetOneRequest
            {
                Id = clientId
            };
            dynamic result = await _clientService.Update(clientGetOneRequest, clientUpdateRequest);
            return result;
        }

        [HttpGet]
        public async Task<IActionResult> GetList(string? name, string? phone, string? email, string? address, string? clinicName, int? page, int? pageSize)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            ClientGetListRequest clientGetListRequest = new ClientGetListRequest
            {
                Name = name,
                Phone = phone,
                Email = email,
                Address = address,
                ClinicName = clinicName,
                Page = page ?? Constants.DefaultPage,
                PageSize = pageSize ?? Constants.DefaultPageSize
            };

            dynamic result = await _clientService.GetList(clientGetListRequest);
            return result;
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            ClientGetOneRequest clientGetOneRequest = new ClientGetOneRequest
            {
                Id = id
            };
            dynamic result = await _clientService.Delete(clientGetOneRequest);
            return result;
        }
    }
}
