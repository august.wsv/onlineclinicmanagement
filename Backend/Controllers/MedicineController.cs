﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class MedicineController : ControllerBase
    {
        private readonly IMedicineService _medicineService;

        public MedicineController(IMedicineService medicineService)
        {
            _medicineService = medicineService;
        }

        [HttpGet]
        public async Task<IActionResult> GetList(string? name, string? code, decimal? maxPrice, decimal? minPrice, int? medicineTypeId, DateTime maxExpDate, DateTime minExpDate, int? page, int? pageSize)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            MedicineGetListRequest medicineGetListRequest = new MedicineGetListRequest
            {
                Name = name,
                Code = code,
                MaxPrice = maxPrice,
                MinPrice = minPrice,
                MedicineTypeId = medicineTypeId,
                MaxExpDate = maxExpDate,
                MinExpDate = minExpDate,
                Page = page ?? Constants.DefaultPage,
                PageSize = pageSize ?? Constants.DefaultPageSize
            };

            dynamic result = await _medicineService.GetList(medicineGetListRequest);
            return result;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetOne(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            MedicineGetOneRequest medicineGetOneRequest = new MedicineGetOneRequest
            {
                Id = id
            };
            dynamic result = await _medicineService.GetOne(medicineGetOneRequest);
            return result;
        }

        [HttpPost]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Create([FromForm] MedicineCreateRequest medicineCreateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            dynamic result = await _medicineService.Create(medicineCreateRequest);
            return result;
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Update(int id, [FromForm] MedicineUpdateRequest medicineUpdateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            MedicineGetOneRequest medicineGetOneRequest = new MedicineGetOneRequest
            {
                Id = id
            };
            dynamic result = await _medicineService.Update(medicineGetOneRequest, medicineUpdateRequest);
            return result;
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            MedicineGetOneRequest medicineGetOneRequest = new MedicineGetOneRequest
            {
                Id = id
            };
            dynamic result = await _medicineService.Delete(medicineGetOneRequest);
            return result;
        }
    }
}
