﻿using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class UserController : ControllerBase
    {
        private readonly IWebHostEnvironment _env;
        private readonly IAboutUsRepository _AboutUsRepository;

        public UserController(
            IWebHostEnvironment env,
            IAboutUsRepository AboutUsRepository
        )
        {
            _env = env;
            _AboutUsRepository = AboutUsRepository;
        }

        [HttpPost]
        public async Task<IActionResult> UploadFile([FromForm] FileUploadDto fileDto)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            foreach (var file in fileDto.Files)
            {
                await Helpers.UploadFile(file, "Uploads2", _env);
            }

            await Helpers.UploadFile(fileDto.File, "Upload2s", _env);

            return Ok(new { message = "Tải lên file thành công." });
        }

        [HttpPost]
        public async Task<IActionResult> Test()
        {
            //Random random = new Random();
            //int randomNumber = random.Next(0, 26);
            //char randomChar = (char)('A' + randomNumber);
            //AboutUs aboutUs = new AboutUs
            //{
            //    Id = "A" + randomChar,
            //    Description = "EFdf"
            //};

            //AboutUsGetOneRequest aboutUsGetOneRequest = new AboutUsGetOneRequest
            //{
            //    Id = "AD"
            //};

            //AboutUs? aboutUsToUpdate = await _AboutUsRepository.GetOne(aboutUsGetOneRequest);
            //if (aboutUsToUpdate is not null)
            //{
            //    aboutUsToUpdate.Description = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
            //    bool updateStatus = await _AboutUsRepository.Update(aboutUsToUpdate);

            //    return updateStatus
            //        ? Helpers.Respond(message: "AboutUs successfully updated")
            //        : Helpers.Respond(status: 401, message: "AboutUs updation failed");
            //}
            //return Helpers.Respond(status: 404, message: "AboutUs not found!");

            var data = await _AboutUsRepository.GetById(1241);
            return Helpers.Respond(data: data);


        }

    }
}
