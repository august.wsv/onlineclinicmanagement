﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _commentService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CommentController
        (
            ICommentService commentService,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _commentService = commentService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        [Route("{educationalEventId}")]
        public async Task<IActionResult> GetListByEducationalEventId(int educationalEventId)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            CommentGetListRequest commentGetListRequest = new CommentGetListRequest
            {
                EducationalEventId = educationalEventId
            };

            dynamic result = await _commentService.GetListByEducationalEventId(commentGetListRequest);
            return result;
        }

        [HttpPost]
        [Authorize]
        [Authorize(Policy = "CanCreateComment")]
        public async Task<IActionResult> Create([FromForm] CommentCreateRequest commentCreateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            dynamic result = await _commentService.Create(commentCreateRequest);
            return result;
        }
    }
}
