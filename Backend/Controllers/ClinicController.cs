﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class ClinicController : ControllerBase
    {
        private readonly IClinicService _clinicService;

        public ClinicController(IClinicService clinicService)
        {
            _clinicService = clinicService;
        }

        [HttpGet]
        public async Task<IActionResult> GetList(string? name, string? phone, string? email, string? address, int? page, int? pageSize)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            ClinicGetListRequest clinicGetListRequest = new ClinicGetListRequest
            {
                Name = name,
                Phone = phone,
                Email = email,
                Address = address,
                Page = page ?? Constants.DefaultPage,
                PageSize = pageSize ?? Constants.DefaultPageSize
            };

            dynamic result = await _clinicService.GetList(clinicGetListRequest);
            return result;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }

            dynamic result = await _clinicService.GetAll();
            return result;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetOne(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            ClinicGetOneRequest clinicGetOneRequest = new ClinicGetOneRequest
            {
                Id = id
            };
            dynamic result = await _clinicService.GetOne(clinicGetOneRequest);
            return result;
        }

        [HttpPost]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Create([FromForm] ClinicCreateRequest clinicCreateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            dynamic result = await _clinicService.Create(clinicCreateRequest);
            return result;
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Update(int id, [FromForm] ClinicUpdateRequest clinicUpdateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            ClinicGetOneRequest clinicGetOneRequest = new ClinicGetOneRequest
            {
                Id = id
            };
            dynamic result = await _clinicService.Update(clinicGetOneRequest, clinicUpdateRequest);
            return result;
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            ClinicGetOneRequest clinicGetOneRequest = new ClinicGetOneRequest
            {
                Id = id
            };
            dynamic result = await _clinicService.Delete(clinicGetOneRequest);
            return result;
        }
    }
}
