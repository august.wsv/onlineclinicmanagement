﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class AboutUsController : ControllerBase
    {
        private readonly IAboutUsService _aboutUsService;

        public AboutUsController(IAboutUsService aboutUsService)
        {
            _aboutUsService = aboutUsService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }

            dynamic result = await _aboutUsService.GetAll();
            return result;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetOne(string id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            AboutUsGetOneRequest aboutUsGetOneRequest = new AboutUsGetOneRequest
            {
                Id = id
            };
            dynamic result = await _aboutUsService.GetOne(aboutUsGetOneRequest);
            return result;
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "AllAdmin")]
        public async Task<IActionResult> Update(string id, [FromForm] AboutUsUpdateRequest aboutUsUpdateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            AboutUsGetOneRequest aboutUsGetOneRequest = new AboutUsGetOneRequest
            {
                Id = id
            };
            dynamic result = await _aboutUsService.Update(aboutUsGetOneRequest, aboutUsUpdateRequest);
            return result;
        }
    }
}
