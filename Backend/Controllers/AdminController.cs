﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class AdminController : ControllerBase
    {
        private readonly IAdminService _adminService;

        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromForm] AdminLoginRequest adminLoginRequest)
        {
            if (!ModelState.IsValid) { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            dynamic result = await _adminService.Login(adminLoginRequest);
            return result;
        }

        [HttpPost]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Register([FromForm] AdminRegisterRequest adminRegisterRequest)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            dynamic result = await _adminService.Register(adminRegisterRequest);
            return result;
        }

        [HttpGet]
        [Authorize]
        [Authorize(Policy = "AllAdmin")]
        public async Task<IActionResult> GetList(string? name, string? username, int? page, int? pageSize)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            AdminGetListRequest adminGetListRequest = new AdminGetListRequest
            {
                Name = name,
                Username = username,
                Page = page ?? Constants.DefaultPage,
                PageSize = pageSize ?? Constants.DefaultPageSize
            };

            dynamic result = await _adminService.GetList(adminGetListRequest);
            return result;
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Update(int id, [FromForm] AdminUpdateRequest adminUpdateRequest)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            AdminGetOneRequest adminGetOneRequest = new AdminGetOneRequest
            {
                Id = id
            };
            dynamic result = await _adminService.Update(adminGetOneRequest, adminUpdateRequest);
            return result;
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            AdminGetOneRequest adminGetOneRequest = new AdminGetOneRequest
            {
                Id = id
            };
            dynamic result = await _adminService.Delete(adminGetOneRequest);
            return result;
        }
    }
}
