﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class CartController : ControllerBase
    {
        private readonly ICartService _cartService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CartController(
            ICartService cartService,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _cartService = cartService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        [Authorize]
        [Authorize(Policy = "Client")]
        public async Task<IActionResult> GetListByClient()
        {            
            string clientIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("ClientId").Value;
            int clientId = int.Parse(clientIdClaim);
            CartGetListRequest cartGetListRequest = new CartGetListRequest
            {
                ClientId = clientId
            };
            dynamic result = await _cartService.GetListByClient(cartGetListRequest);
            return result;
        }

        [HttpPost]
        [Authorize]
        [Authorize(Policy = "Client")]
        public async Task<IActionResult> Create([FromForm] CartCreateRequest cartCreateRequest)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            string clientIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("ClientId").Value;
            int clientId = int.Parse(clientIdClaim);
            dynamic result = await _cartService.Create(cartCreateRequest, clientId);
            return result;
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "Client")]
        public async Task<IActionResult> Update(int id, [FromForm] CartUpdateRequest cartUpdateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            CartGetOneRequest cartGetOneRequest = new CartGetOneRequest
            {
                Id = id
            };
            string clientIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("ClientId").Value;
            int clientId = int.Parse(clientIdClaim);
            dynamic result = await _cartService.Update(cartGetOneRequest, cartUpdateRequest, clientId);
            return result;
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "Client")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            CartGetOneRequest cartGetOneRequest = new CartGetOneRequest
            {
                Id = id
            };
            dynamic result = await _cartService.Delete(cartGetOneRequest);
            return result;
        }

        [HttpDelete]
        [Authorize]
        [Authorize(Policy = "Client")]
        public async Task<IActionResult> DeleteAllOfClient()
        {
            string clientIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("ClientId").Value;
            int clientId = int.Parse(clientIdClaim);
            CartGetListRequest cartGetListRequest = new CartGetListRequest
            {
                ClientId = clientId
            };
            dynamic result = await _cartService.DeleteAllOfClient(cartGetListRequest);
            return result;
        }
    }
}
