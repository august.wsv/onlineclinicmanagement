﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class MedicineTypeController : ControllerBase
    {
        private readonly IMedicineTypeService _medicineTypeService;

        public MedicineTypeController(IMedicineTypeService medicineTypeService)
        {
            _medicineTypeService = medicineTypeService;
        }

        [HttpGet]
        public async Task<IActionResult> GetList(string? name, int? page, int? pageSize)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            MedicineTypeGetListRequest medicineTypeGetListRequest = new MedicineTypeGetListRequest
            {
                Name = name,
                Page = page ?? Constants.DefaultPage,
                PageSize = pageSize ?? Constants.DefaultPageSize
            };

            dynamic result = await _medicineTypeService.GetList(medicineTypeGetListRequest);
            return result;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }

            dynamic result = await _medicineTypeService.GetAll();
            return result;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetOne(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            MedicineTypeGetOneRequest medicineTypeGetOneRequest = new MedicineTypeGetOneRequest
            {
                Id = id
            };
            dynamic result = await _medicineTypeService.GetOne(medicineTypeGetOneRequest);
            return result;
        }

        [HttpPost]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Create([FromForm] MedicineTypeCreateRequest medicineTypeCreateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            dynamic result = await _medicineTypeService.Create(medicineTypeCreateRequest);
            return result;
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Update(int id, [FromForm] MedicineTypeUpdateRequest medicineTypeUpdateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            MedicineTypeGetOneRequest medicineTypeGetOneRequest = new MedicineTypeGetOneRequest
            {
                Id = id
            };
            dynamic result = await _medicineTypeService.Update(medicineTypeGetOneRequest, medicineTypeUpdateRequest);
            return result;
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "JustSuperAdmin")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            MedicineTypeGetOneRequest medicineTypeGetOneRequest = new MedicineTypeGetOneRequest
            {
                Id = id
            };
            dynamic result = await _medicineTypeService.Delete(medicineTypeGetOneRequest);
            return result;
        }
    }
}
