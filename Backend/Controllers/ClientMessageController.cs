﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class ClientMessageController : ControllerBase
    {
        private readonly IClientMessageService _clientMessageService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ClientMessageController
        (
            IClientMessageService clientMessageService,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _clientMessageService = clientMessageService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public async Task<IActionResult> GetList(string? clientName,  string? clientPhone, string? clientEmail, string? title, DateTime maxCreatedAt, DateTime minCreatedAt, int? page, int? pageSize)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            ClientMessageGetListRequest clientMessageGetListRequest = new ClientMessageGetListRequest
            {
                ClientName = clientName,
                ClientPhone = clientPhone,
                ClientEmail = clientEmail,
                Title = title,
                MaxCreatedAt = maxCreatedAt,
                MinCreatedAt = minCreatedAt,
                Page = page ?? Constants.DefaultPage,
                PageSize = pageSize ?? Constants.DefaultPageSize
            };

            dynamic result = await _clientMessageService.GetList(clientMessageGetListRequest);
            return result;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetOne(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            ClientMessageGetOneRequest clientMessageGetOneRequest = new ClientMessageGetOneRequest
            {
                Id = id
            };
            dynamic result = await _clientMessageService.GetOne(clientMessageGetOneRequest);
            return result;
        }

        [HttpPost]
        [Authorize]
        [Authorize(Policy = "Client")]
        public async Task<IActionResult> Create([FromForm] ClientMessageCreateRequest clientMessageCreateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            string clientIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("ClientId").Value;
            int clientId = int.Parse(clientIdClaim);
            dynamic result = await _clientMessageService.Create(clientMessageCreateRequest, clientId);
            return result;
        }
    }
}
