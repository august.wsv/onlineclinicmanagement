﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class SubjectController : ControllerBase
    {
        private readonly ISubjectService _subjectService;

        public SubjectController(ISubjectService subjectService)
        {
            _subjectService = subjectService;
        }

        [HttpGet]
        public async Task<IActionResult> GetList(string? name, int? page, int? pageSize)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            SubjectGetListRequest subjectGetListRequest = new SubjectGetListRequest
            {
                Name = name,
                Page = page ?? Constants.DefaultPage,
                PageSize = pageSize ?? Constants.DefaultPageSize
            };

            dynamic result = await _subjectService.GetList(subjectGetListRequest);
            return result;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetOne(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            SubjectGetOneRequest subjectGetOneRequest = new SubjectGetOneRequest
            {
                Id = id
            };
            dynamic result = await _subjectService.GetOne(subjectGetOneRequest);
            return result;
        }

        [HttpPost]
        [Authorize]
        [Authorize(Policy = "AllAdmin")]
        public async Task<IActionResult> Create([FromForm] SubjectCreateRequest subjectCreateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            dynamic result = await _subjectService.Create(subjectCreateRequest);
            return result;
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "AllAdmin")]
        public async Task<IActionResult> Update(int id, [FromForm] SubjectUpdateRequest subjectUpdateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            SubjectGetOneRequest subjectGetOneRequest = new SubjectGetOneRequest
            {
                Id = id
            };
            dynamic result = await _subjectService.Update(subjectGetOneRequest, subjectUpdateRequest);
            return result;
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "AllAdmin")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            SubjectGetOneRequest subjectGetOneRequest = new SubjectGetOneRequest
            {
                Id = id
            };
            dynamic result = await _subjectService.Delete(subjectGetOneRequest);
            return result;
        }
    }
}
