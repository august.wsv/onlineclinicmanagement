﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Commons;
using Backend.Request;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class PurchaseController : ControllerBase
    {
        private readonly IPurchaseService _purchaseService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public PurchaseController(
            IPurchaseService purchaseService,
            IHttpContextAccessor httpContextAccessor
            )
        {
            _purchaseService = purchaseService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public async Task<IActionResult> GetList(string? clientName, string? clientEmail, int? status, string? address, DateTime maxCreatedAt, DateTime minCreatedAt, int? page, int? pageSize)
        {
            if (!ModelState.IsValid)
            {
                return Helpers.GetErrorResponse(ModelState);
            }
            PurchaseGetPagedListRequest purchaseGetPagedListRequest = new PurchaseGetPagedListRequest
            {
                ClientName = clientName,
                ClientEmail = clientEmail,
                Status = status,
                Address = address,
                MaxCreatedAt = maxCreatedAt,
                MinCreatedAt = minCreatedAt,
                Page = page ?? Constants.DefaultPage,
                PageSize = pageSize ?? Constants.DefaultPageSize
            };

            dynamic result = await _purchaseService.GetPagedList(purchaseGetPagedListRequest, _httpContextAccessor);
            return result;
        }

        [HttpGet]
        [Authorize]
        [Authorize(Policy = "Client")]
        public async Task<IActionResult> GetListByClient()
        {
            string clientIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("ClientId").Value;
            int clientId = int.Parse(clientIdClaim);
            PurchaseGetListRequest purchaseGetListRequest = new PurchaseGetListRequest
            {
                ClientId = clientId
            };
            dynamic result = await _purchaseService.GetListByClient(purchaseGetListRequest);
            return result;
        }

        [HttpGet]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "CanUpdatePurchaseStatus")]
        public async Task<IActionResult> GetOne(int id)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            PurchaseGetOneRequest purchaseGetOneRequest = new PurchaseGetOneRequest
            {
                Id = id
            };
            dynamic result = await _purchaseService.GetOne(purchaseGetOneRequest);
            return result;
        }

        [HttpPost]
        [Authorize]
        [Authorize(Policy = "Client")]
        public async Task<IActionResult> Create([FromForm] PurchaseCreateRequest purchaseCreateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            string clientIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst("ClientId").Value;
            int clientId = int.Parse(clientIdClaim);
            dynamic result = await _purchaseService.Create(purchaseCreateRequest, clientId);
            return result;
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize]
        [Authorize(Policy = "CanUpdatePurchaseStatus")]
        public async Task<IActionResult> Update(int id, [FromForm] PurchaseUpdateRequest purchaseUpdateRequest)
        {
            if (!ModelState.IsValid)
            { 
                return Helpers.GetErrorResponse(ModelState); 
            }
            PurchaseGetOneRequest purchaseGetOneRequest = new PurchaseGetOneRequest
            {
                Id = id
            };            
            dynamic result = await _purchaseService.Update(purchaseGetOneRequest, purchaseUpdateRequest);
            return result;
        }
    }
}
