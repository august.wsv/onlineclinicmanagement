﻿using Microsoft.AspNetCore.Mvc;
using Backend.Services.Contracts;

namespace Backend.Controllers
{
    [ApiController]
    [Route("Api/[Controller]/[Action]")]
    public class BusinessReportController : ControllerBase
    {
        private readonly IBusinessReportService _businessReportService;

        public BusinessReportController(IBusinessReportService businessReportService)
        {
            _businessReportService = businessReportService;
        }

        [HttpGet]
        public async Task<IActionResult> GetThisWeekReport()
        {       
            dynamic result = await _businessReportService.GetThisWeekReport();
            return result;
        }

        [HttpGet]
        public async Task<IActionResult> GetThisMonthReport()
        {
            dynamic result = await _businessReportService.GetThisMonthReport();
            return result;
        }
    }
}
