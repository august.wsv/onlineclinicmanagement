﻿using Backend.Commons;
using Backend.Models;

namespace Backend.Data
{
    public class DataInitializer
    {
        public static void Seed(DataContext context)
        {
            context.Database.EnsureCreated();            

            int administratorRole =  Constants.AdminRole["SUPERADMIN"];

            if (!context.Admins.Any())
            {
                var data = new[]
                {
                    new Admin { 
                        Name = "Luo Wen Tian", 
                        Username = "administrator", 
                        Password = BCrypt.Net.BCrypt.HashPassword("12345678"),
                        Role = administratorRole
                    }
                };
                context.Admins.AddRange(data);
                context.SaveChanges();
            }

            if (!context.AboutUs.Any())
            {
                var data = new[]
                {
                    new AboutUs
                    {
                        Key = "facebook",
                        Value = "https://www.facebook.com/"
                    },
                    new AboutUs
                    {
                        Key = "youtube",
                        Value = "https://www.youtube.com/"
                    },
                    new AboutUs
                    {
                        Key = "phone",
                        Value = "+84-835200708"
                    },
                    new AboutUs
                    {
                        Key = "address",
                        Value = "34 Alley 23, Giang Van Minh Street, Kim Ma Ward, Ba Dinh District, Hanoi City.g"
                    },
                    new AboutUs
                    {
                        Key = "copyright",
                        Value = "Copyright ©1995-2023 XWS. All rights reserved."
                    },
                    new AboutUs
                    {
                        Key = "company",
                        Value = "XWS Corporation Co., Ltd."
                    },
                    new AboutUs
                    {
                        Key = "auth",
                        Value = "Business Registration Certificate: 011043002319, issued by Hanoi People's Committee on January 01, 2023."
                    }

                };
                context.AboutUs.AddRange(data);
                context.SaveChanges();
            }
        }
    }
}
    