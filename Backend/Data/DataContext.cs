﻿using Microsoft.EntityFrameworkCore;
using Backend.Models;

namespace Backend.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<AboutUs> AboutUs { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientMessage> ClientMessages { get; set; }
        public DbSet<Clinic> Clinics { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<EducationalEvent> EducationalEvents { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<Medicine> Medicines { get; set; }
        public DbSet<MedicineType> MedicineTypes { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<PurchaseDetail> PurchaseDetails { get; set; }
        public DbSet<ScientificApparatus> ScientificApparatuses { get; set; }
        public DbSet<StaffMember> StaffMembers { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<BusinessReport> BusinessReports { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admin>().HasIndex(admin => new { admin.Username }).IsUnique(true);
            modelBuilder.Entity<Client>().HasIndex(client => new { client.Email }).IsUnique(true);
            modelBuilder.Entity<Clinic>().HasIndex(clinic => new { clinic.Email }).IsUnique(true);            
            modelBuilder.Entity<StaffMember>().HasIndex(staffMember => new { staffMember.Email }).IsUnique(true);
            base.OnModelCreating(modelBuilder);
        }
    }
}
