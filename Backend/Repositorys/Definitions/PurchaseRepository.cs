﻿using Microsoft.EntityFrameworkCore;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Definitions
{
    public class PurchaseRepository : IPurchaseRepository
    {
        private readonly DataContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public PurchaseRepository
        (
            DataContext context,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<IPagedList<Purchase>> GetPagedList(PurchaseGetPagedListRequest purchaseGetPagedListRequest)
        {
            IQueryable<Purchase> query = _context.Purchases.Include(purchase => purchase.AboutClient).AsQueryable();

            if (!string.IsNullOrEmpty(purchaseGetPagedListRequest.ClientName))
            {
                query = query.Include(purchase => purchase.AboutClient)
                    .Where(p => p.AboutClient.Name.Contains(purchaseGetPagedListRequest.ClientName));
            }

            if (!string.IsNullOrEmpty(purchaseGetPagedListRequest.ClientEmail))
            {
                query = query.Include(purchase => purchase.AboutClient)
                    .Where(purchase => purchase.AboutClient.Email.Contains(purchaseGetPagedListRequest.ClientEmail));
            }

            if (purchaseGetPagedListRequest.Status != null)
            {
                query = query.Where(purchase => purchase.Status == purchaseGetPagedListRequest.Status);
            }

            if (!string.IsNullOrEmpty(purchaseGetPagedListRequest.Address))
            {
                query = query.Where(medicineType => medicineType.Address.Contains(purchaseGetPagedListRequest.Address));
            }

            if (purchaseGetPagedListRequest.MaxCreatedAt > DateTime.MinValue)
            {
                query = query.Where(purchase => purchase.CreatedAt <= purchaseGetPagedListRequest.MaxCreatedAt);
            }

            if (purchaseGetPagedListRequest.MinCreatedAt > DateTime.MinValue)
            {
                query = query.Where(purchase => purchase.CreatedAt >= purchaseGetPagedListRequest.MinCreatedAt);
            }

            return await query.ToPagedListAsync(purchaseGetPagedListRequest.Page, purchaseGetPagedListRequest.PageSize);
        }

        public async Task<List<Purchase>> GetListByClient(PurchaseGetListRequest purchaseGetListRequest)
        {
            var query = _context.Purchases
                .Where(purchase => purchase.ClientId == purchaseGetListRequest.ClientId)
                .OrderByDescending(purchase => purchase.CreatedAt)
                .Select(purchase => new Purchase
                {
                    Id = purchase.Id,
                    Status = purchase.Status,
                    Address = purchase.Address,
                    CreatedAt = purchase.CreatedAt,
                    TotalString = string.Format("{0:C}", purchase.PurchaseDetails.Sum(purchaseDetails => purchaseDetails.Price * purchaseDetails.Quantity))
                });

            return await query.ToListAsync();
        }

        public async Task<Purchase> GetOne(PurchaseGetOneRequest purchaseGetOneRequest)
        {
            Purchase purchase = await _context.Purchases
                .Include(purchase => purchase.AboutClient)
                .Include(purchase => purchase.PurchaseDetails)
                .SingleOrDefaultAsync(purchase => purchase.Id.Equals(purchaseGetOneRequest.Id));

            return purchase;
        }

        public async Task<Purchase> Create(Purchase purchase)
        {
            await _context.Purchases.AddAsync(purchase);
            await _context.SaveChangesAsync();
            return purchase;
        }

        public async Task<bool> Update(PurchaseGetOneRequest purchaseGetOneRequest, PurchaseUpdateRequest purchaseUpdateRequest)
        {
            try
            {
                Purchase purchase = await _context.Purchases.FirstAsync(purchase => purchase.Id == purchaseGetOneRequest.Id);
                purchase.Status = purchaseUpdateRequest.Status;
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> UpdateMedicineQuantity(int medicineId, int quantity)
        {
            try
            {
                var medicine = await _context.Medicines.FindAsync(medicineId);
                if (medicine != null)
                {
                    medicine.Quantity -= quantity;
                    int result = await _context.SaveChangesAsync();
                    return result > 0;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> UpdateScientificApparatusQuantity(int apparatusId, int quantity)
        {
            try
            {
                var scientificApparatus = await _context.ScientificApparatuses.FindAsync(apparatusId);
                if (scientificApparatus != null)
                {
                    scientificApparatus.Quantity -= quantity;
                    int result = await _context.SaveChangesAsync();
                    return result > 0;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> UpdateMedicineQuantityRollback(int medicineId, int quantity)
        {
            try
            {
                var medicine = await _context.Medicines.FindAsync(medicineId);
                if (medicine != null)
                {
                    medicine.Quantity += quantity;
                    int result = await _context.SaveChangesAsync();
                    return result > 0;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> UpdateScientificApparatusQuantityRollback(int apparatusId, int quantity)
        {
            try
            {
                var scientificApparatus = await _context.ScientificApparatuses.FindAsync(apparatusId);
                if (scientificApparatus != null)
                {
                    scientificApparatus.Quantity += quantity;
                    int result = await _context.SaveChangesAsync();
                    return result > 0;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public async Task<BusinessReportCreateRequest> GetTotalDaily()
        {
            DateTime startOfDay = DateTime.Now.Date.AddDays(-1);
            DateTime endOfDay = DateTime.Now.Date;
            IQueryable<Purchase> query = _context.Purchases
                .Include(purchase => purchase.PurchaseDetails)
                .Where(purchase => purchase.CreatedAt >= startOfDay && purchase.CreatedAt < endOfDay)
                .AsQueryable();
            var result = await query.ToListAsync();

            foreach (var purchase in result)
            {
                purchase.TotalSales = purchase.PurchaseDetails.Sum(purchaseDetails => purchaseDetails.Price * purchaseDetails.Quantity);
                purchase.OrderCount = purchase.PurchaseDetails.Sum(purchaseDetails => purchaseDetails.Quantity);
            }
            decimal totalSalesDaily = (decimal)result.Sum(purchase => purchase.TotalSales);
            int orderCountlDaily = (int)result.Sum(purchase => purchase.OrderCount);

            BusinessReportCreateRequest businessReportCreateRequest = new BusinessReportCreateRequest
            {
                TotalSales = totalSalesDaily,
                OrderCount = orderCountlDaily
            };

            return businessReportCreateRequest;
        }
    }
}
