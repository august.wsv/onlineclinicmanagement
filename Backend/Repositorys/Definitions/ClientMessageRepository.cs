﻿using Microsoft.EntityFrameworkCore;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Definitions
{
    public class ClientMessageRepository : IClientMessageRepository
    {
        private readonly DataContext _context;

        public ClientMessageRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IPagedList<ClientMessage>> GetList(ClientMessageGetListRequest clientMessageGetListRequest)
        {
            IQueryable<ClientMessage> query = _context.ClientMessages.Include(clientMessage => clientMessage.AboutClient).AsQueryable();

            if (!string.IsNullOrEmpty(clientMessageGetListRequest.ClientName))
            {
                query = query.Include(clientMessage => clientMessage.AboutClient)
                    .Where(clientMessage => clientMessage.AboutClient.Name.Contains(clientMessageGetListRequest.ClientName));
            }

            if (!string.IsNullOrEmpty(clientMessageGetListRequest.ClientPhone))
            {
                query = query.Include(clientMessage => clientMessage.AboutClient)
                    .Where(clientMessage => clientMessage.AboutClient.Phone.Contains(clientMessageGetListRequest.ClientPhone));
            }

            if (!string.IsNullOrEmpty(clientMessageGetListRequest.ClientEmail))
            {
                query = query.Include(clientMessage => clientMessage.AboutClient)
                    .Where(clientMessage => clientMessage.AboutClient.Email.Contains(clientMessageGetListRequest.ClientEmail));
            }

            if (!string.IsNullOrEmpty(clientMessageGetListRequest.Title))
            {
                query = query.Where(clientMessage => clientMessage.Title.Contains(clientMessageGetListRequest.Title));
            }

            if (clientMessageGetListRequest.MaxCreatedAt > DateTime.MinValue)
            {
                query = query.Where(clientMessage => clientMessage.CreatedAt <= clientMessageGetListRequest.MaxCreatedAt);
            }

            if (clientMessageGetListRequest.MinCreatedAt > DateTime.MinValue)
            {
                query = query.Where(clientMessage => clientMessage.CreatedAt >= clientMessageGetListRequest.MinCreatedAt);
            }

            return await query.ToPagedListAsync(clientMessageGetListRequest.Page, clientMessageGetListRequest.PageSize);
        }

        public async Task<ClientMessage> GetOne(ClientMessageGetOneRequest clientMessageGetOneRequest)
        {
            return await _context.ClientMessages.Include(clientMessage => clientMessage.AboutClient)
                .SingleOrDefaultAsync(clientMessage => clientMessage.Id.Equals(clientMessageGetOneRequest.Id));
        }

        public async Task<bool> Create(ClientMessage clientMessage)
        {
            try
            {
                await _context.ClientMessages.AddAsync(clientMessage);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
