﻿using Microsoft.EntityFrameworkCore;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Definitions
{
    public class AdminRepository : IAdminRepository
    {
        private readonly DataContext _context;

        public AdminRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<Admin> GetOneByUsername(AdminGetOneByUsernameRequest adminGetOneByUsernameRequest)
        {
            Admin admin = await _context.Admins.Where(admin => admin.DeletedAt == null).SingleOrDefaultAsync(u => u.Username.Equals(adminGetOneByUsernameRequest.Username));

            return admin;
        }

        public async Task<bool> Create(Admin admin)
        {
            try
            {
                await _context.Admins.AddAsync(admin);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<IPagedList<Admin>> GetList(AdminGetListRequest adminGetListRequest)
        {
            IQueryable<Admin> query = _context.Admins.AsQueryable();

            if (!string.IsNullOrEmpty(adminGetListRequest.Name))
            {
                query = query.Where(admin => admin.Name.Contains(adminGetListRequest.Name));
            }

            if (!string.IsNullOrEmpty(adminGetListRequest.Username))
            {
                query = query.Where(admin => admin.Username.Contains(adminGetListRequest.Username));
            }

            query = query.Where(admin => admin.DeletedAt == null);

            return await query.ToPagedListAsync(adminGetListRequest.Page, adminGetListRequest.PageSize);
        }

        public async Task<Admin> GetOne(AdminGetOneRequest adminGetOneRequest)
        {
            return await _context.Admins
                .Where(admin => admin.DeletedAt == null)
                .SingleOrDefaultAsync(admin => admin.Id.Equals(adminGetOneRequest.Id));
        }

        public async Task<bool> Delete(AdminGetOneRequest adminGetOneRequest)
        {
            try
            {
                Admin admin = await _context.Admins.FirstAsync(admin => admin.Id == adminGetOneRequest.Id);
                admin.DeletedAt = DateTime.Now;
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Update(AdminGetOneRequest adminGetOneRequest, AdminUpdateRequest adminUpdateRequest)
        {
            try
            {
                Admin admin = await _context.Admins.FirstAsync(admin => admin.Id == adminGetOneRequest.Id);
                if (adminUpdateRequest.Name != null) { admin.Name = adminUpdateRequest.Name; }
                if (adminUpdateRequest.Username != null) { admin.Username = adminUpdateRequest.Username; }
                if (adminUpdateRequest.Password != null) { admin.Password = BCrypt.Net.BCrypt.HashPassword(adminUpdateRequest.Password); }
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
