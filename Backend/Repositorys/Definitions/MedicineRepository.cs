﻿using Microsoft.EntityFrameworkCore;
using Backend.Commons;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Definitions
{
    public class MedicineRepository : IMedicineRepository
    {
        private readonly DataContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IWebHostEnvironment _env;

        public MedicineRepository(
            DataContext context,
            IHttpContextAccessor httpContextAccessor,
            IWebHostEnvironment env
        )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _env = env;
        }

        public async Task<IPagedList<Medicine>> GetList(MedicineGetListRequest medicineGetListRequest)
        {
            IQueryable<Medicine> query = _context.Medicines.Include(medicine => medicine.AboutMedicineType).AsQueryable();

            query = query
                .Filter(!string.IsNullOrEmpty(medicineGetListRequest.Name), q => q.Where(medicine => medicine.Name.Contains(medicineGetListRequest.Name)))
                .Filter(!string.IsNullOrEmpty(medicineGetListRequest.Code), q => q.Where(medicine => medicine.Code.Contains(medicineGetListRequest.Code)))
                .Filter(medicineGetListRequest.MaxPrice != null, q => q.Where(medicine => medicine.Price <= medicineGetListRequest.MaxPrice))
                .Filter(medicineGetListRequest.MinPrice != null, q => q.Where(medicine => medicine.Price >= medicineGetListRequest.MinPrice))
                .Filter(medicineGetListRequest.MedicineTypeId != null, q => q.Where(medicine => medicine.MedicineTypeId == medicineGetListRequest.MedicineTypeId))
                .Filter(medicineGetListRequest.MaxExpDate > DateTime.MinValue, q => q.Where(medicine => medicine.ExpDate <= medicineGetListRequest.MaxExpDate))
                .Filter(medicineGetListRequest.MinExpDate > DateTime.MinValue, q => q.Where(medicine => medicine.ExpDate >= medicineGetListRequest.MinExpDate));

            query = query.Where(medicine => medicine.DeletedAt == null);

            return await query.ToPagedListAsync(medicineGetListRequest.Page, medicineGetListRequest.PageSize);
        }

        public async Task<Medicine?> GetOne(MedicineGetOneRequest medicineGetOneRequest)
        {
            Medicine? medicine = await _context.Medicines
                .Include(m => m.AboutMedicineType)
                .Where(m => m.DeletedAt == null)
                .SingleOrDefaultAsync(m => m.Id.Equals(medicineGetOneRequest.Id));

            return medicine;
        }

        public async Task<bool> Create(Medicine medicine)
        {
            try
            {
                await _context.Medicines.AddAsync(medicine);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Update(Medicine medicine)
        {
            try
            {
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
