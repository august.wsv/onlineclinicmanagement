﻿using Microsoft.EntityFrameworkCore;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Definitions
{
    public class StaffMemberRepository : IStaffMemberRepository
    {
        private readonly DataContext _context;

        public StaffMemberRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<StaffMember> GetOneByEmail(StaffMemberGetOneByEmailRequest staffMemberGetOneByEmailRequest)
        {
            return await _context.StaffMembers.Where(staffMember => staffMember.DeletedAt == null).SingleOrDefaultAsync(u => u.Email.Equals(staffMemberGetOneByEmailRequest.Email));
        }

        public async Task<StaffMember> GetOne(StaffMemberGetOneRequest staffMemberGetOneRequest)
        {
            return await _context.StaffMembers
                .Where(staffMember => staffMember.DeletedAt == null)
                .SingleOrDefaultAsync(staffMember => staffMember.Id.Equals(staffMemberGetOneRequest.Id));
        }

        public async Task<bool> Create(StaffMember staffMember)
        {
            try
            {
                await _context.StaffMembers.AddAsync(staffMember);
                var result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<IPagedList<StaffMember>> GetList(StaffMemberGetListRequest staffMemberGetListRequest)
        {
            IQueryable<StaffMember> query = _context.StaffMembers.AsQueryable();

            if (!string.IsNullOrEmpty(staffMemberGetListRequest.Name))
            {
                query = query.Where(staffMember => staffMember.Name.Contains(staffMemberGetListRequest.Name));
            }

            if (!string.IsNullOrEmpty(staffMemberGetListRequest.Phone))
            {
                query = query.Where(staffMember => staffMember.Phone.Contains(staffMemberGetListRequest.Phone));
            }

            if (!string.IsNullOrEmpty(staffMemberGetListRequest.Email))
            {
                query = query.Where(staffMember => staffMember.Email.Contains(staffMemberGetListRequest.Email));
            }

            query = query.Where(staffMember => staffMember.DeletedAt == null);

            return await query.ToPagedListAsync(staffMemberGetListRequest.Page, staffMemberGetListRequest.PageSize);
        }

        public async Task<bool> Delete(StaffMemberGetOneRequest staffMemberGetOneRequest)
        {
            try
            {
                StaffMember staffMember = await _context.StaffMembers.FirstAsync(staffMember => staffMember.Id == staffMemberGetOneRequest.Id);
                staffMember.DeletedAt = DateTime.Now;
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Update(StaffMemberGetOneRequest staffMemberGetOneRequest, StaffMemberUpdateRequest staffMemberUpdateRequest)
        {
            try
            {
                StaffMember staffMember = await _context.StaffMembers.FirstAsync(staffMember => staffMember.Id == staffMemberGetOneRequest.Id);
                if (staffMemberUpdateRequest.Name != null) { staffMember.Name = staffMemberUpdateRequest.Name; }
                if (staffMemberUpdateRequest.Phone != null) { staffMember.Phone = staffMemberUpdateRequest.Phone; }
                if (staffMemberUpdateRequest.Email != null) { staffMember.Email = staffMemberUpdateRequest.Email; }
                if (staffMemberUpdateRequest.PersonalBiography != null) { staffMember.PersonalBiography = staffMemberUpdateRequest.PersonalBiography; }
                if (staffMemberUpdateRequest.Password != null) { staffMember.Password = BCrypt.Net.BCrypt.HashPassword(staffMemberUpdateRequest.Password); }
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
