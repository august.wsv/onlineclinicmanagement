﻿using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;

namespace Backend.Repositorys.Definitions
{
    public class PurchaseDetailRepository : IPurchaseDetailRepository
    {
        private readonly DataContext _context;

        public PurchaseDetailRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> Create(PurchaseDetail purchaseDetail)
        {
            try
            {
                await _context.PurchaseDetails.AddAsync(purchaseDetail);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
