﻿using Microsoft.EntityFrameworkCore;
using Backend.Commons;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Definitions
{
    public class EducationalEventRepository : IEducationalEventRepository
    {
        private readonly DataContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IWebHostEnvironment _env;

        public EducationalEventRepository(
            DataContext context,
            IHttpContextAccessor httpContextAccessor,
            IWebHostEnvironment env
        )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _env = env;
        }

        public async Task<IPagedList<EducationalEvent>> GetList(EducationalEventGetListRequest educationalEventGetListRequest)
        {
            IQueryable<EducationalEvent> query = _context.EducationalEvents
                .AsQueryable();

            if (!string.IsNullOrEmpty(educationalEventGetListRequest.Name))
            {
                query = query.Where(educationalEvent => educationalEvent.Name.Contains(educationalEventGetListRequest.Name));
            }

            if (educationalEventGetListRequest.EducationalEventType != null)
            {
                query = query.Where(educationalEvent => educationalEvent.EducationalEventType == educationalEventGetListRequest.EducationalEventType);
            }

            if (educationalEventGetListRequest.SubjectId != null)
            {
                query = query.Where(educationalEvent => educationalEvent.SubjectId == educationalEventGetListRequest.SubjectId);
            }

            if (educationalEventGetListRequest.StaffMemberId != null)
            {
                query = query.Where(educationalEvent => educationalEvent.StaffMemberId == educationalEventGetListRequest.StaffMemberId);
            }

            if (educationalEventGetListRequest.MaxCreatedAt > DateTime.MinValue)
            {
                query = query.Where(educationalEvent => educationalEvent.CreatedAt <= educationalEventGetListRequest.MaxCreatedAt);
            }

            if (educationalEventGetListRequest.MinCreatedAt > DateTime.MinValue)
            {
                query = query.Where(educationalEvent => educationalEvent.CreatedAt >= educationalEventGetListRequest.MinCreatedAt);
            }

            query = query.Where(educationalEvent => educationalEvent.DeletedAt == null);

            return await query.ToPagedListAsync(educationalEventGetListRequest.Page, educationalEventGetListRequest.PageSize);
        }

        public async Task<List<EducationalEvent>> GetListBySubject(EducationalEventGetListRequest educationalEventGetListRequest)
        {
            IQueryable<EducationalEvent> query = _context.EducationalEvents
                .Where(educationalEvent => educationalEvent.DeletedAt == null)
                .AsQueryable();
            query = query.Where(educationalEvent => educationalEvent.SubjectId == educationalEventGetListRequest.SubjectId);

            return await query.ToListAsync();
        }

        public async Task<EducationalEvent> GetOne(EducationalEventGetOneRequest educationalEventGetOneRequest)
        {
            EducationalEvent educationalEvent = await _context.EducationalEvents
                .Where(educationalEvent => educationalEvent.DeletedAt == null)
                .SingleOrDefaultAsync(m => m.Id.Equals(educationalEventGetOneRequest.Id));

            return educationalEvent;
        }

        public async Task<bool> Create(EducationalEvent educationalEvent)
        {
            try
            {
                await _context.EducationalEvents.AddAsync(educationalEvent);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Update(EducationalEventGetOneRequest educationalEventGetOneRequest, EducationalEventUpdateRequest educationalEventUpdateRequest)
        {
            try
            {
                EducationalEvent educationalEvent = await _context.EducationalEvents.FirstAsync(educationalEvent => educationalEvent.Id == educationalEventGetOneRequest.Id);
                if (educationalEventUpdateRequest.Name != null) { educationalEvent.Name = educationalEventUpdateRequest.Name; }
                if (educationalEventUpdateRequest.BriefDescription != null) { educationalEvent.BriefDescription = educationalEventUpdateRequest.BriefDescription; }
                if (educationalEventUpdateRequest.Description != null) { educationalEvent.Description = educationalEventUpdateRequest.Description; }
                if (educationalEventUpdateRequest.VideoPath != null && educationalEventUpdateRequest.VideoPath.Length > 0) { educationalEvent.VideoPath = await Helpers.UploadFile(educationalEventUpdateRequest.VideoPath, "EducationalEvent", _env); }
                if (educationalEventUpdateRequest.EducationalEventType != null) { educationalEvent.EducationalEventType = educationalEventUpdateRequest.EducationalEventType; }
                if (educationalEventUpdateRequest.SubjectId != null) { educationalEvent.SubjectId = (int)educationalEventUpdateRequest.SubjectId; }
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Delete(EducationalEventGetOneRequest educationalEventGetOneRequest)
        {
            try
            {
                EducationalEvent educationalEvent = await _context.EducationalEvents.FirstAsync(educationalEvent => educationalEvent.Id == educationalEventGetOneRequest.Id);
                educationalEvent.DeletedAt = DateTime.Now;
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
