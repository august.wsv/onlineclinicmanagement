﻿using Microsoft.EntityFrameworkCore;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Definitions
{
    public class ClinicRepository : IClinicRepository
    {
        private readonly DataContext _context;

        public ClinicRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IPagedList<Clinic>> GetList(ClinicGetListRequest clinicGetListRequest)
        {
            IQueryable<Clinic> query = _context.Clinics.AsQueryable();

            if (!string.IsNullOrEmpty(clinicGetListRequest.Name))
            {
                query = query.Where(clinic => clinic.Name.Contains(clinicGetListRequest.Name));
            }

            if (!string.IsNullOrEmpty(clinicGetListRequest.Phone))
            {
                query = query.Where(clinic => clinic.Phone.Contains(clinicGetListRequest.Phone));
            }

            if (!string.IsNullOrEmpty(clinicGetListRequest.Email))
            {
                query = query.Where(clinic => clinic.Email.Contains(clinicGetListRequest.Email));
            }

            if (!string.IsNullOrEmpty(clinicGetListRequest.Address))
            {
                query = query.Where(clinic => clinic.Address.Contains(clinicGetListRequest.Address));
            }

            query = query.Where(clinic => clinic.DeletedAt == null);

            return await query.ToPagedListAsync(clinicGetListRequest.Page, clinicGetListRequest.PageSize);
        }

        public async Task<List<Clinic>> GetAll()
        {
            IQueryable<Clinic> query = _context.Clinics.AsQueryable();
            return await query.ToListAsync();

        }

        public async Task<Clinic> GetOne(ClinicGetOneRequest clinicGetOneRequest)
        {
            return await _context.Clinics
                .Where(clinic => clinic.DeletedAt == null)
                .SingleOrDefaultAsync(clinic => clinic.Id.Equals(clinicGetOneRequest.Id));
        }

        public async Task<bool> Create(Clinic clinic)
        {
            try
            {
                await _context.Clinics.AddAsync(clinic);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Update(ClinicGetOneRequest clinicGetOneRequest, ClinicUpdateRequest clinicUpdateRequest)
        {
            try
            {
                Clinic clinic = await _context.Clinics.FirstAsync(clinic => clinic.Id == clinicGetOneRequest.Id);
                if (clinicUpdateRequest.Name != null) { clinic.Name = clinicUpdateRequest.Name; }
                if (clinicUpdateRequest.Phone != null) { clinic.Phone = clinicUpdateRequest.Phone; }
                if (clinicUpdateRequest.Email != null) { clinic.Email = clinicUpdateRequest.Email; }
                if (clinicUpdateRequest.Address != null) { clinic.Address = clinicUpdateRequest.Address; }
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Delete(ClinicGetOneRequest clinicGetOneRequest)
        {
            try
            {
                Clinic clinic = await _context.Clinics.FirstAsync(clinic => clinic.Id == clinicGetOneRequest.Id);
                clinic.DeletedAt = DateTime.Now;
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
