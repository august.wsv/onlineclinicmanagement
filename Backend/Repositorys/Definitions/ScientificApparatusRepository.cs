﻿using Microsoft.EntityFrameworkCore;
using Backend.Commons;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Definitions
{
    public class ScientificApparatusRepository : IScientificApparatusRepository
    {
        private readonly DataContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IWebHostEnvironment _env;

        public ScientificApparatusRepository
        (
            DataContext context,
            IHttpContextAccessor httpContextAccessor,
            IWebHostEnvironment env
        )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _env = env;
        }

        public async Task<IPagedList<ScientificApparatus>> GetList(ScientificApparatusGetListRequest scientificApparatusGetListRequest)
        {
            IQueryable<ScientificApparatus> query = _context.ScientificApparatuses.AsQueryable();

            if (!string.IsNullOrEmpty(scientificApparatusGetListRequest.Name))
            {
                query = query.Where(scientificApparatus => scientificApparatus.Name.Contains(scientificApparatusGetListRequest.Name));
            }

            if (!string.IsNullOrEmpty(scientificApparatusGetListRequest.Code))
            {
                query = query.Where(scientificApparatus => scientificApparatus.Code.Contains(scientificApparatusGetListRequest.Code));
            }

            if (scientificApparatusGetListRequest.MaxPrice != null)
            {
                query = query.Where(scientificApparatus => scientificApparatus.Price <= scientificApparatusGetListRequest.MaxPrice);
            }

            if (scientificApparatusGetListRequest.MinPrice != null)
            {
                query = query.Where(scientificApparatus => scientificApparatus.Price >= scientificApparatusGetListRequest.MinPrice);
            }

            if (scientificApparatusGetListRequest.MaxExpDate > DateTime.MinValue)
            {
                query = query.Where(scientificApparatus => scientificApparatus.ExpDate <= scientificApparatusGetListRequest.MaxExpDate);
            }

            if (scientificApparatusGetListRequest.MinExpDate > DateTime.MinValue)
            {
                query = query.Where(scientificApparatus => scientificApparatus.ExpDate >= scientificApparatusGetListRequest.MinExpDate);
            }

            if (!string.IsNullOrEmpty(scientificApparatusGetListRequest.Origin))
            {
                query = query.Where(scientificApparatus => scientificApparatus.Origin.Contains(scientificApparatusGetListRequest.Origin));
            }

            query = query.Where(scientificApparatus => scientificApparatus.DeletedAt == null);

            return await query.ToPagedListAsync(scientificApparatusGetListRequest.Page, scientificApparatusGetListRequest.PageSize);
        }

        public async Task<ScientificApparatus> GetOne(ScientificApparatusGetOneRequest scientificApparatusGetOneRequest)
        {
            ScientificApparatus scientificApparatus = await _context.ScientificApparatuses
                .Where(s => s.DeletedAt == null)
                .SingleOrDefaultAsync(s => s.Id.Equals(scientificApparatusGetOneRequest.Id));

            return scientificApparatus;
        }

        public async Task<bool> Create(ScientificApparatus scientificApparatus)
        {
            try
            {
                await _context.ScientificApparatuses.AddAsync(scientificApparatus);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Update(ScientificApparatusGetOneRequest scientificApparatusGetOneRequest, ScientificApparatusUpdateRequest scientificApparatusUpdateRequest)
        {
            try
            {
                ScientificApparatus scientificApparatus = await _context.ScientificApparatuses.FirstAsync(scientificApparatus => scientificApparatus.Id == scientificApparatusGetOneRequest.Id);
                if (scientificApparatusUpdateRequest.Name != null) { scientificApparatus.Name = scientificApparatusUpdateRequest.Name; }
                if (scientificApparatusUpdateRequest.ImagePath != null && scientificApparatusUpdateRequest.ImagePath.Length > 0)
                {
                    scientificApparatus.ImagePath = await Helpers.UploadFile(scientificApparatusUpdateRequest.ImagePath, "ScientificApparatus", _env); ;
                }
                if (scientificApparatusUpdateRequest.Code != null) { scientificApparatus.Code = scientificApparatusUpdateRequest.Code; }
                if (scientificApparatusUpdateRequest.Quantity != null) { scientificApparatus.Quantity = (int)scientificApparatusUpdateRequest.Quantity; }
                if (scientificApparatusUpdateRequest.Price != null) { scientificApparatus.Price = (int)scientificApparatusUpdateRequest.Price; }
                if (scientificApparatusUpdateRequest.Description != null) { scientificApparatus.Description = scientificApparatusUpdateRequest.Description; }
                if (scientificApparatusUpdateRequest.ExpDate >= DateTime.MinValue) { scientificApparatus.ExpDate = scientificApparatusUpdateRequest.ExpDate; }
                if (scientificApparatusUpdateRequest.Origin != null) { scientificApparatus.Origin = scientificApparatusUpdateRequest.Origin; }
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Delete(ScientificApparatusGetOneRequest scientificApparatusGetOneRequest)
        {
            try
            {
                ScientificApparatus scientificApparatus = await _context.ScientificApparatuses.FirstAsync(scientificApparatus => scientificApparatus.Id == scientificApparatusGetOneRequest.Id);
                scientificApparatus.DeletedAt = DateTime.Now;
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
