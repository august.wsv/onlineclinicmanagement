﻿using Microsoft.EntityFrameworkCore;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Definitions
{
    public class ClientRepository : IClientRepository
    {
        private readonly DataContext _context;

        public ClientRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IPagedList<Client>> GetList(ClientGetListRequest clientGetListRequest)
        {
            IQueryable<Client> query = _context.Clients.Include(client => client.AboutClinic).AsQueryable();

            if (!string.IsNullOrEmpty(clientGetListRequest.Name))
            {
                query = query.Where(client => client.Name.Contains(clientGetListRequest.Name));
            }

            if (!string.IsNullOrEmpty(clientGetListRequest.Phone))
            {
                query = query.Where(client => client.Phone.Contains(clientGetListRequest.Phone));
            }

            if (!string.IsNullOrEmpty(clientGetListRequest.Email))
            {
                query = query.Where(client => client.Email.Contains(clientGetListRequest.Email));
            }

            if (!string.IsNullOrEmpty(clientGetListRequest.Address))
            {
                query = query.Where(client => client.Address.Contains(clientGetListRequest.Address));
            }

            if (!string.IsNullOrEmpty(clientGetListRequest.ClinicName))
            {
                query = query
                    .Where(client => client.AboutClinic.Name.Contains(clientGetListRequest.ClinicName));
            }

            query = query.Where(client => client.DeletedAt == null);

            return await query.ToPagedListAsync(clientGetListRequest.Page, clientGetListRequest.PageSize);
        }

        public async Task<Client> GetOneByEmail(ClientGetOneByEmailRequest clientGetOneByEmailRequest)
        {
            return await _context.Clients.Where(client => client.DeletedAt == null).SingleOrDefaultAsync(u => u.Email.Equals(clientGetOneByEmailRequest.Email));
        }

        public async Task<Client> GetOne(ClientGetOneRequest clientGetOneRequest)
        {
            return await _context.Clients.Include(client => client.AboutClinic)
                .Where(client => client.DeletedAt == null)
                .SingleOrDefaultAsync(clinic => clinic.Id.Equals(clientGetOneRequest.Id));
        }

        public async Task<bool> Create(Client client)
        {
            try
            {
                await _context.Clients.AddAsync(client);
                var result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Update(ClientGetOneRequest clientGetOneRequest, ClientUpdateRequest clientUpdateRequest)
        {
            try
            {
                Client client = await _context.Clients.FirstAsync(client => client.Id == clientGetOneRequest.Id);
                if (clientUpdateRequest.Name != null) { client.Name = clientUpdateRequest.Name; }
                if (clientUpdateRequest.Phone != null) { client.Phone = clientUpdateRequest.Phone; }
                if (clientUpdateRequest.Address != null) { client.Address = clientUpdateRequest.Address; }
                if (clientUpdateRequest.Password != null) { client.Email = clientUpdateRequest.Password; }
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Delete(ClientGetOneRequest clientGetOneRequest)
        {
            try
            {
                Client client = await _context.Clients.FirstAsync(client => client.Id == clientGetOneRequest.Id);
                client.DeletedAt = DateTime.Now;
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
