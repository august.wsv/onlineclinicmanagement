﻿using Microsoft.EntityFrameworkCore;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Definitions
{
    public class MedicineTypeRepository : IMedicineTypeRepository
    {
        private readonly DataContext _context;

        public MedicineTypeRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IPagedList<MedicineType>> GetList(MedicineTypeGetListRequest medicineTypeGetListRequest)
        {
            IQueryable<MedicineType> query = _context.MedicineTypes.AsQueryable();

            if (!string.IsNullOrEmpty(medicineTypeGetListRequest.Name))
            {
                query = query.Where(medicineType => medicineType.Name.Contains(medicineTypeGetListRequest.Name));
            }

            query = query.Where(medicineType => medicineType.DeletedAt == null);

            return await query.ToPagedListAsync(medicineTypeGetListRequest.Page, medicineTypeGetListRequest.PageSize);
        }


        public async Task<List<MedicineType>> GetAll()
        {
            IQueryable<MedicineType> query = _context.MedicineTypes
                .Where(medicineType => medicineType.DeletedAt == null)
                .AsQueryable();
            return await query.ToListAsync();

        }

        public async Task<MedicineType> GetOne(MedicineTypeGetOneRequest medicineTypeGetOneRequest)
        {
            return await _context.MedicineTypes
                .Where(medicineType => medicineType.DeletedAt == null)
                .SingleOrDefaultAsync(medicineType => medicineType.Id.Equals(medicineTypeGetOneRequest.Id));
        }

        public async Task<bool> Create(MedicineType medicineType)
        {
            try
            {
                await _context.MedicineTypes.AddAsync(medicineType);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Update(MedicineTypeGetOneRequest medicineTypeGetOneRequest, MedicineTypeUpdateRequest medicineTypeUpdateRequest)
        {
            try
            {
                MedicineType medicineType = await _context.MedicineTypes.FirstAsync(medicineType => medicineType.Id == medicineTypeGetOneRequest.Id);
                if (medicineTypeUpdateRequest.Name != null) { medicineType.Name = medicineTypeUpdateRequest.Name; }
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Delete(MedicineTypeGetOneRequest medicineTypeGetOneRequest)
        {
            try
            {
                MedicineType medicineType = await _context.MedicineTypes.FirstAsync(medicineType => medicineType.Id == medicineTypeGetOneRequest.Id);
                medicineType.DeletedAt = DateTime.Now;
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

    }
}
