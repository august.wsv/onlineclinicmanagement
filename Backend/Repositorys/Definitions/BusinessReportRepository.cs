﻿using Microsoft.EntityFrameworkCore;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;

namespace Backend.Repositorys.Definitions
{
    public class BusinessReportRepository : IBusinessReportRepository
    {
        private readonly DataContext _context;

        public BusinessReportRepository
        (
            DataContext context,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _context = context;
        }

        public async Task<List<BusinessReport>> GetThisWeekReport()
        {
            DateTime today = DateTime.Now.Date;
            DayOfWeek dayOfWeek = today.DayOfWeek;
            int daysUntilStartOfWeek = (int)dayOfWeek - (int)DayOfWeek.Monday;
            DateTime startOfWeek = today.AddDays(-daysUntilStartOfWeek).Date;

            DateTime endOfWeek = startOfWeek.AddDays(6);
            IQueryable<BusinessReport> query = _context.BusinessReports
                .Where(businessReport => businessReport.CreatedAt >= startOfWeek && businessReport.CreatedAt <= endOfWeek)
                .AsQueryable();

            return await query.ToListAsync();
        }

        public async Task<List<BusinessReport>> GetThisMonthReport()
        {
            DateTime today = DateTime.Now.Date;
            DateTime startOfMonth = new DateTime(today.Year, today.Month, 1); // Đầu tháng
            DateTime endOfMonth = startOfMonth.AddMonths(1).AddDays(-1);
            IQueryable<BusinessReport> query = _context.BusinessReports
                .Where(businessReport => businessReport.CreatedAt >= startOfMonth && businessReport.CreatedAt <= endOfMonth)
                .AsQueryable();

            return await query.ToListAsync();
        }

        public async Task<bool> Create(BusinessReport businessReport)
        {
            try
            {
                await _context.BusinessReports.AddAsync(businessReport);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
