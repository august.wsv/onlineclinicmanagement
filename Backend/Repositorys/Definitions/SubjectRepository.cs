﻿using Microsoft.EntityFrameworkCore;
using Backend.Commons;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Definitions
{
    public class SubjectRepository : ISubjectRepository
    {
        private readonly DataContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IWebHostEnvironment _env;

        public SubjectRepository(
            DataContext context,
            IHttpContextAccessor httpContextAccessor,
            IWebHostEnvironment env
        )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _env = env;
        }

        public async Task<IPagedList<Subject>> GetList(SubjectGetListRequest subjectGetListRequest)
        {
            IQueryable<Subject> query = _context.Subjects.AsQueryable();

            if (!string.IsNullOrEmpty(subjectGetListRequest.Name))
            {
                query = query.Where(subject => subject.Name.Contains(subjectGetListRequest.Name));
            }

            query = query.Where(subject => subject.DeletedAt == null);

            return await query.ToPagedListAsync(subjectGetListRequest.Page, subjectGetListRequest.PageSize);
        }

        public async Task<Subject> GetOne(SubjectGetOneRequest subjectGetOneRequest)
        {
            Subject subject = await _context.Subjects
                .Include(s => s.EducationalEvents)
                .Where(s => s.DeletedAt == null)
                .SingleOrDefaultAsync(m => m.Id.Equals(subjectGetOneRequest.Id));

            return subject;
        }

        public async Task<bool> Create(Subject subject)
        {
            try
            {
                await _context.Subjects.AddAsync(subject);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Update(SubjectGetOneRequest subjectGetOneRequest, SubjectUpdateRequest subjectUpdateRequest)
        {
            try
            {
                Subject subject = await _context.Subjects.FirstAsync(subject => subject.Id == subjectGetOneRequest.Id);
                if (subjectUpdateRequest.Name != null) { subject.Name = subjectUpdateRequest.Name; }
                if (subjectUpdateRequest.ImagePath != null && subjectUpdateRequest.ImagePath.Length > 0)
                {
                    subject.ImagePath = await Helpers.UploadFile(subjectUpdateRequest.ImagePath, "Subject", _env);
                }
                if (subjectUpdateRequest.BriefDescription != null) { subject.BriefDescription = subjectUpdateRequest.BriefDescription; }
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Delete(SubjectGetOneRequest subjectGetOneRequest)
        {
            try
            {
                Subject subject = await _context.Subjects.FirstAsync(subject => subject.Id == subjectGetOneRequest.Id);
                subject.DeletedAt = DateTime.Now;
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
