﻿using Microsoft.EntityFrameworkCore;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;

namespace Backend.Repositorys.Definitions
{
    public class AboutUsRepository : BaseEntity<AboutUs>, IAboutUsRepository
    {
        private readonly DataContext _context;

        public AboutUsRepository(DataContext context) : base(context)
        {
            _context = context;
        }        

        public async Task<List<AboutUs>> GetAll()
        {
            IQueryable<AboutUs> query = _context.AboutUs.AsQueryable();
            return await query.ToListAsync();

        }

        public async Task<AboutUs?> GetOne(AboutUsGetOneRequest aboutUsGetOneRequest)
        {
            return await _context.AboutUs
                .SingleOrDefaultAsync(aboutUs => aboutUs.Id.Equals(aboutUsGetOneRequest.Id));
        }
    }
}
