﻿using Microsoft.EntityFrameworkCore;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;

namespace Backend.Repositorys.Definitions
{
    public class FeedbackRepository : IFeedbackRepository
    {
        private readonly DataContext _context;

        public FeedbackRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<List<Feedback>> GetListByItem(FeedbackGetListRequest feedbackGetListRequest)
        {
            IQueryable<Feedback> query = _context.Feedbacks.Include(feedback => feedback.AboutClient).AsQueryable();

            query = query.Where(feedback =>
                feedback.ItemId == feedbackGetListRequest.ItemId &&
                feedback.ItemType == feedbackGetListRequest.ItemType
            );

            query = query.OrderBy(feedback => feedback.Id);

            return await query.ToListAsync();
        }

        public async Task<bool> CheckExist(FeedbacktCheckExistRequest feedbacktCheckExistRequest)
        {
            int count = await _context.Feedbacks.CountAsync(feedback =>
                feedback.ClientId == feedbacktCheckExistRequest.ClientId &&
                feedback.ItemId == feedbacktCheckExistRequest.ItemId &&
                feedback.ItemType == feedbacktCheckExistRequest.ItemType
            );

            return count > 0;
        }

        public async Task<bool> Create(Feedback feedback)
        {
            try
            {
                await _context.Feedbacks.AddAsync(feedback);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
