﻿using Microsoft.EntityFrameworkCore;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;

namespace Backend.Repositorys.Definitions
{
    public class CartRepository : ICartRepository
    {
        private readonly DataContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CartRepository(DataContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<List<Cart>> GetListByClient(CartGetListRequest cartGetListRequest)
        {
            IQueryable<Cart> query = _context.Carts.AsQueryable();

            query = query.Where(cart => cart.ClientId == cartGetListRequest.ClientId);

            query = query.OrderBy(cart => cart.ItemType);

            return await query.ToListAsync();
        }

        public async Task<string> GetTotalValue(CartGetListRequest cartGetListRequest)
        {
            IQueryable<Cart> query = _context.Carts.AsQueryable();

            query = query.Where(cart => cart.ClientId == cartGetListRequest.ClientId);

            var result = await query.ToListAsync();

            decimal totalValue = result.Sum(cart => cart.Price * cart.Quantity);

            return string.Format("{0:C}", totalValue);
        }

        public async Task<Cart> GetOne(CartGetOneRequest cartGetOneRequest)
        {
            return await _context.Carts
                .SingleOrDefaultAsync(cart => cart.Id.Equals(cartGetOneRequest.Id));
        }

        public async Task<Cart> CheckItem(CartCheckItemRequest cartCheckItemRequest)
        {
            return await _context.Carts.FirstOrDefaultAsync(cart =>
                cart.ClientId.Equals(cartCheckItemRequest.ClientId) &&
                cart.ItemId.Equals(cartCheckItemRequest.ItemId) &&
                cart.ItemType.Equals(cartCheckItemRequest.ItemType)
            );
        }

        public async Task<bool> UpdateInc(Cart cart)
        {
            try
            {
                cart.Quantity += 1;
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Create(Cart cart)
        {
            try
            {
                await _context.Carts.AddAsync(cart);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Update(CartGetOneRequest cartGetOneRequest, CartUpdateRequest cartUpdateRequest)
        {
            try
            {
                Cart cart = await _context.Carts.FirstAsync(cart => cart.Id == cartGetOneRequest.Id);
                cart.Quantity = (int)cartUpdateRequest.Quantity;
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Delete(CartGetOneRequest cartGetOneRequest)
        {
            try
            {
                Cart cart = await _context.Carts.FirstAsync(cart => cart.Id == cartGetOneRequest.Id);
                _context.Carts.Remove(cart);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> DeleteAllOfClient(CartGetListRequest cartGetListRequest)
        {
            try
            {
                var cartsToDelete = _context.Carts
                    .Where(cart => cart.ClientId == cartGetListRequest.ClientId)
                    .ToList();
                _context.Carts.RemoveRange(cartsToDelete);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
