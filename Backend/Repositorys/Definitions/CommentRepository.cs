﻿using Microsoft.EntityFrameworkCore;
using Backend.Data;
using Backend.Models;
using Backend.Repositorys.Contracts;
using Backend.Request;

namespace Backend.Repositorys.Definitions
{
    public class CommentRepository : ICommentRepository
    {
        private readonly DataContext _context;

        public CommentRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<List<Comment>> GetListByEducationalEventId(CommentGetListRequest commentGetListRequest)
        {
            IQueryable<Comment> query = _context.Comments.AsQueryable();

            query = query.Where(comment => comment.EducationalEventId == commentGetListRequest.EducationalEventId);
            query = query.OrderBy(comment => comment.Id);

            return await query.ToListAsync();
        }

        public async Task<bool> Create(Comment comment)
        {
            try
            {
                await _context.Comments.AddAsync(comment);
                int result = await _context.SaveChangesAsync();
                return result > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
