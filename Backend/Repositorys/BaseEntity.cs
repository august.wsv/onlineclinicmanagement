﻿using Microsoft.EntityFrameworkCore;
using Backend.Models;
using System.Security.Principal;

namespace Backend.Repositorys
{
    public abstract class BaseEntity<T> where T : class, IEntity
    {
        private readonly DbContext context;

        public BaseEntity(DbContext context)
        {
            this.context = context;
        }

        public async Task<bool> Create(T entity)
        {
            try
            {
                await context.Set<T>().AddAsync(entity);
                int result = await context.SaveChangesAsync();
                Console.WriteLine($"{typeof(T).Name} created successfully.");
                return result > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error creating entity: {ex.Message}");
                return false;
            }
        }

        public async Task<bool> Update(T entity)
        {
            try
            {
                context.Set<T>().Update(entity);
                int result = await context.SaveChangesAsync();
                Console.WriteLine($"{typeof(T).Name} updated successfully.");
                return result > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error updating entity: {ex.Message}");
                return false;
            }
        }

        public async Task<bool> Delete(T entity)
        {
            try
            {
                context.Set<T>().Remove(entity);
                int result = await context.SaveChangesAsync();
                Console.WriteLine($"{typeof(T).Name} deleted successfully.");
                return result > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error deleting entity: {ex.Message}");
                return false;
            }
        }

        public async Task<T?> GetById(long Id)
        {
            try
            {
                return await context.Set<T>().SingleOrDefaultAsync(entity => entity.Id.Equals(Id));
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error getting entities by ids: {ex.Message}");
                return null;
            }
        }
    }
}
