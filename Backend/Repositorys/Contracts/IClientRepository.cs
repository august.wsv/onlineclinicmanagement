﻿using Backend.Models;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Contracts
{
    public interface IClientRepository
    {
        Task<IPagedList<Client>> GetList(ClientGetListRequest clientGetListRequest);
        Task<Client> GetOneByEmail(ClientGetOneByEmailRequest clientGetOneByEmailRequest);
        Task<Client> GetOne(ClientGetOneRequest clientGetOneRequest);
        Task<bool> Create(Client client);
        Task<bool> Update(ClientGetOneRequest clientGetOneRequest, ClientUpdateRequest clientUpdateRequest);
        Task<bool> Delete(ClientGetOneRequest clientGetOneRequest);
    }
}
