﻿using Backend.Models;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Contracts
{
    public interface ISubjectRepository
    {
        Task<IPagedList<Subject>> GetList(SubjectGetListRequest subjectGetListRequest);
        Task<Subject> GetOne(SubjectGetOneRequest subjectGetOneRequest);
        Task<bool> Create(Subject subject);
        Task<bool> Update(SubjectGetOneRequest subjectGetOneRequest, SubjectUpdateRequest subjectUpdateRequest);
        Task<bool> Delete(SubjectGetOneRequest subjectGetOneRequest);
    }
}
