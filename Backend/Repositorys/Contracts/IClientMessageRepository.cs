﻿using Backend.Models;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Contracts
{
    public interface IClientMessageRepository
    {
        Task<IPagedList<ClientMessage>> GetList(ClientMessageGetListRequest clientMessageGetListRequest);
        Task<ClientMessage> GetOne(ClientMessageGetOneRequest clientMessageGetOneRequest);
        Task<bool> Create(ClientMessage clientMessage);
    }
}
