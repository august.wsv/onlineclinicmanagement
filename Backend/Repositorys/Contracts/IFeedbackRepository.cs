﻿using Backend.Models;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Contracts
{
    public interface IFeedbackRepository
    {
        Task<List<Feedback>> GetListByItem(FeedbackGetListRequest feedbackGetListRequest);
        Task<bool> CheckExist(FeedbacktCheckExistRequest feedbacktCheckExistRequest);
        Task<bool> Create(Feedback feedback);
    }
}
