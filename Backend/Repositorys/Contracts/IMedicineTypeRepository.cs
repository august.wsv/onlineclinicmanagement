﻿using Backend.Models;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Contracts
{
    public interface IMedicineTypeRepository
    {
        Task<IPagedList<MedicineType>> GetList(MedicineTypeGetListRequest medicineTypeGetListRequest);
        Task<List<MedicineType>> GetAll();
        Task<MedicineType> GetOne(MedicineTypeGetOneRequest medicineTypeGetOneRequest);
        Task<bool> Create(MedicineType medicineType);
        Task<bool> Update(MedicineTypeGetOneRequest medicineTypeGetOneRequest, MedicineTypeUpdateRequest medicineTypeUpdateRequest);
        Task<bool> Delete(MedicineTypeGetOneRequest medicineTypeGetOneRequest);
    }
}
