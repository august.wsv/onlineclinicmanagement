﻿using Backend.Models;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Contracts
{
    public interface IPurchaseRepository
    {
        Task<IPagedList<Purchase>> GetPagedList(PurchaseGetPagedListRequest purchaseGetPagedListRequest);
        Task<List<Purchase>> GetListByClient(PurchaseGetListRequest purchaseGetListRequest);
        Task<Purchase> GetOne(PurchaseGetOneRequest purchaseGetOneRequest);
        Task<Purchase> Create(Purchase purchase);
        Task<bool> Update(PurchaseGetOneRequest purchaseGetOneRequest, PurchaseUpdateRequest purchaseUpdateRequest);
        Task<bool> UpdateMedicineQuantity(int itemId, int quantity);
        Task<bool> UpdateScientificApparatusQuantity(int itemId, int quantity);
        Task<bool> UpdateMedicineQuantityRollback(int itemId, int quantity);
        Task<bool> UpdateScientificApparatusQuantityRollback(int itemId, int quantity);
        Task<BusinessReportCreateRequest> GetTotalDaily();
    }
}
