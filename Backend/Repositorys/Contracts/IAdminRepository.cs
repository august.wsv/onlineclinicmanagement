﻿using Backend.Models;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Contracts
{
    public interface IAdminRepository
    {
        Task<Admin> GetOneByUsername(AdminGetOneByUsernameRequest adminGetOneByUsernameRequest);
        Task<bool> Create(Admin admin);
        Task<IPagedList<Admin>> GetList(AdminGetListRequest adminGetListRequest);
        Task<Admin> GetOne(AdminGetOneRequest adminGetOneRequest);
        Task<bool> Delete(AdminGetOneRequest adminGetOneRequest);
        Task<bool> Update(AdminGetOneRequest adminGetOneRequest, AdminUpdateRequest adminUpdateRequest);
    }
}
