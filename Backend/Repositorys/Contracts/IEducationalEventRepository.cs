﻿using Backend.Models;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Contracts
{
    public interface IEducationalEventRepository
    {
        Task<IPagedList<EducationalEvent>> GetList(EducationalEventGetListRequest educationalEventGetListRequest);
        Task<List<EducationalEvent>> GetListBySubject(EducationalEventGetListRequest educationalEventGetListRequest);
        Task<EducationalEvent> GetOne(EducationalEventGetOneRequest educationalEventGetOneRequest);
        Task<bool> Create(EducationalEvent educationalEvent);
        Task<bool> Update(EducationalEventGetOneRequest educationalEventGetOneRequest, EducationalEventUpdateRequest educationalEventUpdateRequest);
        Task<bool> Delete(EducationalEventGetOneRequest educationalEventGetOneRequest);
    }
}
