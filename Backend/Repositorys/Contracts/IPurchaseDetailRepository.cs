﻿using Backend.Models;

namespace Backend.Repositorys.Contracts
{
    public interface IPurchaseDetailRepository
    {
        Task<bool> Create(PurchaseDetail purchaseDetail);
    }
}
