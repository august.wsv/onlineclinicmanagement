﻿using Backend.Models;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Contracts
{
    public interface IClinicRepository
    {
        Task<IPagedList<Clinic>> GetList(ClinicGetListRequest clinicGetListRequest);
        Task<List<Clinic>> GetAll();
        Task<Clinic> GetOne(ClinicGetOneRequest clinicGetOneRequest);
        Task<bool> Create(Clinic clinic);
        Task<bool> Update(ClinicGetOneRequest clinicGetOneRequest, ClinicUpdateRequest clinicUpdateRequest);
        Task<bool> Delete(ClinicGetOneRequest clinicGetOneRequest);
    }
}
