﻿using Backend.Models;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Contracts
{
    public interface IAboutUsRepository
    {
        Task<List<AboutUs>> GetAll();
        Task<AboutUs?> GetOne(AboutUsGetOneRequest aboutUsGetOneRequest);
        Task<AboutUs?> GetById(long Id);
        Task<bool> Update(AboutUs aboutUs);
        Task<bool> Create(AboutUs aboutUs);
        Task<bool> Delete(AboutUs aboutUs);
    }
}
