﻿using Backend.Models;

namespace Backend.Repositorys.Contracts
{
    public interface IBusinessReportRepository
    {       
        Task<List<BusinessReport>> GetThisWeekReport();
        Task<List<BusinessReport>> GetThisMonthReport();
        Task<bool> Create(BusinessReport businessReport);
    }
}
