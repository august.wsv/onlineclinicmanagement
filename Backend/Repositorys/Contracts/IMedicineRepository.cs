﻿using Backend.Models;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Contracts
{
    public interface IMedicineRepository
    {
        Task<IPagedList<Medicine>> GetList(MedicineGetListRequest medicineGetListRequest);
        Task<Medicine?> GetOne(MedicineGetOneRequest medicineGetOneRequest);
        Task<bool> Create(Medicine medicine);
        Task<bool> Update(Medicine medicine);
    }
}
