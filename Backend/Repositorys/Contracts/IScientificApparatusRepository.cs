﻿using Backend.Models;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Contracts
{
    public interface IScientificApparatusRepository
    {
        Task<IPagedList<ScientificApparatus>> GetList(ScientificApparatusGetListRequest scientificApparatusGetListRequest);
        Task<ScientificApparatus> GetOne(ScientificApparatusGetOneRequest scientificApparatusGetOneRequest);
        Task<bool> Create(ScientificApparatus scientificApparatus);
        Task<bool> Update(ScientificApparatusGetOneRequest scientificApparatusGetOneRequest, ScientificApparatusUpdateRequest scientificApparatusUpdateRequest);
        Task<bool> Delete(ScientificApparatusGetOneRequest scientificApparatusGetOneRequest);
    }
}
