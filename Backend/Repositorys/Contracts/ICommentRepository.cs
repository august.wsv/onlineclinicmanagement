﻿using Backend.Models;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Contracts
{
    public interface ICommentRepository
    {
        Task<List<Comment>> GetListByEducationalEventId(CommentGetListRequest commentGetListRequest);
        Task<bool> Create(Comment comment);
    }
}
