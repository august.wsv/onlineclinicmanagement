﻿using Backend.Models;
using Backend.Request;
using X.PagedList;

namespace Backend.Repositorys.Contracts
{
    public interface IStaffMemberRepository
    {
        Task<StaffMember> GetOneByEmail(StaffMemberGetOneByEmailRequest staffMemberGetOneByEmailRequest);
        Task<StaffMember> GetOne(StaffMemberGetOneRequest staffMembertGetOneRequest);
        Task<bool> Create(StaffMember staffMember);
        Task<IPagedList<StaffMember>> GetList(StaffMemberGetListRequest staffMemberGetListRequest);
        Task<bool> Update(StaffMemberGetOneRequest staffMemberGetOneRequest, StaffMemberUpdateRequest staffMemberUpdateRequest);
        Task<bool> Delete(StaffMemberGetOneRequest staffMemberGetOneRequest);        
    }
}
