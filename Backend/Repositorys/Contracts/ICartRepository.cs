﻿using Backend.Models;
using Backend.Request;

namespace Backend.Repositorys.Contracts
{
    public interface ICartRepository
    {
        Task<List<Cart>> GetListByClient(CartGetListRequest cartGetListRequest);
        Task<string> GetTotalValue(CartGetListRequest cartGetListRequest);
        Task<Cart> GetOne(CartGetOneRequest cartGetOneRequest);
        Task<bool> Create(Cart cart);
        Task<bool> Update(CartGetOneRequest cartGetOneRequest, CartUpdateRequest cartUpdateRequest);
        Task<bool> Delete(CartGetOneRequest cartGetOneRequest);
        Task<Cart> CheckItem(CartCheckItemRequest cartCheckItemRequest);
        Task<bool> UpdateInc(Cart cart);
        Task<bool> DeleteAllOfClient(CartGetListRequest cartGetListRequest);
    }
}
