﻿using System.Net.Mail;
using System.Net;
using RazorLight;
using Backend.Models;

namespace Backend.Mail
{
    public class ToClient
    {
        public static async Task OrderDetailAsync(string toAddress, Purchase purchase)
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            string templateFolderPath = Path.Combine(currentDirectory, "Mail", "EmailTemplates");
            string templateName = "OrderDetail.cshtml";
            var engine = new RazorLightEngineBuilder()
                .UseFileSystemProject(templateFolderPath)
                .Build();
            string fromAddress = "sws.pxl@gmail.com";
            string password = "zllwxnyqdirrnvvq";
            string subject = "Order has been approved";
            string body = await engine.CompileRenderAsync(templateName, purchase);

            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential(fromAddress, password),
                EnableSsl = true,
            };

            MailMessage mailMessage = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true,
            };

            mailMessage.From = new MailAddress(fromAddress, "Saler");
            mailMessage.To.Add(new MailAddress(toAddress));

            try
            {
                smtpClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error sending email: " + ex.Message);
            }
        }

        public static async Task EmailConfirmationAsync(string toAddress, string code)
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            string templateFolderPath = Path.Combine(currentDirectory, "Mail", "EmailTemplates");
            string templateName = "EmailConfirmation.cshtml";
            var engine = new RazorLightEngineBuilder()
                .UseFileSystemProject(templateFolderPath)
                .Build();
            string fromAddress = "sws.pxl@gmail.com";
            string password = "zllwxnyqdirrnvvq";
            string subject = "Email confirmation";
            string body = await engine.CompileRenderAsync(templateName, code);

            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential(fromAddress, password),
                EnableSsl = true,
            };

            MailMessage mailMessage = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true,
            };

            mailMessage.From = new MailAddress(fromAddress, "Saler");
            mailMessage.To.Add(new MailAddress(toAddress));

            try
            {
                smtpClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error sending email: " + ex.Message);
            }
        }
    }
}
